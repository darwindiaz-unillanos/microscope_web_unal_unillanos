
/* Drop Tables */

DROP TABLE IF EXISTS roles_modules_actions;
DROP TABLE IF EXISTS actions;
DROP TABLE IF EXISTS annotations;
DROP TABLE IF EXISTS cases;
DROP TABLE IF EXISTS collections;
DROP TABLE IF EXISTS modules;
DROP TABLE IF EXISTS pathos_organs;
DROP TABLE IF EXISTS organs;
DROP TABLE IF EXISTS pathos_specialty;
DROP TABLE IF EXISTS pathologists;
DROP TABLE IF EXISTS users_roles;
DROP TABLE IF EXISTS roles;
DROP TABLE IF EXISTS specialty;
DROP TABLE IF EXISTS users;




/* Create Tables */

CREATE TABLE actions
(
	id_action smallint NOT NULL,
	name_action varchar(20) NOT NULL,
	active_action boolean NOT NULL,
	PRIMARY KEY (id_action)
) WITHOUT OIDS;


CREATE TABLE annotations
(
	id_anno serial NOT NULL,
	description_anno varchar(200) NOT NULL,
	color_anno varchar(7) NOT NULL,
	create_at_zoom_anno double precision NOT NULL,
	region_definition_anno varchar(500) NOT NULL,
	region_type_anno varchar(1) NOT NULL,
	id_case int NOT NULL,
	id_user int NOT NULL,
	PRIMARY KEY (id_anno)
) WITHOUT OIDS;


CREATE TABLE cases
(
	id_case serial NOT NULL,
	name_case varchar(50) NOT NULL,
	path_imagen_case varchar(100),
	id_colle int NOT NULL,
	PRIMARY KEY (id_case)
) WITHOUT OIDS;


CREATE TABLE collections
(
	id_colle serial NOT NULL,
	description_colle varchar(100) NOT NULL,
	father_colle varchar(50),
	end_node_cole varchar(50),
	PRIMARY KEY (id_colle)
) WITHOUT OIDS;


CREATE TABLE modules
(
	id_module smallint NOT NULL,
	name_module varchar(20) NOT NULL,
	description_module varchar(50),
	active_module boolean NOT NULL,
	PRIMARY KEY (id_module)
) WITHOUT OIDS;


CREATE TABLE organs
(
	id_organ smallint NOT NULL,
	name_organ varchar(50) NOT NULL,
	PRIMARY KEY (id_organ)
) WITHOUT OIDS;


CREATE TABLE pathologists
(
	users_id_user int NOT NULL,
	id_patho bigint NOT NULL,
	name_patho varchar(100) NOT NULL,
	lastname_patho varchar(100) NOT NULL,
	country_patho varchar(2) NOT NULL,
	city_patho varchar(50) NOT NULL,
	zipcode_patho varchar(10) NOT NULL,
	university_degrees_patho varchar(5) NOT NULL,
	institution_patho varchar(100) NOT NULL,
	phone_patho varchar(20),
	mobile_patho varchar(20),
	occupation_patho varchar(100),
	language_patho varchar(2),
	PRIMARY KEY (id_patho)
) WITHOUT OIDS;


CREATE TABLE pathos_organs
(
	id_patho bigint NOT NULL,
	id_organ smallint NOT NULL
) WITHOUT OIDS;


CREATE TABLE pathos_specialty
(
	id_patho bigint NOT NULL,
	id_special smallint NOT NULL
) WITHOUT OIDS;


CREATE TABLE roles
(
	id_rol smallint NOT NULL,
	name_rol varchar(20) NOT NULL,
	description_rol varchar(50),
	PRIMARY KEY (id_rol)
) WITHOUT OIDS;


CREATE TABLE roles_modules_actions
(
	id_rol smallint NOT NULL,
	id_module smallint NOT NULL,
	id_action smallint NOT NULL
) WITHOUT OIDS;


CREATE TABLE specialty
(
	id_special smallint NOT NULL,
	name_special varchar(50) NOT NULL,
	PRIMARY KEY (id_special)
) WITHOUT OIDS;


CREATE TABLE users
(
	id_user serial NOT NULL,
	email_user varchar(100) NOT NULL UNIQUE,
	password_user varchar(256) NOT NULL,
	create_date_user timestamp NOT NULL,
	PRIMARY KEY (id_user)
) WITHOUT OIDS;


CREATE TABLE users_roles
(
	id_user int NOT NULL,
	id_rol smallint NOT NULL
) WITHOUT OIDS;



/* Create Foreign Keys */

ALTER TABLE roles_modules_actions
	ADD FOREIGN KEY (id_action)
	REFERENCES actions (id_action)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE annotations
	ADD FOREIGN KEY (id_case)
	REFERENCES cases (id_case)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE cases
	ADD FOREIGN KEY (id_colle)
	REFERENCES collections (id_colle)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE roles_modules_actions
	ADD FOREIGN KEY (id_module)
	REFERENCES modules (id_module)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE pathos_organs
	ADD FOREIGN KEY (id_organ)
	REFERENCES organs (id_organ)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE pathos_organs
	ADD FOREIGN KEY (id_patho)
	REFERENCES pathologists (id_patho)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE pathos_specialty
	ADD FOREIGN KEY (id_patho)
	REFERENCES pathologists (id_patho)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE roles_modules_actions
	ADD FOREIGN KEY (id_rol)
	REFERENCES roles (id_rol)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE users_roles
	ADD FOREIGN KEY (id_rol)
	REFERENCES roles (id_rol)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE pathos_specialty
	ADD FOREIGN KEY (id_special)
	REFERENCES specialty (id_special)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE annotations
	ADD FOREIGN KEY (id_user)
	REFERENCES users (id_user)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE pathologists
	ADD FOREIGN KEY (users_id_user)
	REFERENCES users (id_user)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE users_roles
	ADD FOREIGN KEY (id_user)
	REFERENCES users (id_user)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;



