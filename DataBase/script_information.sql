INSERT INTO organs
values
(1,'Bazo'),
(2,'Cerebro'),
(3,'Clítoris'),
(4,'Corazón'),
(5,'Estómago'),
(6,'Hígado'),
(7,'Huesos'),
(8,'Intestino delgado'),
(9,'Intestino grueso'),
(10,'Lengua'),
(11,'Músculos'),
(12,'Nariz'),
(13,'Oído'),
(14,'Ojos'),
(15,'Ovarios'),
(16,'Páncreas'),
(17,'Pene'),
(18,'Piel'),
(19,'Próstata'),
(20,'Pulmones'),
(21,'Riñones'),
(22,'Testículos'),
(23,'Timo'),
(24,'Útero'),
(25,'Vejiga');

INSERT INTO specialty
values
(1,'Citopatología'),
(2,'Dermatopatología'),
(3,'Hematología'),
(4,'Imnunología'),
(5,'Neuropatología'),
(6,'Patología anatómica'),
(7,'Patología clínica'),
(8,'Patología forence'),
(9,'Patología química'),
(10,'Patologia quirúrgica');

INSERT INTO roles
values
(1,'guest'),
(2,'student'),
(3,'pathologist'),
(4,'admin');


INSERT INTO modules
values
(1,'viewer','',true),
(2,'list','',true),
(3,'index','',true),
(4,'profile','',true);

INSERT INTO actions
values
(1,'create',true),
(2,'read',true),
(3,'update',true),
(4,'delete',true),
(5,'view',true);

INSERT INTO roles_modules_actions SELECT id_rol,id_module,id_action from roles,modules,actions where name_rol='guest' and name_module='viewer' and name_action='view';
INSERT INTO roles_modules_actions SELECT id_rol,id_module,id_action from roles,modules,actions where name_rol='student' and name_module='viewer' and name_action='view';
INSERT INTO roles_modules_actions SELECT id_rol,id_module,id_action from roles,modules,actions where name_rol='student' and name_module='viewer' and name_action='read';
INSERT INTO roles_modules_actions SELECT id_rol,id_module,id_action from roles,modules,actions where name_rol='pathologist' and name_module='viewer' and name_action='view';
INSERT INTO roles_modules_actions SELECT id_rol,id_module,id_action from roles,modules,actions where name_rol='pathologist' and name_module='viewer' and name_action='create';
INSERT INTO roles_modules_actions SELECT id_rol,id_module,id_action from roles,modules,actions where name_rol='pathologist' and name_module='viewer' and name_action='update';
INSERT INTO roles_modules_actions SELECT id_rol,id_module,id_action from roles,modules,actions where name_rol='pathologist' and name_module='viewer' and name_action='read';
INSERT INTO roles_modules_actions SELECT id_rol,id_module,id_action from roles,modules,actions where name_rol='pathologist' and name_module='viewer' and name_action='delete';
INSERT INTO roles_modules_actions SELECT id_rol,id_module,id_action from roles,modules,actions where name_rol='admin' and name_module='viewer' and name_action='view';

INSERT INTO collections VALUES
('1','carcinomas','skin','');

INSERT INTO cases VALUES
('1','1164-16','','1');


