/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jasper;

/**
 *
 * @author RauCelis
 */
public class Decoder {
    static {        
        System.loadLibrary("jasper");
        //System.load("C:\\Program Files (x86)\\Java\\jdk1.7.0_67\\bin\\jasper.dll");
    }
    public native int[] decode(byte[] bufjpc, int len, int w, int h);
}
