/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.unal.jp2k.util;

import edu.unal.jp2k.entities.Packet;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author German
 */
public class CodestreamGenerator {

    /**
     * Crea código para el encabezado de un bloque
     * @param index Índice del bloque
     * @param packLen Longitud de los bytes para dicho bloque
     * @return bytes del encabezado del bloque
     * @throws IOException 
     */
    private static byte[] getTileCode(int index, int packLen) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        int psot = packLen + 2 + 12;                  // packet_lenght + EOC + Lsot

        baos.write(charToBytesArray(0xFF90));   // SOT
        baos.write(charToBytesArray(0xA));      // Lsot
        baos.write(charToBytesArray(index));     // Isot
        baos.write(intToBytesArray(psot));      // Psot

        baos.write(0);                          // TPsot
        baos.write(1);                          // TNsot
        baos.write(charToBytesArray(0xFF93));   // SOD

        return baos.toByteArray();
    }

    /**
     * Genera un flujo de datos codificados (codestream)
     * @param lst lista de todos los paquetes existentes
     * @param coord coordenadas de la región solicitada
     * @param header encabezado de la imagen
     * @param packs lista con los paquetes necesarios
     * @param l número de capas de calidad
     * @param maxr máximo número de niveles de resolución
     * @param r número de capas de calidad
     * @return
     * @throws IOException 
     */
    public static byte[] generate(Packet[] lst, int w, int h, byte[] header,
            int[] packs, int l, int maxr, int r) throws IOException {
        ByteArrayOutputStream cdst = new ByteArrayOutputStream();
        ByteArrayOutputStream tmp = new ByteArrayOutputStream();
        byte[] nhd = transcodeHeader(header, w,h, l, maxr, r);
        byte[] dataBytes;
        byte[] pktBytes;
        int size;

        cdst.write(nhd);
        size=packs.length;
        for (int i = 0; i < size; i++) {
            pktBytes = (lst[i]).getBytes();            
            setPcktIndex(pktBytes,i);
            tmp.write(pktBytes);
        }
        dataBytes = tmp.toByteArray();
        cdst.write(getTileCode(0, dataBytes.length));
        cdst.write(dataBytes);
        cdst.write(charToBytesArray(0xFFD9));
        return cdst.toByteArray();
    }
    
    /**
     * Modifica el indice del paquete para ofrecer mayor compatibilidad con 
     * decodificadores
     * @param bytes
     * @param ind 
     */
    private static void setPcktIndex(byte[] bytes, int ind){
        bytes[4] = (byte) (ind >> 8);
        bytes[5] = (byte) (ind);
    }

    /**
     * Genera una versión modificada del encabezado a partir de los parámetros
     * de ancho, alto, resolución y calidad de la imagen.
     * @param header Encabezado principal de la imagen
     * @param w Ancho de la imagen
     * @param h Altura de la imagen
     * @param l Número de capas de calidad de la imagen
     * @param maxr Número máximo de resoluciones de la imagen original
     * @param r Número de niveles de resolución de la imagen
     * @return Nuevo encabezado principal
     * @throws IOException 
     */
    private static byte[] transcodeHeader(byte[] header, int w,
            int h, int l, int maxr, int r) throws IOException {
        //int rate = (int) Math.pow(2, maxr - r);
        byte[] width = intToBytesArray(w);
        byte[] height = intToBytesArray(h);

        //Xsiz
        for (int i = 8; i < 12; i++) {
            header[i] = width[i - 8];
        }
        //Ysiz
        for (int i = 12; i < 16; i++) {
            header[i] = height[i - 12];
        }

        //num_lay
        for (int i = 0; i < header.length; i++) {
            if (header[i] == -1 && header[i + 1] == 0x52) {
                header[i + 6] = (byte) ((l + 1) >>> 8);
                header[i + 7] = (byte) (l + 1);
                break;
            }
        }

        if (maxr != r) {
            int aux, sb, i = 0, j = 0, numQCC = 0;
            byte[] nhd;

            numQCC=calcNumQCC(header);
            
            nhd = new byte[header.length - ((3 * numQCC + 4) * (maxr - r))];
            sb = 3 * r + 1;

            while (i < header.length) {
                nhd[j] = header[i];
                if (header[i] == -1) {
                    i++;
                    j++;
                    nhd[j] = header[i];
                    switch (header[i]) {
                        case 0x52: //COD
                            //Lcod 
                            aux = 13 + r;
                            nhd[j + 1] = (byte) (aux >>> 8);
                            nhd[j + 2] = (byte) aux;
                            //Scod
                            nhd[j + 3] = header[i + 3];
                            //SGcod
                            nhd[j + 4] = header[i + 4];         //prog_order
                            nhd[j + 5] = header[i + 5];         //num_layers
                            nhd[j + 6] = header[i + 6];
                            nhd[j + 7] = header[i + 7];         //mult_cmp_transf
                            //SPcod
                            nhd[j + 8] = (byte) r;              //desc_lev
                            nhd[j + 9] = header[i + 9];         //cb_w
                            nhd[j + 10] = header[i + 10];       //cb_h
                            nhd[j + 11] = header[i + 11];       //cb_style
                            nhd[j + 12] = header[i + 12];       //transf
                            for (int k = 13; k <= r + 13; k++) { //prec_size
                                nhd[j + k] = header[i + k];
                            }

                            j += 13 + r;
                            i += 13 + maxr;

                            break;
                        case 0x5C: //QCD
                            //Lqcd
                            aux = 4 + 3 * r;
                            nhd[j + 1] = (byte) (aux >>> 8);
                            nhd[j + 2] = (byte) aux;
                            //Sqcd
                            nhd[j + 3] = header[i + 3];
                            //SPqcd
                            for (int k = 4; k <= sb + 4; k++) {
                                nhd[j + k] = header[i + k];
                            }

                            j += 4 + 3 * r;
                            i += 4 + 3 * maxr;

                            break;
                        case 0x5D: //QCC
                            //Lqcd
                            aux = 5 + 3 * r;
                            nhd[j + 1] = (byte) (aux >>> 8);
                            nhd[j + 2] = (byte) aux;
                            //Cqcc
                            nhd[j + 3] = header[i + 3];
                            //Sqcc
                            nhd[j + 4] = header[i + 4];
                            //SPqcc
                            for (int k = 5; k <= sb + 5; k++) {
                                nhd[j + k] = header[i + k];
                            }

                            j += 5 + 3 * r;
                            i += 5 + 3 * maxr;
                            break;
                    }
                }
                i++;
                j++;
            }
            return nhd;
        }
        return header;
    }

    /**
     * Calcula el número de marcadores QCC (FF5D) presentes en el encabezado
     * de la imagen.
     * @param hd Encabezado de la imagen
     * @return Ocurrencias del marcador QCC
     */
    private static int calcNumQCC(byte[] hd) {
        int numQCC=0;
        for (int k = 0; k < hd.length; k++) {
            if (hd[k] == -1 && hd[k + 1] == 0x5D) {
                numQCC++;
                k++;
            }
        }
        return numQCC;
    }

    private static byte[] charToBytesArray(int value) {
        return new byte[]{(byte) (value >>> 8), (byte) (value)};
    }

    private static byte[] intToBytesArray(int value) {
        return new byte[]{(byte) (value >>> 24), (byte) (value >>> 16), (byte) (value >>> 8), (byte) (value)};
    }
}
