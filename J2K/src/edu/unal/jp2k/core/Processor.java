/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.unal.jp2k.core;

import edu.unal.jp2k.entities.PrecROI;
import edu.unal.jp2k.entities.Packet;
import edu.unal.jp2k.util.CodestreamGenerator;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;

public class Processor {

    private static final int ALL = -1;
    private static final String imgInfoFile = "imgInfo";
    private static String imageDir;
    private static String indexDir;
    private static final String imgPktFile = "img_";
    private static final String idxExt = ".idx";
    
    final private String name;                  // Name of the file
    final private int width;                    // Image Width
    final private int height;                   // Image Height		
    //final private int progOrder;		// Progression order id (0=LRCP, 1=RLCP, 2=RPCL, 3=PCRL, 4=CPRL)
    final private int[] numPrecX;               // Precincts number in X for resolution i
    final private int[] numPrecY;               // Precincts number in Y for resolution i
    //final private int numPacketsTile;		// Packets number for each tile
    final private int numPackets;		// Total packets number
    final private int tx;                       // Tiles Number in X
    final private int ty;			// Tiles Number in Y
    final private int numCmp;			// Number of components
    final private int numLev;			// Number of resolution levels 
    final private int numLay;			// Number of layers for each tile
    final private int endMainHeaderPos;         // Position in bytes of the main header end
    //final private int codestreamSize;         // Number of bytes in the codestream
    final private int partition;                // Index partition size
    
    private byte[] header;                      // The image header

    public Processor(String imageDir, String indexDir, String imageName) throws IOException {        
        Processor.imageDir = imageDir;
        Processor.indexDir = indexDir;
        
        System.out.println("Requesting image info...");        
        String[] aux;
        int[] numPrecRes;
        int numRes, j, numTiles, numPacketsTile, numPrec;
        
        FileReader fr = new FileReader(indexDir + imageName + "/" + imgInfoFile);
        BufferedReader br = new BufferedReader(fr);

        name = imageName;
        
        //Image info block        
        aux = br.readLine().split(" ");
        width = Integer.parseInt(aux[0]);
        height = Integer.parseInt(aux[1]);
        br.readLine();//progOrder = Integer.parseInt(br.readLine());
        
        aux = br.readLine().split(" ");        
        tx = (int) Math.ceil(width / Double.parseDouble(aux[0]));
        ty = (int) Math.ceil(height / Double.parseDouble(aux[1]));
        numCmp = Integer.parseInt(br.readLine());
        numLay = Integer.parseInt(br.readLine());
        numLev = Integer.parseInt(br.readLine());
        
        aux = br.readLine().split(" ");
        numRes = numLev + 1;
        
        numPrec = 0;
        numPrecX = new int[numRes];
        numPrecY = new int[numRes];
        numPrecRes = new int[numRes];
        
        j = 0;
        for (int i = 0; i < numRes; i++) {
            numPrecX[i] = Integer.parseInt(aux[j]);
            numPrecY[i] = Integer.parseInt(aux[j + 1]);
            numPrecRes[i] = numPrecX[i] * numPrecY[i];
            //numPrec += numPrecRes[i];
            j += 2;
        }

        endMainHeaderPos = Integer.parseInt(br.readLine());
        partition = Integer.parseInt(br.readLine());

        calculateImageHeader();
        numTiles = ty * tx;
        numPacketsTile = numCmp * numLay * numPrec;	//Packets number per tile
        numPackets = numPacketsTile * numTiles;

        br.close();
        fr.close();
    }

    public int getDim(int numLev, int lon, int r) {
        return lon / ((int) Math.pow(2, numLev - r));
    }

    public Packet[] getPackets(String name, int[] list, int partition) {
        byte[] bytes;
        int ind = -1;
        int id, i = 0;
        double startPos, endPos;
        Packet[] packets = new Packet[list.length];
        
        try {
            RandomAccessFile file = new RandomAccessFile(imageDir + name, "r");
            double[][] packs = null;

            while (i < list.length) {
                id = list[i];
                if (ind != id / partition) {
                    ind = id / partition;
                    packs = loadPackets(ind, partition);
                }
                if (packs != null) {
                    int val = id - (partition * ind);
                    startPos = packs[val][0];
                    endPos = packs[val][1];
                    bytes = new byte[(int) (endPos - startPos + 1)];
                    file.seek((long) startPos);
                    file.read(bytes);
                    packets[i] = new Packet(id, bytes);
                }
                i++;
            }
            file.close();
        } catch (Exception e) {
            System.out.println("Error al recuperar paquetes");
        }
        return packets;
    }

    private int getValidRes(int r) {
        return r > numLev ? numLev : r < 0 ? 0 : r;
    }

    private int getValidLay(int l) {
        return l > numLay - 1 ? numLay - 1 : l < 0 ? 0 : l;
    }

    public byte[] requestRegion(int[] coord, int l, int r)
            throws IOException, ClassNotFoundException {
        int w, h;

        l = getValidLay(l);
        r = getValidRes(r);
        int[] needed = calcPacketsForRequest(coord, ALL, l, r, ALL);

        w = getDim(numLev, coord[2] - coord[0], r);
        h = getDim(numLev, coord[3] - coord[1], r);
        
        Packet[] lstPkts = getPackets(name, needed, partition);
        return CodestreamGenerator.generate(lstPkts, w, h, header,
                needed, l, numLev, r);
    }

    /**
     * Calcula el precinto inicial (superior izquierdo) y final (inferior
     * derecho) para una región
     *
     * @param xy Coordenadas (x0,y0,x1,y1)
     * @param precROI Estructura para almacenar la información de los precintos
     * @param res Resolución solicitada
     * @return
     */
    private void coord2Prec(int[] xy, PrecROI[] precROI, int res) {
        if (xy[0] < 0) {
            xy[0] = 0;
        }
        if (xy[1] < 0) {
            xy[1] = 0;
        }
        if (xy[2] > width) {
            xy[2] = width;
        }
        if (xy[3] > height) {
            xy[3] = height;
        }

        for (int i = 0; i <= res; i++) {

            xy[0] = (int) Math.floor((double) xy[0] * numPrecX[i] / width) * (width / numPrecX[i]);
            xy[1] = (int) Math.floor((double) xy[1] * numPrecY[i] / height) * (height / numPrecY[i]);
            xy[2] = (int) Math.ceil((double) xy[2] * numPrecX[i] / width) * (width / numPrecX[i]);
            xy[3] = (int) Math.ceil((double) xy[3] * numPrecY[i] / height) * (height / numPrecY[i]);

            precROI[i].tl = (xy[0] * numPrecX[i] / width) + (xy[1] * numPrecY[i] / height) * numPrecX[i];
            precROI[i].bl = (xy[0] * numPrecX[i] / width) + (xy[3] * numPrecY[i] / height - 1) * numPrecX[i];
            precROI[i].tr = (xy[2] * numPrecX[i] / width) - 1 + (xy[1] * numPrecY[i] / height) * numPrecX[i];
            precROI[i].br = (xy[2] * numPrecX[i] / width) - 1 + (xy[3] * numPrecY[i] / height - 1) * numPrecX[i];
        }
    }

    /**
     * Calcula los paquetes necesarios para satisfacer la solicitud y los que
     * deben ser solicitados al servidor
     *
     * @param coord Coordenadas de la región solicitada
     * @param tiles Bloques solicitados
     * @param layers Capas solicitadas
     * @param resolutions Resoluciones solicitadas
     * @param components Componentes solicitados
     * @return Arreglo con los índices de los paquetes necesarios
     */
    private int[] calcPacketsForRequest(int[] coord, int tiles,
            int layers, int resolutions, int components) {

        PrecROI[] precROI = new PrecROI[resolutions + 1];
        boolean c1, c2, c3, c4;
        int dx;
        ArrayList<Integer> missing = new ArrayList<>();

        for (int i = 0; i < precROI.length; i++) {
            precROI[i] = new PrecROI();
        }
        if ((coord[0] > 0) || (coord[1] > 0) || (coord[2] > 0) || (coord[3] > 0)) {
            coord2Prec(coord, precROI, resolutions);
        }

        c1 = tiles == ALL;
        c2 = layers == ALL;
        c3 = resolutions == ALL;
        c4 = components == ALL;

        int precX;
        int num = 0;
        int numTiles = tx * ty;

        int numPrec;
        int lgral, cgral, startLine;
        for (int t = 0; t < numTiles; t++) {
            for (int l = 0; l < numLay; l++) {
                for (int r = 0; r < numLev + 1; r++) {
                    for (int c = 0; c < numCmp; c++) {
                        numPrec = numPrecX[r] * numPrecY[r];
                        for (int p = 0; p < numPrec; p++) {
                            if ((t <= tiles || c1) && (l <= layers || c2) && (r <= resolutions || c3) && (c <= components || c4)) {
                                lgral = (t / tx) * numPrecX[r] + p / numPrecX[r];
                                cgral = (t % tx) * numPrecX[r] + p % numPrecX[r];
                                int pg = numPrecX[r] * tx * lgral + cgral;
                                if (pg >= precROI[r].tl && pg <= precROI[r].br) {
                                    precX = numPrecX[r] * tx;
                                    dx = precROI[r].tr - precROI[r].tl;
                                    //dy = precROI[r].bl / numPrecX - precROI[r].tl / numPrecX;
                                    startLine = precROI[r].tl + (int) Math.floor((pg - precROI[r].tl) / precX) * precX;
                                    if (pg >= startLine && pg <= startLine + dx) {
                                        //packs.add(num);
                                        //if (lstPcks[num] == null) {
                                        missing.add(num);
                                        //}
                                    }
                                }
                            }
                            num++;
                        }
                    }
                }
            }
        }
        int[] mpacks = new int[missing.size()];
        for (int i = 0; i < mpacks.length; i++) {
            mpacks[i] = missing.get(i);
        }
        return mpacks;
    }

    /**
     * Realiza la solicitud del encabezado de la imagen precargada.
     *
     * @throws IOException
     */
    public final void calculateImageHeader() throws IOException {
        System.out.println("Requesting image header...");
        File file = new File(imageDir + name);
        
        header = new byte[endMainHeaderPos + 1];
        BufferedInputStream in = new BufferedInputStream(new FileInputStream(file));
        in.read(header, 0, endMainHeaderPos + 1);
        in.close();
    }

    /**
     * Solicita la descarga de las minituaras de las imágenes
     *
     * @return Arreglo con las minaturas solicitadas
     * @throws java.io.IOException
     */
    public String[] getThumbList(String thumbDir) throws IOException {
        File[] files = (new File(thumbDir)).listFiles();
        String[] thumbs = new String[files.length];
        for (int i = 0; i < files.length; i++) {
            thumbs[i] = files[i].getName();
        }
        return thumbs;
    }

    public double[][] loadPackets(int ind, int partition) {
        double[][] packs = null;
        try {
            FileReader fr = new FileReader(indexDir + name
                    + File.separator + imgPktFile + ind + idxExt);

            BufferedReader br = new BufferedReader(fr);
            int numFiles = (int) Math.ceil((double) numPackets / (double) partition);
            int totPackets = ind + 1 == numFiles ? numPackets % partition : partition;
            String[] aux;
            packs = new double[totPackets][2];
            for (int i = 0; i < totPackets; i++) {
                String line = br.readLine();
                if (line != null) {
                    aux = line.split(" ");
                    packs[i][0] = Double.parseDouble(aux[6]);//pkt start position
                    packs[i][1] = Double.parseDouble(aux[7]);//pckt end position
                }
            }
            br.close();
            fr.close();
        } catch (IOException e) {
            System.out.println("Error al cargar paquetes: " + e);
        }

        return packs;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getNumLev() {
        return numLev;
    }
    
    public String getName() {
        return name;
    }
}