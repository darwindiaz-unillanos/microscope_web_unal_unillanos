package edu.unal.jp2k.entities;

public class PrecROI {
    public int tl;     //precinct number at bottom-right position
    public int tr;     //precinct number at top-right position
    public int bl;     //precinct number at bottom-left position
    public int br;     //precinct number at top-left position
}
