package edu.unal.jp2k.entities;

import java.io.Serializable;

public class ImageInfo implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    private String name;                // Image Name
    private int width;                  // Image Width
    private int height;                 // Image Height		
    private int progOrder;		// Progression order id (0=LRCP, 1=RLCP, 2=RPCL, 3=PCRL, 4=CPRL)
    private int[] numPrecX;             // Precincts number in X for resolution i
    private int[] numPrecY;             // Precincts number in Y for resolution i
    private int numPacketsTile;		// Packets number for each tile
    private int totPackets;		// Total packets number
    private int tx;                     // Tiles Number in X
    private int ty;			// Tiles Number in Y
    private int numCmp;			// Number of components
    private int numLev;			// Number of resolution levels 
    private int numLay;			// Number of layers for each tile
    private int endMainHeaderPos;	// Position in bytes of the main header end
    private int codestreamSize;         // Number of bytes in the codestream
    private int partition;              // Index partition size

    /* Getters */
    public String getName() {
        return name;
    }

    public int getPartition() {
        return partition;
    }
    
    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getProgOrder() {
        return progOrder;
    }

    public int[] getNumPrecX() {
        return numPrecX;
    }

    public int[] getNumPrecY() {
        return numPrecY;
    }

    public int getNumPacketsTile() {
        return numPacketsTile;
    }

    public int getTotPackets() {
        return totPackets;
    }

    public int getTx() {
        return tx;
    }

    public int getTy() {
        return ty;
    }

    public int getNumCmp() {
        return numCmp;
    }

    public int getNumLev() {
        return numLev;
    }

    public int getNumLay() {
        return numLay;
    }

    public int getEndMainHeaderPos() {
        return endMainHeaderPos;
    }

    public int getCodestreamSize() {
        return codestreamSize;
    }
    
    /* Setters */
    public void setName(String name) {
        this.name = name;
    }

    public void setPartition(int partition) {
        this.partition = partition;
    }
    
    public void setWidth(int width) {
        this.width = width;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void setProgOrder(int progOrder) {
        this.progOrder = progOrder;
    }

    public void setNumPrecX(int[] numPrecX) {
        this.numPrecX = numPrecX;
    }

    public void setNumPrecY(int[] numPrecY) {
        this.numPrecY = numPrecY;
    }

    public void setNumPacketsTile(int numPacketsTile) {
        this.numPacketsTile = numPacketsTile;
    }

    public void setTotPackets(int totPackets) {
        this.totPackets = totPackets;
    }

    public void setTx(int tx) {
        this.tx = tx;
    }

    public void setTy(int ty) {
        this.ty = ty;
    }

    public void setNumCmp(int numCmp) {
        this.numCmp = numCmp;
    }

    public void setNumLev(int numLev) {
        this.numLev = numLev;
    }

    public void setNumLay(int numLay) {
        this.numLay = numLay;
    }

    public void setEndMainHeaderPos(int endMainHeaderPos) {
        this.endMainHeaderPos = endMainHeaderPos;
    }

    public void setCodestreamSize(int codestreamSize) {
        this.codestreamSize = codestreamSize;
    }
    
    public void dispose() {
        this.numPrecX = null;
        this.numPrecY = null;
    }
}
