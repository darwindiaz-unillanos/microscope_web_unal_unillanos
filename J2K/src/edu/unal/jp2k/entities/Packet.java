package edu.unal.jp2k.entities;

public class Packet {
    private int index;
    private byte[] bytes;
    
    public Packet(){}
    
    public Packet(int ind,byte[] bytes){
        index=ind;        
        this.bytes=bytes;                
    }

    public int getIndex() {
        return index;
    }

    public byte[] getBytes() {
        return bytes;
    }    
}