/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.unal.wsi.servlets;

import edu.unal.wsi.models.DBQuery;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import org.apache.commons.io.IOUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;;

/**
 *
 * @author Telemedicina
 */

@MultipartConfig
public class WSIViewer extends HttpServlet {

    private static final String parent = "/SlideViewer/";
    private static DBQuery query = new DBQuery();
    private HttpSession session;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String requestURI=request.getRequestURI();
        if(requestURI.equals(parent + "viewer")) {
            request.setAttribute("wsi", request.getParameter("wsi"));
            request.setAttribute("reader", request.getParameter("reader"));
            request.setAttribute("uid", request.getParameter("uid"));
            request.setAttribute("play", request.getParameter("play"));
            
            request.getRequestDispatcher("viewer.jsp").forward(request, response);
        }else if(requestURI.equals(parent + "list")) {
            request.setAttribute("cat", request.getParameter("cat"));
            request.getRequestDispatcher("list.jsp").forward(request, response);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) 
            throws ServletException, IOException {
        //String text = req.getParameter("text");
        //System.out.println("Text: " + text);
        String requestURI=req.getRequestURI();
        if(requestURI.equals(parent + "load_annotations")) {
            Part filePart = req.getPart("file");
            InputStream inputStream = filePart.getInputStream();
            StringWriter writer = new StringWriter();
            IOUtils.copy(inputStream, writer, "utf-8");
            String jsonString = writer.toString();

            resp.setHeader("Content-type", "application/json");
            resp.getWriter().write(jsonString);
        }else if(requestURI.equals(parent + "load_annotationsdb")){
            session = req.getSession();
            Object dataCase[] = {req.getParameter("image")};
            ArrayList<String> result = query.loadCase(dataCase);
            int caseId = Integer.parseInt(result.get(0));
            int userId = Integer.parseInt(session.getAttribute("id").toString());
            if(req.getParameter("role").equals("pathologist")){
                Object dataAnnotation[] = {
                    userId,
                    caseId
                };
                result = query.loadAnnotations(dataAnnotation);
            }else if(req.getParameter("role").equals("student")){
                Object dataAnnotation[] = {
                    caseId
                };
                result = query.loadAnnotationsAll(dataAnnotation);
            }
            resp.setHeader("Content-type", "application/json");
            resp.getWriter().write(dataToJSON(result));
        }else if(requestURI.equals(parent + "save_annotations")){
            JSONParser jsonParser = new JSONParser();
            try {
                session = req.getSession();
                Object dataCase[] = {req.getParameter("image")};
                ArrayList<String> result = query.loadCase(dataCase);
                int caseId = Integer.parseInt(result.get(0));
                int userId = Integer.parseInt(session.getAttribute("id").toString());
                Object dataAnnotations [] ={
                    caseId, 
                    userId
                };
                
                if(query.deleteAnnotations(dataAnnotations)){
                    JSONArray annotations = (JSONArray) jsonParser.parse(req.getParameter("annotations"));                
                    if(this.saveAnnotationsToDataBase(annotations, userId, caseId)){
                        resp.getWriter().write("Success");   
                    }else{
                        resp.getWriter().write("Error");   
                    }
                }
            } catch (ParseException ex) {
                throw new RuntimeException("Error while saving annotations: " + ex.getMessage(), ex);
            }
        }else if(requestURI.equals(parent + "privileges")){
            String [] param = req.getParameter("roles").split(",");
            ArrayList<String> privileges = new ArrayList<String>();
            JSONArray jsonArray = new JSONArray();
            session = req.getSession();
            jsonArray = queryPrivilegesRoles(0, param, "viewer", jsonArray);
            resp.getWriter().write(jsonArray.toJSONString());
        }        
    }
    
    private JSONArray queryPrivilegesRoles(int id,String [] param, String module, JSONArray jsonArray){
        if(id < param.length){
            Object dataRole[] ={param[id], module};
            ArrayList<String> result = query.loadRolesModuleActions(dataRole);
            for(String r : result){
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("privilege",r);
                if(jsonArray.indexOf(jsonObject) == -1){
                    jsonArray.add(jsonObject);
                }
            }
            id++;
            queryPrivilegesRoles(id, param, module, jsonArray);
        }
        return jsonArray;
    }
    
    private boolean saveAnnotationsToDataBase(JSONArray annotations, int userId, int caseId){
        boolean status;
        if(annotations.size() > 0){
            JSONObject jsonObject = (JSONObject) annotations.get(0);
            Object dataNextVal [] = {"annotations_id_anno_seq"};
            
            Object data [] = {
                query.nextVal(dataNextVal),
                (String)jsonObject.get("annotation"),
                (String)jsonObject.get("color"),
                (Double)jsonObject.get("createdAtZoom"),
                String.valueOf(jsonObject.get("regionDefinition")),
                String.valueOf(jsonObject.get("regionType")),
                caseId,
                userId
            };
            if(query.saveAnnotations(data)){
                annotations.remove(0);
                status = saveAnnotationsToDataBase(annotations, userId, caseId);
            }else{
                return false;
            }
        }else{
            status = true;
        }
        return status;
    }
    
    private String dataToJSON (ArrayList<String> data){
        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject;
        JSONParser jsonParser = new JSONParser();
        JSONArray jsonObjectRegion;
        int sizeArray = data.size()/5;
        for (int i = 0; i < sizeArray; i++) {
            jsonObject = new JSONObject();
            try{
                jsonObject.put("annotation",data.get(i*5));
                jsonObject.put("color",data.get((i*5)+1));
                jsonObject.put("createdAtZoom",Double.parseDouble(data.get((i*5)+2)));
                jsonObjectRegion = (JSONArray) jsonParser.parse(data.get((i*5)+3));
                jsonObject.put("regionDefinition", jsonObjectRegion);
                jsonObject.put("regionType",Integer.parseInt(data.get((i*5)+4)));   
                jsonArray.add(jsonObject);
            } catch (ParseException ex) {
                throw new RuntimeException("Error while saving annotations- " + ex.getMessage(), ex);
            }
        }
        return jsonArray.toJSONString();
    }
    /*private static String getSubmittedFileName(Part part) {
        for (String cd : part.getHeader("content-disposition").split(";")) {
            if (cd.trim().startsWith("filename")) {
                String fileName = cd.substring(cd.indexOf('=') + 1).trim().replace("\"", "");
                return fileName.substring(fileName.lastIndexOf('/') + 1).substring(fileName.lastIndexOf('\\') + 1); // MSIE fix.
            }
        }
        return null;
    }*/
}