/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.unal.wsi.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import edu.unal.wsi.models.DBQuery;
import java.security.Key;
import java.util.ArrayList;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import sun.misc.BASE64Encoder;

/**
 *
 * @author darwi
 */
public class Login extends HttpServlet {
    
    HttpSession session;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String page_result;

        try{
            if(session == null)
                session = request.getSession(true);
            String email = request.getParameter("email");
            String password = request.getParameter("password");

            Object data[] = {email};            
            DBQuery query = new DBQuery();
            ArrayList<String> result = query.singIn(data);
            if(result != null){
                if(encrypt(password).equals(result.get(1))){                    
                    session.setAttribute("user", email);
                    session.setAttribute("id", result.get(0));
                    Object idUser[] = {Integer.valueOf(result.get(0))};
                    
                    result.clear();
                    result = query.userRoles(idUser);
                    for (int i = result.size()-2; i >= 0; i -= 2){
                        result.remove(i);
                    }
                    session.setAttribute("roles", result);
                    session.removeAttribute("error");
                    page_result = "index.jsp";
                }else{
                    session.setAttribute("error", "invalid_user");
                    page_result = "login.jsp";
                }
            }else{
                    session.setAttribute("error", "does_not_user");
                    page_result = "login.jsp";
            }
            response.sendRedirect(page_result);
        }catch(Exception ex){
            throw new RuntimeException("Error validing user  - " + ex.getMessage(), ex);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private Key generateKey(String key) throws Exception{
            byte [] keyValue = key.getBytes();
            return new SecretKeySpec(keyValue, "AES");
    }
    
    private String encrypt (String password){ 
        try{
            String encryptedValue = "";
            if(password != null){
                Key key = generateKey("microscopy2018v2");
                Cipher cipher = Cipher.getInstance("AES");
                cipher.init(Cipher.ENCRYPT_MODE, key);    
                byte[] encVal = cipher.doFinal(password.getBytes());
                encryptedValue = new BASE64Encoder().encode(encVal);
            }
            return encryptedValue;
        }catch(Exception ex){
            throw new RuntimeException("Error encripting key - " + ex.getMessage(), ex);
        }
    }
}
