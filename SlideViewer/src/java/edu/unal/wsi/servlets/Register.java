/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.unal.wsi.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import edu.unal.wsi.models.DBQuery;
import java.security.Key;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpSession;
import sun.misc.BASE64Encoder;

/**
 *
 * @author darwi
 */
public class Register extends HttpServlet {

    HttpSession session;
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            if(session == null)
                session = request.getSession(true);
            DBQuery query = new DBQuery();
            
            String userEmail = request.getParameter("email");
            String userPassword = encrypt(request.getParameter("password"));
            Calendar date = Calendar.getInstance();
            
            Object dataNextVal [] = {"users_id_user_seq"};
            Integer userId = query.nextVal(dataNextVal);
            
            Object dataUser [] = {
                userId,
                userEmail,
                userPassword,
                date.getTime()
            };
            
            if(query.registerUser(dataUser)){
                Object dataPathologist [] = {
                    userId,
                    Integer.parseInt(request.getParameter("identification")),
                    request.getParameter("firstname"),
                    request.getParameter("lastname"),
                    request.getParameter("country"),
                    request.getParameter("city"),
                    request.getParameter("zipcode"),
                    request.getParameter("degress"),
                    request.getParameter("institution"),
                    "","","",""
                };
                
                Object dataRole[] = {
                    userId,
                    "guest"
                };
                if(query.registerPatgologists(dataPathologist)){
                    if(query.registerUserRole(dataRole)){
                        response.sendRedirect("index.jsp");
                        session.setAttribute("role", "guest");
                    }
                }else{
                    System.out.println("Error2");
                    response.sendRedirect("login.jsp");
                }
            }else{
                System.out.println("Error1");
                response.sendRedirect("login.jsp");
            }
        }catch(Exception ex){
            throw new RuntimeException("Error Register user  - " + ex.getMessage(), ex);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private Key generateKey(String key) throws Exception{
            byte [] keyValue = key.getBytes();
            return new SecretKeySpec(keyValue, "AES");
    }
    
    private String encrypt (String password){ 
        try{
            Key key = generateKey("microscopy2018v2");
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.ENCRYPT_MODE, key);    
            byte[] encVal = cipher.doFinal(password.getBytes());
            String encryptedValue = new BASE64Encoder().encode(encVal);
            return encryptedValue;
        }catch(Exception ex){
            throw new RuntimeException("Error encrepting key - " + ex.getMessage(), ex);
        }    
    }
}
