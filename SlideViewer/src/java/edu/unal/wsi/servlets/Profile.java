/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.unal.wsi.servlets;

import edu.unal.wsi.models.DBQuery;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
/**
 *
 * @author darwi
 */
public class Profile extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        ArrayList<String> roles;
        ArrayList<String> organs;
        ArrayList<String> specialty;

        JSONArray jsonArrayRoles = new JSONArray();
        JSONArray jsonArrayOrgans = new JSONArray();
        JSONArray jsonArraySpecialty = new JSONArray();
        
        JSONObject jsonObject = new JSONObject();
        JSONObject jsonObjectNode;
        
        DBQuery query = new DBQuery();
        Object userId = request.getParameter("user");
        
        if(userId.equals("all")){
            ArrayList<String> user;
            JSONArray jsonArrayUser = new JSONArray();
            
            user = query.userAllQuery();
            Iterator iter = user.iterator();
            while(iter.hasNext()){
                jsonObjectNode = new JSONObject();
                jsonObjectNode.put("id", iter.next());
                jsonObjectNode.put("email", iter.next());
                jsonArrayUser.add(jsonObjectNode);   
            }
            jsonObject.put("usersAll", jsonArrayUser);
        }
        else if(Integer.parseInt(userId.toString()) == -1){
            roles = query.rolesQuery();
            Iterator iter = roles.iterator();
            while(iter.hasNext()){
                jsonObjectNode = new JSONObject();
                jsonObjectNode.put("id", iter.next());
                String dat = iter.next().toString();
                jsonObjectNode.put("text", dat);
                jsonObjectNode.put("value", dat);
                jsonArrayRoles.add(jsonObjectNode);   
            }
            jsonObject.put("roles", jsonArrayRoles);

            organs = query.organsQuery();
            iter = organs.iterator();
            while(iter.hasNext()){
                jsonObjectNode = new JSONObject();
                jsonObjectNode.put("id", iter.next());
                String dat = iter.next().toString();
                jsonObjectNode.put("text", dat);
                jsonObjectNode.put("value", dat);
                jsonArrayOrgans.add(jsonObjectNode);   
            }
            jsonObject.put("organs", jsonArrayOrgans);

            specialty = query.specialtyQuery();
            iter = specialty.iterator();
            while(iter.hasNext()){
                jsonObjectNode = new JSONObject();
                jsonObjectNode.put("id", iter.next());
                String dat = iter.next().toString();
                jsonObjectNode.put("text", dat);
                jsonObjectNode.put("value", dat);
                jsonArraySpecialty.add(jsonObjectNode);   
            }
            jsonObject.put("specialty", jsonArraySpecialty);    
        }else if(Integer.parseInt(userId.toString()) > 0){
            ArrayList<String> user;
            
            Object data[] = {Integer.parseInt(userId.toString())};
            user = query.userQuery(data);
            jsonObjectNode = new JSONObject();
            for (int i = 0; i < user.size();i++) {    
                jsonObjectNode.put("user"+i, user.get(i));
            }
            jsonObject.put("user", jsonObjectNode);
            
            roles = query.userRoles(data);
            if(roles != null){
                for (int i = 0; i < roles.size()/2;i++) {    
                    jsonObjectNode = new JSONObject();
                    jsonObjectNode.put("id", roles.get(i*2));
                    jsonObjectNode.put("role", roles.get((i*2)+1));
                    jsonArrayRoles.add(jsonObjectNode);
                }
                jsonObject.put("userRoles", jsonArrayRoles);
            }
            
            organs = query.organsUserQuery(data);
            if(organs != null){
                for (int i = 0; i < organs.size();i++) {    
                    jsonObjectNode = new JSONObject();
                    jsonObjectNode.put("id", organs.get(i));
                    jsonArrayOrgans.add(jsonObjectNode);
                }
                jsonObject.put("userOrgans", jsonArrayOrgans);
            }
            
            specialty = query.specialtyUserQuery(data);
            if(specialty != null){
                for (int i = 0; i < specialty.size();i++) {    
                    jsonObjectNode = new JSONObject();
                    jsonObjectNode.put("id", specialty.get(i));
                    jsonArraySpecialty.add(jsonObjectNode);
                }
                jsonObject.put("userSpecialty", jsonArraySpecialty);
            }
        }
        response.setCharacterEncoding("utf-8");
        response.getWriter().write(jsonObject.toJSONString());
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        DBQuery query = new DBQuery();
        int idUser = Integer.parseInt(request.getParameter("id_user"));
        int idPathologist = Integer.parseInt(request.getParameter("identification"));
        Object dataPathologist [] = {
            request.getParameter("firstname"),
            request.getParameter("lastname"),
            request.getParameter("country"),
            request.getParameter("city"),
            request.getParameter("zipcode"),
            request.getParameter("degress"),
            request.getParameter("institution"),
            request.getParameter("phone_number"),
            request.getParameter("mobile_number"),
            request.getParameter("occupation"),
            request.getParameter("language"),
            idPathologist,
        };
        
        
        if(query.updatePatgologists(dataPathologist)){
            int idx = 0;
            String roles[] = request.getParameterValues("roles");
            String organs[] = request.getParameterValues("organs");
            String specialty[] = request.getParameterValues("specialty");
            
            Object dataRoles [] = new Object[roles.length*2];
            Object dataOrgans [] = new Object[organs.length*2];
            Object dataSpecialty [] = new Object[specialty.length*2];
            
            for (int i = 0; i < roles.length; i++) {
                dataRoles[idx] = idUser;
                dataRoles[idx+1] = Integer.parseInt(roles[i]);
                idx += 2;
            }
            idx = 0;
            for (int i = 0; i < organs.length; i++) {
                dataOrgans[idx] = idPathologist;
                dataOrgans[idx+1] = Integer.parseInt(organs[i]);
                idx += 2;
            }
            idx = 0;
            for (int i = 0; i < specialty.length; i++) {
                dataSpecialty[idx] = idPathologist;
                dataSpecialty[idx+1] = Integer.parseInt(specialty[i]);
                idx += 2;
            }
            
            if(query.updateUserRoles(idUser, dataRoles)){
                if(query.updateUserOrgans(idPathologist, dataOrgans)){
                    if(query.updateUserSpecialty(idPathologist, dataSpecialty)){
                        System.out.println("Exito");
                    }else
                        System.out.println("Error");
                }else
                    System.out.println("Error");
            }else
                System.out.println("Error");
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
