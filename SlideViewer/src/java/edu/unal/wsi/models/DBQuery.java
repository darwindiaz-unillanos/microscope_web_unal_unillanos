
package edu.unal.wsi.models;

/**
 *
 * @author darwi
 */

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;

public class DBQuery extends DBConnection{
    private PreparedStatement pst;
    private ResultSet rs;
    private ResultSetMetaData rsmd;
    private String sql;
    ArrayList<String> result;
    
    public DBQuery(){
        pst = null;
        rs = null;
        sql = null;
        result = new ArrayList<>();
    }
    
    public ArrayList<String> singIn(Object [] data){
        sql = "Select id_user,password_user from users where email_user = ?";
        result = queryWithRetrun(sql, data);
        if(!result.isEmpty())
            return result;
        else
            return null;
    }
    
    public boolean registerUser(Object [] data){
        sql = "Insert into users values (?,?,?,?)";
        return query(sql, data);
    }
    
    public boolean registerPatgologists(Object [] data){
        sql = "Insert into pathologists values (?,?,?,?,?,?,?,?,?,?,?,?,?)";
        return query(sql, data);
    }
    
    public boolean registerUserRole(Object [] data){
        sql = "Insert into users_roles Select ?,id_rol from roles where name_rol = ?";
        System.out.println(sql);
        return query(sql, data);
    }
    
    public ArrayList<String> userRoles(Object [] data){
        sql = "Select ur.id_rol,rol.name_rol from users as us,roles as rol,users_roles as ur "
                + "where us.id_user = ? and us.id_user = ur.id_user and rol.id_rol = ur.id_rol";
        result = queryWithRetrun(sql, data);
        if(!result.isEmpty())
            return result;
        else
            return null;
    }
    
    public ArrayList<String> userAllQuery(){
        sql = "Select id_user,email_user from users";
        result = queryWithRetrun(sql, null);
        if(!result.isEmpty())
            return result;
        else
            return null;
    }
    
    public ArrayList<String> userQuery(Object data[]){
        sql = "Select id_patho,name_patho,lastname_patho,country_patho,city_patho,zipcode_patho,university_degrees_patho,institution_patho,phone_patho,mobile_patho,occupation_patho,language_patho,email_user from pathologists,users "
                + "where id_user = users_id_user and id_user = ?";
        result = queryWithRetrun(sql, data);
        if(!result.isEmpty())
            return result;
        else
            return null;
    }
    
    public ArrayList<String> organsUserQuery(Object data[]){
        sql = "Select org.id_organ from organs as org, pathologists as pa, pathos_organs as po "
                + "where pa.id_patho = po.id_patho and org.id_organ = po.id_organ and users_id_user = ?";
        result = queryWithRetrun(sql, data);
        if(!result.isEmpty())
            return result;
        else
            return null;
    }
    
    public ArrayList<String> specialtyUserQuery(Object data[]){
        sql = "Select sp.id_special from specialty as sp, pathologists as pa, pathos_specialty as ps "
                + "where pa.id_patho = ps.id_patho and sp.id_special = ps.id_special and users_id_user = ?";
        result = queryWithRetrun(sql, data);
        if(!result.isEmpty())
            return result;
        else
            return null;
    }
    
    public ArrayList<String> rolesQuery(){
        sql = "Select id_rol, name_rol from roles";
        result = queryWithRetrun(sql, null);
        if(!result.isEmpty())
            return result;
        else
            return null;
    }
    
    public ArrayList<String> organsQuery(){
        sql = "Select id_organ, name_organ from organs";
        result = queryWithRetrun(sql, null);
        if(!result.isEmpty())
            return result;
        else
            return null;
    }
    
    public ArrayList<String> specialtyQuery(){
        sql = "Select id_special, name_special from specialty";
        result = queryWithRetrun(sql, null);
        if(!result.isEmpty())
            return result;
        else
            return null;
    }
    
    public boolean updatePatgologists(Object [] data){
        sql ="Update pathologists "
                + "set name_patho=?, lastname_patho=?, country_patho=?, city_patho=?, zipcode_patho=?, university_degrees_patho=?, institution_patho=?, phone_patho=?, mobile_patho=?, occupation_patho=?, language_patho=? "
                + "where id_patho=?";
        return query(sql, data);
    }
    
    public boolean updateUserRoles(Integer id,Object [] data){
        sql = "delete from users_roles where id_user = "+ id;
        if(query(sql,null)){
            sql = "Insert into users_roles values ";
            sql = assembleQuery(sql, data.length/2);
            return query(sql,data);
        }else{
            return false;
        }
    }
    
    public boolean updateUserOrgans(Integer id,Object [] data){
        sql = "delete from pathos_organs where id_patho = "+ id;
        System.out.println(sql);
        if(query(sql,null)){
            sql = "Insert into pathos_organs values ";
            sql = assembleQuery(sql, data.length/2);
            return query(sql,data);
        }else{
            return false;
        }
    }
    
    public boolean updateUserSpecialty(Integer id,Object [] data){
        sql = "delete from pathos_specialty where id_patho = "+ id;
        if(query(sql,null)){
            sql = "Insert into pathos_specialty values ";
            sql = assembleQuery(sql, data.length/2);
            return query(sql,data);
        }else{
            return false;
        }
    }    
    
    public  ArrayList<String> loadRolesModuleActions (Object [] data){
        sql = "Select ac.name_action from roles as ro, modules as mo, actions ac, roles_modules_actions rma "
                + "where ro.name_rol = ? and ro.id_rol = rma.id_rol and mo.name_module = ? and mo.id_module = rma.id_module and "
                + "ac.id_action = rma.id_action";
        result = queryWithRetrun(sql, data);
        if(!result.isEmpty())
            return result;
        else
            return null;
    }
    public boolean deleteAnnotations (Object [] data){
        sql = "delete from annotations where id_case = ? and id_user = ?";
        return query(sql, data);
    }
    
    public boolean saveAnnotations(Object [] data){
        sql = "Insert into annotations values (?,?,?,?,?,?,?,?)";
        return query(sql, data);
    }
    
    public ArrayList<String> loadAnnotations (Object [] data){
        sql = "Select description_anno, color_anno, create_at_zoom_anno, region_definition_anno, region_type_anno "
                + "from annotations where id_user = ? and id_case = ?";
        result = queryWithRetrun(sql, data);
        if(!result.isEmpty())
            return result;
        else
            return null;
    }
    
    public ArrayList<String> loadAnnotationsAll (Object [] data){
        sql = "Select description_anno, color_anno, create_at_zoom_anno, region_definition_anno, region_type_anno "
                + "from annotations where id_case = ?";
        result = queryWithRetrun(sql, data);
        if(!result.isEmpty())
            return result;
        else
            return null;
    }
    
    public ArrayList<String> loadCase(Object [] data){
        sql = "Select id_case from cases where name_case = ?";
        result = queryWithRetrun(sql, data);
        if(!result.isEmpty())
            return result;
        else
            return null;
    }
    
    public Integer nextVal(Object [] data){
        sql = "select nextval(?)";
        result = queryWithRetrun(sql, data);
        if(!result.isEmpty())
            return Integer.parseInt(result.get(0));
        else
            return null;
    }
    
    public String assembleQuery(String sql, int sizeValue){
        for (int i = 0; i < sizeValue; i++) {
            if(i == sizeValue-1) 
                sql = sql + "(?,?);";
            else
                sql = sql + "(?,?),";
        }
        return sql;
    }
    
    public ArrayList<String> queryWithRetrun(String sql, Object [] data){
        result.clear();
        try{
            pst = getConnection().prepareStatement(sql);
            if (data != null){
                for (int i = 0; i < data.length; i++) {
                    if(data[i] instanceof java.lang.String){
                        pst.setString(i+1, String.valueOf(data[i]));
                    }else if(data[i] instanceof java.lang.Integer){
                        pst.setInt(i+1, Integer.parseInt(data[i].toString()));
                    }
                }
            }
            rs = pst.executeQuery();
            rsmd = rs.getMetaData();
            int size = rsmd.getColumnCount();
            while(rs.next()){
                for(int i = 0; i < size; i++){
                    result.add(rs.getString(rsmd.getColumnName(i+1)));
                }
            }
        }catch(SQLException | ClassNotFoundException ex){
            throw new RuntimeException("Query Fail "+ sql +" "+ ex.getMessage(), ex);
        }
        return result;
    }
    
    public boolean query(String sql, Object [] data) {
        result.clear();
        boolean statusQuery = false;
        try{
            pst = getConnection().prepareStatement(sql);
            if (data != null){
                for (int i = 0; i < data.length; i++) {
                    if(data[i] instanceof java.lang.String){
                        pst.setString(i+1, String.valueOf(data[i]));
                    }else if(data[i] instanceof java.util.Date){
                        Date date;
                        SimpleDateFormat format = new SimpleDateFormat("EEE MMM dd HH:mm:ss z YYYY", Locale.ENGLISH);
                        try{
                            date = format.parse(data[i].toString());
                            pst.setDate(i+1, new java.sql.Date(date.getTime()));
                        }catch(Exception ex){
                            throw new RuntimeException("Parse Date Fail "+ex.getMessage(), ex);
                        }
                    }else if(data[i] instanceof java.lang.Double){
                        pst.setDouble(i+1, (double) data[i]);
                    }else if(data[i] instanceof java.lang.Integer){
                        pst.setInt(i+1, (Integer) data[i]);
                    }
                }
            }
            pst.execute();
            statusQuery = true;
        }catch(SQLException | ClassNotFoundException ex){
            throw new RuntimeException("Query Fail "+ sql +" "+ ex.getMessage(), ex);
        }
        return statusQuery;
    }
}