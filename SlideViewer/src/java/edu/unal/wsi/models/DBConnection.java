package edu.unal.wsi.models;

/**
 *
 * @author darwi
 */

import java.sql.*;
import java.util.logging.*;

public class DBConnection {
    
    public static Connection conn;
    
    public static Connection getConnection() throws ClassNotFoundException, SQLException{
        if (conn == null){
            Runtime.getRuntime().addShutdownHook(new getClose());
            Class.forName("org.postgresql.Driver");
            if(!System.getProperty("os.name").equalsIgnoreCase("Windows 10"))
                conn = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/telepath","web_telepath_user","l0v3l4c3");
            else
                conn = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/telepath","postgres","lovelace");
        }
        return conn;
    }
    
    static class getClose extends Thread{
        @Override
        public void run(){
            try{
                Connection conn = DBConnection.getConnection();
                conn.close();
            }catch(SQLException ex){
                throw new RuntimeException("Conexion Fallida");
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
}
