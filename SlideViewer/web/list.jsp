<%-- 
    Document   : index
    Created on : Nov 10, 2015, 8:58:26 PM
    Author     : pabar
--%>

<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    if(session.getAttribute("user") == null)
        request.getRequestDispatcher("index.jsp").forward(request, response); 
    ArrayList<String> roles = null;
    if(session.getAttribute("roles") != null)
        roles = (ArrayList)session.getAttribute("roles");
%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Bootstrap -->
        <link href="assets/css/plugins/bootstrap/bootstrap.css" rel="stylesheet">
        <link href="assets/css/plugins/admin2/sb-admin-2.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/plugins/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!-- Custom -->
        <link href="assets/css/custom/styles.css" rel="stylesheet">

        <title>CIM@LAB - WSI Viewer</title>
    </head>
    <body>
        <nav>
            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <% if(session != null){
                            if(roles != null){
                                if(roles.contains("student") || roles.contains("pathologist") || roles.contains("admin")){%>
                                    <li>
                                        <a href="index.jsp"><i class="fa fa-home fa-fw"></i>Home</a>
                                    </li>
                                <%}%>
                                <%for (String rol : roles){%>
                                    <li>
                                        <a href="#">
                                            <div>
                                                <i class="fa fa-group fa-fw"></i>Rol
                                                <span class="pull-right text-muted small"><%=rol%></span>
                                            </div>
                                        </a>
                                    </li>
                                <%}%>
                                <li class="divider"></li>
                        <%}}%>
                        <li>
                            <a href="Logout"><i class="fa fa-sign-out fa-fw"></i>Logout</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
        <div class="container">
            <p class="text-center">
                <a href="http://cimalab.unal.edu.co"><img src="assets/images/logo_cimalab.png" /></a>
                <a href="http://unal.edu.co">
                    <img style="height:90px;margin-top:10px;margin-left:50px;" src="assets/images/logo-unal.png" />
                </a>
                <a href="http://unillanos.edu.co">
                    <img style="height:90px;margin-top:10px;margin-left:50px;" src="assets/images/logo-unillanos.png" />
                </a>
            </p>
            <br />           

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">JPEG2000 Images</h3>
                </div>
                <div id="jpeg-images" class="panel-body"></div>
            </div>


            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">OpenSlide Images</h3>
                </div>
                <div id="openslide-images" class="panel-body"></div>
            </div>
            <div style="text-align:center">CIM@LAB - 2017</div>
        </div>

        <script src="assets/js/plugins/jquery-2.1.4.min.js"></script>
        <script src="assets/js/plugins/bootstrap/bootstrap.min.js"></script>

        <script src="assets/js/wsi/wsi.js"></script>
        <script src="assets/js/wsi/provider.js"></script>
        <script src="assets/js/wsi/image_list.js"></script>

        <script type="text/javascript">
            jQuery(document).ready(function () {
                var imageList = new edu.wsi.view.ImageList();
                var cat = "${cat}";
                imageList.initialize(cat);
            });
        </script>
    </body>
</html>
