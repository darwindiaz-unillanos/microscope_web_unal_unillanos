<%-- 
    Document   : index
    Created on : 21/09/2015, 11:12:39 AM
    Author     : Telemedicina
--%>

<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    if(session.getAttribute("user") == null)
        request.getRequestDispatcher("index.jsp").forward(request, response); 
    ArrayList<String> roles = null;
    if(session.getAttribute("roles") != null)
        roles = (ArrayList)session.getAttribute("roles");
%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Plugins -->
        <link href="assets/css/plugins/bootstrap/bootstrap.css" rel="stylesheet">
        <link href="assets/css/plugins/toastr.min.css" rel="stylesheet">
        <link href="assets/css/plugins/font-awesome.min.css" rel="stylesheet">
        <!-- Custom -->
        <link href="assets/css/custom/styles.css" rel="stylesheet">
        <title>CIM@LAB - WSI Viewer</title>
    </head>
    <body>
        <!-- Modal -->
        <div class="modal fade" id="end-animation-modal" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">                    
                    <div class="modal-body">
                        <h4>Fin de la reproducción.</h4>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>

            </div>
        </div>

        <div class="modal fade" id="loading-modal" data-backdrop="static" data-keyboard="false" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">                    
                    <div class="modal-body">
                        <i class="fa fa-spinner fa-spin fa-2x"></i>                        
                    </div>                    
                </div>
            </div>
        </div>


        <div id="img-data-widget">
            <div id="widget-close">
                <i class="fa fa-close"></i>
            </div>
            <div id="image-description-container">
                No clinical data available
            </div>
        </div>

        <div id="wsiViewport">
            <div id="nav-toolbar" class="pull-left">
                <button id="back" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Back to list">
                    <i class="glyphicon glyphicon-arrow-left"></i>
                </button>
                <div class="btn-group" id="play-tools">
                    <button id="play" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Play!">
                        <i class="glyphicon glyphicon-play-circle"></i>
                    </button>
                    <button id="pause" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Pause" style="display: none;">
                        <i class="glyphicon glyphicon-pause"></i>
                    </button>
                    <button id="stop" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Stop">
                        <i class="glyphicon glyphicon-stop"></i>
                    </button>
                </div>
                <div class="btn-group">
                    <button id="zoom-in" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Zoom In">
                        <i class="glyphicon glyphicon-zoom-in"></i>
                    </button>
                    <button id="zoom-out" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Zoom Out">
                        <i class="glyphicon glyphicon-zoom-out"></i>
                    </button>
                </div>
                <button id="home" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Home View">
                    <i class="glyphicon glyphicon-home"></i>
                </button>
                <button id="full-page" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Fullscreen">
                    <i class="glyphicon glyphicon-fullscreen"></i>
                </button>
                <button id="annotation" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Show annotations">
                    <i class="fa fa-sticky-note-o"></i>
                </button>
                <button id="info" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Show clinical information">
                    <i class="fa fa-info-circle"></i>
                </button>
                <div class="dropdown" data-toggle="" data-placement="bottom" title="Navigate Annotations" style="display: inline-block">
                    <button class="btn btn-default dropdown-toggle" aria-haspopup="true" aria-expanded="true" data-toggle="dropdown">
                        <i class="glyphicon glyphicon-list"></i>
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-left">
                        <li class="dropdown-header">Available Annotations</li>
                        <li id="annotations-dropdown-list" data-empty="true">
                            <a href="#" class="empty-placeholder">No Annotations...</a>
                        </li>
                    </ul>
                </div>
            </div>

            <div id="actions-toolbar" class="pull-right" style="display: none">
                <button id="edit-region" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Edit Region" style="display: none">
                    <i class="glyphicon glyphicon-screenshot"></i>
                </button>
                <button id="accept-edit-region" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Accept Changes" style="display: none">
                    <i class="glyphicon glyphicon-ok"></i>
                </button>
                <div class="dropdown" data-toggle="" data-placement="bottom" title="Add Annotation" style="display: none">
                    <button class="btn btn-default dropdown-toggle" aria-haspopup="true" aria-expanded="true" data-toggle="dropdown">
                        <i class="glyphicon glyphicon-plus"></i>
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li class="dropdown-header">Add Annotation Tools</li>
                        <li style="text-align: center">
                            <button class="btn btn-default add-region" data-region-type="freehand" data-toggle="tooltip" data-placement="bottom" title="Free Hand">
                                <i class="glyphicon glyphicon-hand-up"></i>
                            </button>
                            <button class="btn btn-default add-region" data-region-type="polygon" data-toggle="tooltip" data-placement="bottom" title="Polygon">
                                <i class="glyphicon glyphicon-star"></i>
                            </button>
                        </li>
                    </ul>
                </div>
                <div class="dropdown" data-toggle="" data-placement="bottom" title="Data Management" style="display: none">
                    <button class="btn btn-default dropdown-toggle" aria-haspopup="true" aria-expanded="true" data-toggle="dropdown">
                        <i class="glyphicon glyphicon-file"></i>
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li class="dropdown-header">Data Management Tools</li>
                        <li style="text-align: center">
                            <button id="save-data" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Save annotations" style="display: none">
                                <i class="glyphicon glyphicon-save"></i>
                            </button>
                            <button id="load-data" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Load annotations" style="display: none">
                                <i class="glyphicon glyphicon-open"></i>
                            </button>
                            <button id="export-data" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Download annotations file" style="display: none">
                                <i class="glyphicon glyphicon-floppy-save"></i>
                            </button>
                            <div data-toggle="tooltip" data-placement="bottom" title="Load annotations file" style="display: none">
                                <button id="import-data" class="btn btn-default" data-toggle="modal" data-target="#load-annotations-modal">
                                    <i class="glyphicon glyphicon-floppy-open"></i>
                                </button>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>

            <div id="infobox"><p></p>
                <hr/>
                <div class="pull-left">
                    <button id="remove-region" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Remove" style="display:none">
                        <i class="glyphicon glyphicon-remove"></i>
                    </button>
                </div>
                <div class="pull-right">
                    <button id="edit-annotation" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Edit Data" style="display:none">
                        <i class="glyphicon glyphicon-pencil"></i>
                    </button>
                    <button id="goto-region" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Go To">
                        <i class="glyphicon glyphicon-share-alt"></i>
                    </button>
                </div>
            </div>
        </div>

        <!-- Edit Region Modal -->
        <div class="modal fade" id="edit-annotation-modal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Edit Region</h4>
                    </div>
                    <div class="modal-body">
                        <form>
                            <div class="form-group">
                                <label class="control-label">Annotation:</label>
                                <input type="text" class="form-control" name="annotation" />
                            </div>
                            <div class="form-group">
                                <label for="message-text" class="control-label">Color:</label>
                                <input type="text" class="form-control" name="color" />
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button id="edit-annotation-modal-accept" type="button" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- Load Annotations Modal -->
        <div class="modal fade" id="load-annotations-modal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Load Annotations</h4>
                    </div>
                    <form id="load-annotations-modal-form" enctype="multipart/form-data" action="load_annotations" method="POST">
                        <div class="modal-body">
                            <div class="form-group">
                                <label class="control-label">Select annotations file:</label>
                                <input type="file" class="form-control" name="file" />
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button id="load-annotations-modal-accept" type="button" class="btn btn-primary" data-dismiss="modal">Load</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div hidden>
            <%if(roles != null){
                for (String rol : roles){%>
                <span class="rol"><%=rol%></span>
            <%}}%>
        </div>
        <script src="assets/js/plugins/jquery-2.1.4.min.js"></script>
        <script src="assets/js/plugins/toastr.min.js"></script>
        <script src="assets/js/plugins/bootstrap/bootstrap.min.js"></script>
        <script src="assets/js/plugins/openseadragon/openseadragon.min.js"></script>
        <script src="assets/js/plugins/openseadragon/openseadragon-svg-overlay.js"></script>
        <script src="assets/js/plugins/openseadragon/openseadragon-imaginghelper.min.js"></script>
        <script src="assets/js/plugins/openseadragon/openseadragonzoomlevels.js"></script>
        <script src="assets/js/plugins/openseadragon/openseadragon-scalebar.js"></script>
        <script src="assets/js/plugins/snap-svg/snap.svg-min.js"></script>

        <script src="assets/js/wsi/wsi.js"></script>
        <script src="assets/js/wsi/provider.js"></script>
        <script src="assets/js/wsi/navigator.js"></script>
        <script src="assets/js/wsi/canvas.js"></script>
        <script src="assets/js/wsi/region.js"></script>
        <script src="assets/js/wsi/interaction_player.js"></script>

        <script type="text/javascript">

            var recording = true;
            jQuery(document).ready(function () {
                var uid = "${uid}";
                var wsi = "${wsi}";
                var reader = "${reader}";
                var play = "${play}";

                var navigator = new edu.wsi.view.Navigator("wsiViewport");
                navigator.initialize(wsi, reader, navigator, uid,$("#image-description-container"));
                navigator.isRecording(recording);

                $("#info").click(function () {
                    $("#img-data-widget").css("left", "0px");
                });

                $("#widget-close").click(function(){
                    $("#img-data-widget").css("left", "-300px");
                });


                if (play === "") {
                    $("#play-tools").hide();
                } else {
                    $("#play-tools").show();
                }

                $('[data-toggle="tooltip"]').tooltip({
                    container: "body"
                });

                $(window).bind("beforeunload", function () {
                    if (recording) {
                        navigator.sendTrackingData();
                        return confirm("Do you really want to close?");
                    }
                });
            });

            $("#back").click(function () {
                //window.location.href = '../microscopio/';
                window.location.href = '../microscopio/';
            });
        </script>
    </body>
</html>
