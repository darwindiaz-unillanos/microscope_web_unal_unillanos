<%-- 
    Document   : index
    Created on : 28/08/2017, 08:58:20 PM
    Author     : darwi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    if(session.getAttribute("user") != null){
        if(session.getAttribute("user").equals("guest")){
           session.removeAttribute("user");
        }else{
            request.getRequestDispatcher("index.jsp").forward(request, response); 
        }
    }
%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <!-- Plugins -->
        <link href="assets/css/plugins/bootstrap/bootstrap.min.css" rel="stylesheet" type="text/css">    
        <link href="assets/css/plugins/admin2/sb-admin-2.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/plugins/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/custom/styles.css" rel="stylesheet" type="text/css">    
        <title>CIM@LAB - WSI Viewer</title>
    </head>
    <body>
        <div class="container">
            <p class="text-center">
                <a href="http://cimalab.unal.edu.co"><img src="assets/images/logo_cimalab.png" /></a>
                <a href="http://unal.edu.co">
                    <img style="height:90px;margin-top:10px;margin-left:50px;" src="assets/images/logo-unal.png" />
                </a>
                <a href="http://unillanos.edu.co">
                    <img style="height:90px;margin-top:10px;margin-left:50px;" src="assets/images/logo-unillanos.png" />
                </a>
            </p>
            <div class="row">
                <div class="col-md-4 col-md-offset-4" id="div-size">
                    <div class="login-panel panel panel-primary">
                        <div class="panel-heading">
                            <h1 class="panel-title h1">Sign In</h1>
                        </div>
                        <div class="panel-body">
                            <form class="form-login" role="form" method="post" action="SignIn">
                                <fieldset>
                                    <%if(session.getAttribute("error") != null && session.getAttribute("error").equals("invalid_user")){%>
                                        <span class="text-center bold error">User or Password incorrect</span>
                                    <%}%>
                                    <%if(session.getAttribute("error") != null && session.getAttribute("error").equals("does_not_user")){%>
                                        <span class="text-center bold error">Unregistered user</span>
                                    <%}%>
                                    <div class="form-group">
                                        <label class="control-label">User</label>
                                        <input class="form-control" placeholder="example@mail.com" name="email" type="email" autofocus>
                                    </div>
                                    <div class="form-group">
                                        <label class=control-label">Password</label>
                                        <input class="form-control" placeholder="password" name="password" type="password">
                                    </div>
                                    <div class="form-actions">
                                        <button type="submit" class="btn btn-lg btn-primary btn-block">Sign In</button>
                                        <button type="button" class="btn btn-lg btn-success btn-block" id="btn-register">Sign Up</button>
                                    </div>
                                </fieldset>
                            </form>
                            <form class="form-register" role="form" method="post" action="SignUp" style="display:none">
                                <fieldset>
                                    <div class="form-group">
                                        <label class="control-label">First name</label>
                                        <input class="form-control" placeholder="" name="firstname" type="text" maxlength="100" autofocus>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Last name</label>
                                        <input class="form-control" placeholder="" name="lastname" type="text" maxlength="100">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Identification number</label>
                                        <input class="form-control" placeholder="" name="identification" type="text" maxlength="18">
                                    </div>
                                    <div class="form-group ">
                                        <label class="control-label">Email</label>
                                        <input class="form-control" placeholder="" name="email" type="email" maxlength="100">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Password</label>
                                        <input class="form-control" placeholder="" name="password" type="password" maxlength="20" id="password">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Confirm password</label>
                                        <input class="form-control" placeholder="" name="confirm_password" type="password" maxlength="20">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Country</label>
                                        <select class="form-control" name="country">
                                            <option value="AF">Afghanistan</option>
                                            <option value="AL">Albania</option>
                                            <option value="DZ">Algeria</option>
                                            <option value="AS">American Samoa</option>
                                            <option value="AD">Andorra</option>
                                            <option value="AO">Angola</option>
                                            <option value="AI">Anguilla</option>
                                            <option value="AR">Argentina</option>
                                            <option value="AM">Armenia</option>
                                            <option value="AW">Aruba</option>
                                            <option value="AU">Australia</option>
                                            <option value="AT">Austria</option>
                                            <option value="AZ">Azerbaijan</option>
                                            <option value="BS">Bahamas</option>
                                            <option value="BH">Bahrain</option>
                                            <option value="BD">Bangladesh</option>
                                            <option value="BB">Barbados</option>
                                            <option value="BY">Belarus</option>
                                            <option value="BE">Belgium</option>
                                            <option value="BZ">Belize</option>
                                            <option value="BJ">Benin</option>
                                            <option value="BM">Bermuda</option>
                                            <option value="BT">Bhutan</option>
                                            <option value="BO">Bolivia</option>
                                            <option value="BA">Bosnia and Herzegowina</option>
                                            <option value="BW">Botswana</option>
                                            <option value="BV">Bouvet Island</option>
                                            <option value="BR">Brazil</option>
                                            <option value="IO">British Indian Ocean Territory</option>
                                            <option value="BN">Brunei Darussalam</option>
                                            <option value="BG">Bulgaria</option>
                                            <option value="BF">Burkina Faso</option>
                                            <option value="BI">Burundi</option>
                                            <option value="KH">Cambodia</option>
                                            <option value="CM">Cameroon</option>
                                            <option value="CA">Canada</option>
                                            <option value="CV">Cape Verde</option>
                                            <option value="KY">Cayman Islands</option>
                                            <option value="CF">Central African Republic</option>
                                            <option value="TD">Chad</option>
                                            <option value="CL">Chile</option>
                                            <option value="CN">China</option>
                                            <option value="CX">Christmas Island</option>
                                            <option value="CC">Cocos (Keeling) Islands</option>
                                            <option value="CO">Colombia</option>
                                            <option value="KM">Comoros</option>
                                            <option value="CG">Congo</option>
                                            <option value="CD">Congo, the Democratic Republic of the</option>
                                            <option value="CK">Cook Islands</option>
                                            <option value="CR">Costa Rica</option>
                                            <option value="CI">Cote d'Ivoire</option>
                                            <option value="HR">Croatia (Hrvatska)</option>
                                            <option value="CU">Cuba</option>
                                            <option value="CY">Cyprus</option>
                                            <option value="CZ">Czech Republic</option>
                                            <option value="DK">Denmark</option>
                                            <option value="DJ">Djibouti</option>
                                            <option value="DM">Dominica</option>
                                            <option value="DO">Dominican Republic</option>
                                            <option value="EC">Ecuador</option>
                                            <option value="EG">Egypt</option>
                                            <option value="SV">El Salvador</option>
                                            <option value="GQ">Equatorial Guinea</option>
                                            <option value="ER">Eritrea</option>
                                            <option value="EE">Estonia</option>
                                            <option value="ET">Ethiopia</option>
                                            <option value="FK">Falkland Islands (Malvinas)</option>
                                            <option value="FO">Faroe Islands</option>
                                            <option value="FJ">Fiji</option>
                                            <option value="FI">Finland</option>
                                            <option value="FR">France</option>
                                            <option value="GF">French Guiana</option>
                                            <option value="PF">French Polynesia</option>
                                            <option value="TF">French Southern Territories</option>
                                            <option value="GA">Gabon</option>
                                            <option value="GM">Gambia</option>
                                            <option value="GE">Georgia</option>
                                            <option value="DE">Germany</option>
                                            <option value="GH">Ghana</option>
                                            <option value="GI">Gibraltar</option>
                                            <option value="GR">Greece</option>
                                            <option value="GL">Greenland</option>
                                            <option value="GD">Grenada</option>
                                            <option value="GP">Guadeloupe</option>
                                            <option value="GU">Guam</option>
                                            <option value="GT">Guatemala</option>
                                            <option value="GN">Guinea</option>
                                            <option value="GW">Guinea-Bissau</option>
                                            <option value="GY">Guyana</option>
                                            <option value="HT">Haiti</option>
                                            <option value="HM">Heard and Mc Donald Islands</option>
                                            <option value="VA">Holy See (Vatican City State)</option>
                                            <option value="HN">Honduras</option>
                                            <option value="HK">Hong Kong</option>
                                            <option value="HU">Hungary</option>
                                            <option value="IS">Iceland</option>
                                            <option value="IN">India</option>
                                            <option value="ID">Indonesia</option>
                                            <option value="IR">Iran (Islamic Republic of)</option>
                                            <option value="IQ">Iraq</option>
                                            <option value="IE">Ireland</option>
                                            <option value="IL">Israel</option>
                                            <option value="IT">Italy</option>
                                            <option value="JM">Jamaica</option>
                                            <option value="JP">Japan</option>
                                            <option value="JO">Jordan</option>
                                            <option value="KZ">Kazakhstan</option>
                                            <option value="KE">Kenya</option>
                                            <option value="KI">Kiribati</option>
                                            <option value="KP">Korea, Democratic People's Republic of</option>
                                            <option value="KR">Korea, Republic of</option>
                                            <option value="KW">Kuwait</option>
                                            <option value="KG">Kyrgyzstan</option>
                                            <option value="LA">Lao People's Democratic Republic</option>
                                            <option value="LV">Latvia</option>
                                            <option value="LB">Lebanon</option>
                                            <option value="LS">Lesotho</option>
                                            <option value="LR">Liberia</option>
                                            <option value="LY">Libyan Arab Jamahiriya</option>
                                            <option value="LI">Liechtenstein</option>
                                            <option value="LT">Lithuania</option>
                                            <option value="LU">Luxembourg</option>
                                            <option value="MO">Macau</option>
                                            <option value="MK">Macedonia, The Former Yugoslav Republic of</option>
                                            <option value="MG">Madagascar</option>
                                            <option value="MW">Malawi</option>
                                            <option value="MY">Malaysia</option>
                                            <option value="MV">Maldives</option>
                                            <option value="ML">Mali</option>
                                            <option value="MT">Malta</option>
                                            <option value="MH">Marshall Islands</option>
                                            <option value="MQ">Martinique</option>
                                            <option value="MR">Mauritania</option>
                                            <option value="MU">Mauritius</option>
                                            <option value="YT">Mayotte</option>
                                            <option value="MX">Mexico</option>
                                            <option value="FM">Micronesia, Federated States of</option>
                                            <option value="MD">Moldova, Republic of</option>
                                            <option value="MC">Monaco</option>
                                            <option value="MN">Mongolia</option>
                                            <option value="MS">Montserrat</option>
                                            <option value="MA">Morocco</option>
                                            <option value="MZ">Mozambique</option>
                                            <option value="MM">Myanmar</option>
                                            <option value="NA">Namibia</option>
                                            <option value="NR">Nauru</option>
                                            <option value="NP">Nepal</option>
                                            <option value="NL">Netherlands</option>
                                            <option value="AN">Netherlands Antilles</option>
                                            <option value="NC">New Caledonia</option>
                                            <option value="NZ">New Zealand</option>
                                            <option value="NI">Nicaragua</option>
                                            <option value="NE">Niger</option>
                                            <option value="NG">Nigeria</option>
                                            <option value="NU">Niue</option>
                                            <option value="NF">Norfolk Island</option>
                                            <option value="MP">Northern Mariana Islands</option>
                                            <option value="NO">Norway</option>
                                            <option value="OM">Oman</option>
                                            <option value="PK">Pakistan</option>
                                            <option value="PW">Palau</option>
                                            <option value="PA">Panama</option>
                                            <option value="PG">Papua New Guinea</option>
                                            <option value="PY">Paraguay</option>
                                            <option value="PE">Peru</option>
                                            <option value="PH">Philippines</option>
                                            <option value="PN">Pitcairn</option>
                                            <option value="PL">Poland</option>
                                            <option value="PT">Portugal</option>
                                            <option value="PR">Puerto Rico</option>
                                            <option value="QA">Qatar</option>
                                            <option value="RE">Reunion</option>
                                            <option value="RO">Romania</option>
                                            <option value="RU">Russian Federation</option>
                                            <option value="RW">Rwanda</option>
                                            <option value="KN">Saint Kitts and Nevis</option>
                                            <option value="LC">Saint LUCIA</option>
                                            <option value="VC">Saint Vincent and the Grenadines</option>
                                            <option value="WS">Samoa</option>
                                            <option value="SM">San Marino</option>
                                            <option value="ST">Sao Tome and Principe</option>
                                            <option value="SA">Saudi Arabia</option>
                                            <option value="SN">Senegal</option>
                                            <option value="SC">Seychelles</option>
                                            <option value="SL">Sierra Leone</option>
                                            <option value="SG">Singapore</option>
                                            <option value="SK">Slovakia (Slovak Republic)</option>
                                            <option value="SI">Slovenia</option>
                                            <option value="SB">Solomon Islands</option>
                                            <option value="SO">Somalia</option>
                                            <option value="ZA">South Africa</option>
                                            <option value="GS">South Georgia and the South Sandwich Islands</option>
                                            <option value="ES">Spain</option>
                                            <option value="LK">Sri Lanka</option>
                                            <option value="SH">St. Helena</option>
                                            <option value="PM">St. Pierre and Miquelon</option>
                                            <option value="SD">Sudan</option>
                                            <option value="SR">Suriname</option>
                                            <option value="SJ">Svalbard and Jan Mayen Islands</option>
                                            <option value="SZ">Swaziland</option>
                                            <option value="SE">Sweden</option>
                                            <option value="CH">Switzerland</option>
                                            <option value="SY">Syrian Arab Republic</option>
                                            <option value="TW">Taiwan, Province of China</option>
                                            <option value="TJ">Tajikistan</option>
                                            <option value="TZ">Tanzania, United Republic of</option>
                                            <option value="TH">Thailand</option>
                                            <option value="TG">Togo</option>
                                            <option value="TK">Tokelau</option>
                                            <option value="TO">Tonga</option>
                                            <option value="TT">Trinidad and Tobago</option>
                                            <option value="TN">Tunisia</option>
                                            <option value="TR">Turkey</option>
                                            <option value="TM">Turkmenistan</option>
                                            <option value="TC">Turks and Caicos Islands</option>
                                            <option value="TV">Tuvalu</option>
                                            <option value="UG">Uganda</option>
                                            <option value="UA">Ukraine</option>
                                            <option value="AE">United Arab Emirates</option>
                                            <option value="GB">United Kingdom</option>
                                            <option value="US">United States</option>
                                            <option value="UM">United States Minor Outlying Islands</option>
                                            <option value="UY">Uruguay</option>
                                            <option value="UZ">Uzbekistan</option>
                                            <option value="VU">Vanuatu</option>
                                            <option value="VE">Venezuela</option>
                                            <option value="VN">Viet Nam</option>
                                            <option value="VG">Virgin Islands (British)</option>
                                            <option value="VI">Virgin Islands (U.S.)</option>
                                            <option value="WF">Wallis and Futuna Islands</option>
                                            <option value="EH">Western Sahara</option>
                                            <option value="YE">Yemen</option>
                                            <option value="ZM">Zambia</option>
                                            <option value="ZW">Zimbabwe</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">City</label>
                                        <input class="form-control" placeholder="" name="city" type="text" maxlength="20">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Zip</label>
                                        <input class="form-control" placeholder="" name="zipcode" type="text" maxlength="10">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">University degrees</label>
                                        <select class="form-control" name="degress">
                                            <option value="UGSTU">Undergraduate student</option>
                                            <optgroup label="Medical specialty student">
                                                <option value="MSSR1">1 year resident</option>
                                                <option value="MSSR2">2 year resident</option>
                                                <option value="MSSR3">3 year resident</option>
                                            </optgroup>
                                            <option value="MDDOC">Medical Doctor</option>
                                            <optgroup label="Specialist Doctor">
                                                <option value="SDEm1">Experience < 1 year</option>
                                                <option value="SDEm3">Experience > 1 year and < 3 year</option>
                                                <option value="SDEm5">Experience > 3 year and < 5 year</option>
                                                <option value="SDEM5">Experience > 5 year</option>
                                            </optgroup>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Institution</label>
                                        <input class="form-control" placeholder="" name="institution" type="text" maxlength="100">
                                    </div>
                                    <div class="form-actions">
                                        <button type="submit" class="btn btn-lg btn-primary btn-block">Sign Up</button>
                                        <button type="button" class="btn btn-lg btn-success btn-block" id="btn-login">Sign In</button>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="assets/js/plugins/jquery-2.1.4.min.js"></script>
        <script src="assets/js/plugins/bootstrap/bootstrap.min.js"></script>
        <script src="assets/js/plugins/jquery-validation/jquery.validate.min.js"></script>
        <script src="assets/js/wsi/login.js"></script>
    </body>
</html>