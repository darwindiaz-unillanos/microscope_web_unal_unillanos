<%-- 
    Document   : list
    Created on : 07-feb-2017, 10:47:03
    Author     : German
--%>

<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    ArrayList<String> roles = null;
    if(session.getAttribute("roles") != null)
        roles = (ArrayList)session.getAttribute("roles");
    if(session.getAttribute("user") == null){
        roles = new ArrayList<String>();
        roles.add("guest");
        session.setAttribute("user", "guest");
        session.setAttribute("roles", roles);
    }
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>CIM@LAB - WSI Viewer</title>
        <link href="assets/css/plugins/bootstrap/bootstrap.css" rel="stylesheet">
        <link href="assets/css/plugins/bootstrap-treeview.min.css" rel="stylesheet">
        <link href="assets/css/plugins/admin2/sb-admin-2.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/plugins/font-awesome.min.css" rel="stylesheet" type="text/css">
        <title>Welcome!</title>
    </head>
    <body>
        <nav><!--class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0"-->
            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <% if(session != null){
                            if(roles != null){
                                if(roles.contains("student") || roles.contains("pathologist") || roles.contains("admin")){%>
                                    <li>
                                        <a href="profile.jsp"><i class="fa fa-user fa-fw"></i> User Profile</a>
                                    </li>
                                <%}%>
                                <%for (String rol : roles){%>
                                    <li>
                                        <a href="#">
                                            <div>
                                                <i class="fa fa-group fa-fw"></i>Rol
                                                <span class="pull-right text-muted small"><%=rol%></span>
                                            </div>
                                        </a>
                                    </li>
                                <%}%>
                                <li class="divider"></li>
                        <%}}%>
                        <li>
                            <a href="Logout"><i class="fa fa-sign-out fa-fw"></i>Logout</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
        <div  class="container-fluid">
            <div class="container">
                <p class="text-center">
                    <a href="http://cimalab.unal.edu.co"><img src="assets/images/logo_cimalab.png" /></a>
                    <a href="http://unal.edu.co">
                        <img style="height:90px;margin-top:10px;margin-left:50px;" src="assets/images/logo-unal.png" />
                    </a>
                    <a href="http://unillanos.edu.co">
                        <img style="height:90px;margin-top:10px;margin-left:50px;" src="assets/images/logo-unillanos.png" />
                    </a>
                </p>
                <br />
                <!--<h1>Whole Slide Images</h1>
                <p>List of available WSI categories.</p>-->
                <div class="row">
                    <div class="span6" style="float: none; margin: 0 auto;">
                        <div id="treeview"></div>
                    </div>                
                </div>
                <div style="text-align:center">CIM@LAB - 2017</div>
            </div>
        </div>
        <script src="assets/js/plugins/jquery-2.1.4.min.js"></script>
        <script src="assets/js/plugins/bootstrap-treeview.min.js"></script>
        <script src="assets/js/plugins/bootstrap/bootstrap.min.js"></script>
        <script src="assets/js/wsi/wsi.js"></script>
        <script src="assets/js/wsi/provider.js"></script>
        <script src="assets/js/wsi/struct.js"></script>
        <script type="text/javascript">

           /*$(function () {

                var defaultData = [{"nodes":[{"nodes":[{"images":["2621-16","2648-09"],"text":"Grade 12","id":"bcc_1","href":"list?path=bcc_1"},{"images":["TAF1_1","13104-15"],"text":"Grade 2","id":"bcc_2","href":"list?path=bcc_2"}],"text":"Basal Cell Carcinoma"},{"images":["2456-09","1808-16"],"text":"Melanoma","id":"melanoma","href":"list?path=melanoma"}],"text":"Skin"},{"images":["TASu2"],"text":"Gastric","id":"gastric","href":"list?path=gastric"}];

                $('#treeview').treeview({
                    data: defaultData,
                    enableLinks: true,
                    //expandIcon: 'glyphicon glyphicon-chevron-right',
                    //collapseIcon: 'glyphicon glyphicon-chevron-down',
                    selectable: true
                });
            });*/
            
            jQuery(document).ready(function() {
                var struct = new edu.wsi.view.Struct();
                var treeview=$('#treeview');
                struct.initialize(treeview);
            });
        </script>
    </body>
</html>
