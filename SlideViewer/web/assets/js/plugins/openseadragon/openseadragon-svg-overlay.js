// OpenSeadragon SVG Overlay plugin 0.0.4

(function() {

    if (!window.OpenSeadragon) {
        console.error('[openseadragon-svg-overlay] requires OpenSeadragon');
        return;
    }

    var svgNS = 'http://www.w3.org/2000/svg';

    // ----------
    OpenSeadragon.Viewer.prototype.svgOverlay = function(width, height) {
        if (this._svgOverlayInfo) {
            return this._svgOverlayInfo;
        }

        this._svgOverlayInfo = new Overlay(this, width, height);
        return this._svgOverlayInfo;
    };

    // ----------
    var Overlay = function(viewer, _width, _height) {
        var self = this;
        this.width = _width;
        this.height = _height;
        this.aspectRatio = _width / _height;

        this._viewer = viewer;
        this._containerWidth = 0;
        this._containerHeight = 0;

        this._svg = document.createElementNS(svgNS, 'svg');
        this._svg.style.position = 'absolute';
        this._svg.style.left = 0;
        this._svg.style.top = 0;
        this._svg.style.width = '100%';
        this._svg.style.height = '100%';

        this._node = document.createElementNS(svgNS, 'g');
        this._viewer.canvas.appendChild(this._svg);
        this._svg.appendChild(this._node);

        this._viewer.addHandler('animation', function() {
            self.transform();
        });

        this._viewer.addHandler('open', function() {
            self.resize();
        });

        this.resize();
    };

    // ----------
    Overlay.prototype = {
        // ----------
        node: function() {
            return this._node;
        },

        // ----------
        transform: function() {
            var screenRatio = this._containerWidth / this._containerHeight;

            var scaleFactor;
            var zoom = this._viewer.viewport.getZoom(true);
            var topLeft = this._viewer.viewport.viewerElementToImageCoordinates(new OpenSeadragon.Point(0, 0), true);
                
            if(this.aspectRatio < screenRatio) {
                scaleFactor = this._containerWidth / (this._containerHeight * this.aspectRatio);
            } else {
                scaleFactor = 1;
            }
            
            this._node.setAttribute('transform', ''
                + 'scale(' + (zoom * scaleFactor) + ') '
                + 'translate(' + (-topLeft.x) + ', ' + (-topLeft.y) + ')'
            );
        },
        
        resize: function() {            
            if (this._containerWidth !== this._viewer.container.clientWidth) {
                this._containerWidth = this._viewer.container.clientWidth;
                this._svg.setAttribute('width', this._containerWidth);
            }

            if (this._containerHeight !== this._viewer.container.clientHeight) {
                this._containerHeight = this._viewer.container.clientHeight;
                this._svg.setAttribute('height', this._containerHeight);
            }
            
            var screenRatio = this._containerWidth / this._containerHeight;
            
            if(screenRatio > this.aspectRatio) {
                this._svg.setAttribute('viewBox', '0 0 ' + (this.height * screenRatio) + ' ' + this.height);
            } else {
                this._svg.setAttribute('viewBox', '0 0 ' + (this.width) + ' ' + (this.width / screenRatio));
            }
            
            this.transform();
        },

        // ----------
        onClick: function(node, handler) {
            // TODO: Fast click for mobile browsers

            new OpenSeadragon.MouseTracker({
                element: node,
                clickHandler: handler
            }).setTracking(true);
        }
    };

})();
