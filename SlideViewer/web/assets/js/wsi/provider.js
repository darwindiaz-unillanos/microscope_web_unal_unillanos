/*
 * JS objects that serve as interfaces between the client and the WSIProvider
 */

edu.wsi.model.WSIData = function(_name, _width, _height, _totalLayers, _readMode,_description) {
    var name = _name;
    var width = _width;
    var height = _height;
    var readMode = _readMode;
    var totalLayers = _totalLayers;
    var description = _description;
    
    return {
        getName: function() { return name; },
        getWidth: function() { return width; },
        getHeight: function() { return height; },
        getReadMode: function() { return readMode; },
        getTotalLayers: function() { return totalLayers; },
        getDescription: function() { return description; }
    };
};

edu.wsi.model.WSIProvider = function() {
    //var providerWebRoot = "http://172.17.0.111:8084/WSIProvider";
    //var providerWebRoot = "http://168.176.61.15:12002/WSIProvider";
    var providerWebRoot = "http://localhost:8084/WSIProvider";
    //var providerWebRoot = "http://cimalab.unal.edu.co/WSIProvider";
    var ajax = new edu.wsi.util.AjaxHandler(providerWebRoot);
    
    var getInfo = function(wsiName, readMode, onReady) {
        var onSuccess = function(data) {
            var wsiData = json2WSIData(data, readMode);
            onReady(wsiData);
        };

        var onError = function(data) {
            console.log(data);
            onReady(null);
        };
        
        var endpointUrl = 'info?'
            + 'wsi=' + wsiName + '&'
            + 'reader=' + readMode;
        
        ajax.get(endpointUrl, onSuccess, onError);
    };
    
    var getInteraction = function(wsiName,onReady){
        var onSuccess = function(data) {
            onReady(data.interaction);
        };
        var onError = function(data) {
            onReady(null);
        };        
        var endpointUrl = 'interaction?wsi=' + wsiName;        
        ajax.get(endpointUrl, onSuccess, onError);
    };
    
    var getServerAnnotation = function(wsiName,onReady){
        var onSuccess = function(data) {
            onReady(data);
        };
        var onError = function(data) {
            onReady(null);
        };        
        var endpointUrl = 'annotation?wsi=' + wsiName;        
        ajax.get(endpointUrl, onSuccess, onError);
    };
    
    var getDescription = function(wsiName,onReady){
        var onSuccess = function(data) {
            onReady(data);
        };
        var onError = function(data) {
            onReady(null);
        };        
        var endpointUrl = 'description?wsi=' + wsiName;        
        ajax.get(endpointUrl, onSuccess, onError);
    };
    
    var getList = function(onReady, cat) {
        var onSuccess = function(wsiList) {
            onReady(wsiList);
        };

        var onError = function(data) {
            console.log(data);
            onReady(null);
        };
        
        var endpointUrl = 'listByCategory?cat='+cat;
        
        ajax.get(endpointUrl, onSuccess, onError);
    };
    
    var getStruct = function(onReady) {
        var onSuccess = function(struct) {
            onReady(struct);
        };

        var onError = function(data) {
            console.log(data);
            onReady(null);
        };
        
        var endpointUrl = 'struct';
        
        ajax.get(endpointUrl, onSuccess, onError);
    };
    
    var buildWSIThumbnailUrl = function(imageName,w,h) {
        return providerWebRoot + "/thumb?wsi=" + imageName+"&w="+w+"&h="+h;
    };
    
    var buildTileUrl = function(wsiData, tileX, tileY, layer) {
        var url = providerWebRoot
            + '/navigate?wsi=' + wsiData.getName() + '&'
            + 'r=' + layer + '&' 
            + 'x=' + tileX + '&' 
            + 'y=' + tileY + '&' 
            + 'reader=' + wsiData.getReadMode();
        
        //console.log("Requesting tile: " + url);
        return url;
    };
    
    var json2WSIData = function(json, readMode) {
        return new edu.wsi.model.WSIData(
            json.name,
            parseInt(json.width),
            parseInt(json.height),
            parseInt(json.layers),
            readMode,
            json.description
        );
    };
    
    return {
        getList: getList,
        getStruct: getStruct,
        getInfo: getInfo,
        getInteraction: getInteraction,
        getServerAnnotation: getServerAnnotation,
        getDescription: getDescription,
        buildTileUrl: buildTileUrl,
        buildWSIThumbnailUrl: buildWSIThumbnailUrl
    };
};