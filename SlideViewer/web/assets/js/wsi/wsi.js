/* 
 * Namespaces
 */
var edu = {};
edu.wsi = {};
edu.wsi.view = {};
edu.wsi.model = {};
edu.wsi.util = {};

/*
 * Util class that encapsulates an ajax request
 */
edu.wsi.util.AjaxHandler = function(_webServiceRoot) {
    var webServiceRoot = _webServiceRoot;
    var request = function (endpoint, method, success, error, data, headers) {
        var options = {
            url: webServiceRoot + "/" + endpoint,
            dataType: 'json',
            type: method,
            crossDomain: true,
            success: success,
            error: error
        };
        
        if(typeof data !== 'undefined' && data !== null) {
            options.data = JSON.stringify(data);
            options.contentType = 'application/json';
        }

        if (typeof headers !== 'undefined' && headers !== null) {
            options.headers = headers;
        }

        $.ajax(options);
    };
    
    var get = function(endpoint, success, error, data, headers) {
        request(endpoint, "GET", success, error, data, headers);
    };
    
    var post = function(endpoint, success, error, data, headers) {
        request(endpoint, "POST", success, error, data, headers);
    };
    
    var put = function(endpoint, success, error, data, headers) {
        request(endpoint, "PUT", success, error, data, headers);
    };
    
    return {
        get: get,
        post: post,
        put: put
    };
};

/*
 * Util static class that contains all different region types constants
 */
edu.wsi.util.RegionType = {
    DEFAULT: 0,
    POLYGON: 1
};

edu.wsi.util.DrawingOperation = {
    NONE: "none",
    FREEHAND: "freehand",
    POLYGON: "polygon"
};