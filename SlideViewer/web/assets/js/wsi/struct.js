edu.wsi.view.Struct = function () {
    var wsiProvider = new edu.wsi.model.WSIProvider();

    var _treeview;

    var initialize = function (treeview) {
        _treeview=treeview;
        wsiProvider.getStruct(struct);
    };

    var printTree = function (treeData) {
        _treeview.treeview({
            data: treeData,
            enableLinks: true,
            expandIcon: 'glyphicon glyphicon-chevron-right',
            collapseIcon: 'glyphicon glyphicon-chevron-down',
            selectable: false
        });
        _treeview.treeview('collapseAll', { silent: true });        
    };

    var struct = function (treeData) {
        if (treeData !== null) {
            printTree(treeData);
        } else {
            console.log("Sorry, couldn't grab data from provider");
        }
    };

    return {
        initialize: initialize
    };
};