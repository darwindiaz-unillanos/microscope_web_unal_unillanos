edu.wsi.view.InteractionPlayer = function () {

    var viewer;
    var overlay;
    var svgPaper;
    var wsiName;
    var strokeWidth = 30;
    var interval;
    var startPoint = true;
    var imagingHelper;
    var navigator;
    var interactions = null;
    var prevZoom = 0.15;
    var cx, cy, curColVal, ind = 1;
    var curZoom = 0.1;
    var wsiProvider = new edu.wsi.model.WSIProvider();


    var initialize = function (openSeadragonViewer, imgHelper, nav, file) {
        viewer = openSeadragonViewer;
        navigator = nav;
        overlay = viewer.svgOverlay();
        svgPaper = new Snap(overlay.node());
        imagingHelper = imgHelper;
        wsiName = file;
        attachEvents();
    };

    var panToPoint = function (point, zoom) {
        viewer.viewport.panTo(point, false);
        if (zoom > 0) {
            imagingHelper.zoomIn();
        } else if (zoom < 0) {
            imagingHelper.zoomOut();
        }
    };

    var drawLine = function (x1, y1, x2, y2, color) {
        svgPaper.line(x1, y1, x2, y2).attr({
            stroke: color,
            strokeWidth: strokeWidth
        });
    };

    var drawCircle = function (centerX, centerY, color) {
        svgPaper.circle(centerX, centerY, 50).attr({
            fill: color,
            stroke: color,
            strokeWidth: strokeWidth / 2
        });
    };

    function attachEvents() {
        $("#play").click(function () {
            $("#pause").show();
            $("#play").hide();
            navigator.isRecording(false);
            
            if(ind===1){
                clearCanvas();
                viewer.viewport.goHome().zoomTo(0.1);                
            }

            if (interactions === null) {
                $("#loading-modal").modal();
                wsiProvider.getInteraction(wsiName, interactionLoaded);
            } else {
                setInterval();
            }            
        });

        $("#pause").click(function () {
            $("#play").show();
            $("#pause").hide();
            clearInterval(interval);
        });

        $("#stop").click(function () {
            stopAnimation();
            clearCanvas();
        });
    }

    function interactionLoaded(data) {
        $("#loading-modal").modal('toggle');
        if (data !== null && data !== "") {
            interactions = data.split(";");            
            setInterval();
        } else {
            $("#play").show();
            $("#pause").hide();
            toastr["error"]("Error al cargar las interacciones");                    
        }
    }

    function setInterval() {
        interval = window.setInterval(function () {
            playInteraction(interactions);
        }, 1000);
    }

    function playInteraction(interactions) {
        if (ind < interactions.length - 1) {
            var str = interactions[ind];
            ind++;
            if (str !== "") {
                var interaction = str.split(",");
                var zoom = parseFloat(interaction[4]);
                if (zoom !== curZoom) {
                    startPoint = true;
                    curColVal = curZoom;
                    curZoom = zoom;
                }
                if (interaction[2] === "0") { //line or point
                    if (startPoint) {
                        startPoint = false;
                    } else {
                        drawLine(cx, cy, interaction[0], interaction[1], getColorForZoomFactor(curColVal));
                    }
                    cx = parseInt(interaction[0]);
                    cy = parseInt(interaction[1]);
                    drawCircle(cx, cy, getColorForZoomFactor(curColVal));
                } else {
                    var dif = prevZoom - zoom.toFixed(2);
                    var zoomChange=(Math.abs(dif) <= .05)?0:(dif<0)?1:-1;
                    var point = viewer.viewport.imageToViewportCoordinates(parseInt(interaction[2]), parseInt(interaction[3]));
                    prevZoom = zoom;
                    panToPoint(point, zoomChange);
                }
            }
        } else {
            $("#end-animation-modal").modal();
            stopAnimation();
        }
    }

    function stopAnimation() {
        $("#play").show();
        $("#pause").hide();
        navigator.isRecording(true);
        clearInterval(interval);
        ind = 1;
    }

    function clearCanvas() {
        $("circle").remove();
        $("line").remove();
    }

    function getColorForZoomFactor(zoomFactor) {
        if (zoomFactor < .25) {
            return "red";
        } else if (zoomFactor < 1) {
            return "green";
        }
        return "yellow";
    }

    return {
        initialize: initialize
    };
};


