 var modelProfile = function(){
    var loadProfile  = function(data, onSuccess, onError){
        $.ajax({
            type: 'GET',
            url: 'user_profile',
            success: onSuccess,
            error: onError,
            data: { "user":data},
            cache: false,
            accepts: "application/json"
        });
    };
    var saveProfile = function(data, onSuccess, onError){
        $.ajax({
            type: 'POST',
            url: 'user_profile',
            success: onSuccess,
            error: onError,
            data: data,
            cache: false,
            accepts: "text/plain"
        });  
    };
    
    return {
      loadProfile: loadProfile,
      saveProfile: saveProfile
    };
};

var viewProfile = function(){
    var model = new modelProfile;
    var roles;
    var organs;
    var specialty;
    var usersAll;
    var userSelected;
    var user_roles = [];
     
    var initialize = function(){
        if(validateRole()){
            model.loadProfile("all",onLoadUserProfileSuccess,onLoadProfileError);
        }else{
            model.loadProfile(-1,onLoadProfileSingleSuccess,onLoadProfileError);
        }
        attachEvent();
    };
     
    var validateRole = function(){
        var elementRoles = $("div > span.pull-right.text-muted.small.rol");
        for (var i = 0; i < elementRoles.length; i++) {
            user_roles.push($(elementRoles[i]).text());
        }
        if(user_roles.indexOf("admin") !== -1)
            return true;
        else
            return false;
    };
    
    var onLoadUserProfileSuccess = function (data){
        var dataJson = JSON.parse(data);
        if(dataJson["usersAll"]){
            usersAll = dataJson["usersAll"];
            model.loadProfile(-1,onLoadDataProfileSuccess,onLoadProfileError);
        }else if(dataJson["user"]){
            loadUserSelectedSuccess(data);
        }
    };
    
    var onLoadDataProfileSuccess = function (data){
        var dataJson = JSON.parse(data);
        roles = dataJson["roles"];
        organs = dataJson["organs"];
        specialty = dataJson["specialty"];
        loadElements();
    };
    
    var onLoadProfileSingleSuccess = function (data){
        onLoadDataProfileSuccess(data, true);
        userSelected = $("input[name='id_user']").val();
        model.loadProfile(userSelected, onLoadUserProfileSuccess, onLoadProfileError);
    };
    
    var onLoadProfileError = function (error){
        alert("An error has occurred while trying to load the data");
    };
    
    var loadElements = function(){
        $("#roles").select2({
            data: roles,
            width: 'resolve',
            tags: true
        });
        $("#organs").select2({
            data: organs,
            width: 'resolve'
        });
        $("#specialty").select2({
            data: specialty,
            width: 'resolve'
        });
        
        $('.typeahead').typeahead({
            source: function(query, process){
                users = [];
                map = {};
                
                $.each(usersAll, function(i, user){
                    map[user.email] = user;
                    users.push(user.email);
                });
                process(users);
            },
            updater: function(item){
                $('#roles').val(null).trigger('change');
                $('#organs').val(null).trigger('change');
                $('#specialty').val(null).trigger('change');
                userSelected = map[item].id;
                loadUserSelected(map[item].id);
            }
        });
    };
    
    var loadUserSelected = function(id){
        model.loadProfile(parseInt(id),loadUserSelectedSuccess,loadUserSelectedError);
    };
    
    var loadUserSelectedSuccess = function(data){
        var dataJson = JSON.parse(data);
        if(dataJson["user"]){
            var userData = dataJson["user"];
            $("input[name='id_user']").val(userSelected);
            $("input[name='firstname']").val(userData["user1"]);
            $("input[name='lastname']").val(userData["user2"]);
            $("input[name='identification']").val(userData["user0"]);
            $("input[name='city']").val(userData["user4"]);
            $("input[name='zipcode']").val(userData["user5"]);
            $("input[name='institution']").val(userData["user7"]);
            $("input[name='phone_number']").val(userData["user8"]);
            $("input[name='mobile_number']").val(userData["user9"]);
            $("input[name='occupation']").val(userData["user10"]);
            $("input[name='languaje']").val(userData["user11"]);
            $("input[name='email']").val(userData["user12"]);

            $("select[name='country']").val(userData["user3"]);
            $("select[name='degress']").val(userData["user6"]);
        }
        
        if(dataJson["userRoles"]){
            var data = dataJson["userRoles"];
            loadSelect2("roles",data,data.length);
        }
        
        if(dataJson["userOrgans"]){
            var data = dataJson["userOrgans"];
            loadSelect2("organs",data,data.length);
        }
        
        if(dataJson["userSpecialty"]){
            var data = dataJson["userSpecialty"];
            loadSelect2("specialty",data,data.length);
        }
    };
    
    var loadUserSelectedError = function(error){
        alert("An error has occurred while trying to load the data user selected");
    };
    
    var loadSelect2 = function(selector, data, size){
        var dataSet = [];
        if(size > 1){
            for (var i = 0; i < size; i++) {
                dataSet.push(data[i].id);
            }
            $("#"+selector).val(dataSet).trigger('change');
        }else{
            $("#"+selector).val(data[0].id).trigger('change');
        }
    };
    
    var attachEvent = function(){
        $('.form-profile').validate({
            rules:{
                firstname:{
                    required: true
                },
                lastname:{
                    required: true
                },
                identification:{
                    required: true
                },
                email:{
                    required: true,
                    email: true
                },
                city:{
                    required: true
                },
                country:{
                    required: true
                },
                zipcode:{
                    required: true
                },
                institution:{
                    required: true
                },
                phone_number:{
                    required: true
                },
                mobile_number:{
                    required: true
                },
                occupation:{
                    required: true
                },
                language:{
                    required: true
                },
                roles:{
                    required: true
                },
                specialty:{
                    required: true
                },
                organs:{
                    required: true
                }
            }
        });
        $(".form-profile button[type='submit']").click(onSaveProfile);
    };
    
    var onSaveProfile = function(){
        var form = $('.form-profile').valid();
        if(form){
            model.saveProfile($('.form-profile').serialize(),onSaveProfileSuccess, onSaveProfileError);
        }
    };
    
    var onSaveProfileSuccess = function (data){
        alert("Save data successfully");
    };
    
    var onSaveProfileError = function (error){
        alert("An error has occurred while trying to save the data");
    };
    
    return{
        initialize: initialize
    };
 };

jQuery(document).ready(function() {
    var profile = new viewProfile();
    profile.initialize();
});

