var Login = function(){
    var handleLogin = function(){
        $('.form-login').validate({
            rules:{
                email:{
                    required: true,
                    email: true
                },
                password:{
                    required: true
                }
            },
            messages:{
                email:{
                    required: "User is required"
                },
                password:{
                    required: "Password is required"
                }
            }
        });
        
        jQuery('#btn-register').click(function(){
            jQuery('#div-size').removeClass('col-md-offset-4 col-md-4');
            jQuery('#div-size').addClass('col-md-offset-3 col-md-6');
            jQuery('.panel-heading > .panel-title.h1').text("Sign Up");
            jQuery('.form-login').hide();
            jQuery('.form-register').show();
            
        }); 
    };
    
    var handleRegister = function(){
        $('.form-register').validate({
            rules:{
                firstname:{
                    required: true
                },
                lastname:{
                    required: true
                },
                identification:{
                    required: true
                },
                email:{
                    required: true,
                    email: true
                },
                password:{
                    required: true
                },
                confirm_password:{
                    required: true,
                    equalTo: "#password"
                },
                city:{
                    required: true
                },
                country:{
                    required: true
                },
                zipcode:{
                    required: true
                },
                institution:{
                    required: true
                }
            }
        });
        jQuery("#btn-login").click(function(){
            jQuery('#div-size').removeClass('col-md-offset-3 col-md-6');
            jQuery('#div-size').addClass('col-md-offset-4 col-md-4');
            jQuery('.panel-heading > .panel-title.h1').text("Iniciar sesión");
            jQuery('.form-login').show();
            jQuery('.form-register').hide();
        });
    };
    
    return {
        init: function(){
            handleLogin();
            handleRegister();
        }
    };
}();

jQuery(document).ready(function() {
    jQuery('.form-register').hide();
    Login.init();
});