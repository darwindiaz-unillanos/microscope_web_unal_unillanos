/**
 * This helper class is devised to easy the edition
 * of polygon points. It encapsulates the behaviour of
 * the point edition handler.
 */
edu.wsi.view.PointHandler = function(parameters) {    
    var index = parameters.index;
    var center = parameters.point;
    var lineWidth = parameters.lineWidth;
    
    // This reference is needed to perform coordinate transformations
    // Another option could be to declare it public static from the canvas
    var viewer = parameters.openSeadragonViewer;
    var onCenterChangedHandler = parameters.centerChangedHandler;
    var onMovingCenterHandler = parameters.movingCenterHandler;
    
    var svgCircle = parameters.paper.circle(
        center.x, center.y, lineWidth * 1.25
    );
    
    var offAttributes = {
        "fill": "#bbb",
        "stroke": "#555",
        "strokeWidth": lineWidth * 0.25
    };
        
    var onAttributes = {
        "fill": "#008000",
        "stroke": "#fff",
        "strokeWidth": lineWidth * 0.25
    };
    
    var onMouseOver = function() {
        svgCircle.attr(onAttributes);
    };
    
    var onMouseOut = function() {
        svgCircle.attr(offAttributes);
    };
    
    var onMouseMove = function(event) {
        center = new OpenSeadragon.Point(event.clientX, event.clientY);
        center = viewer.viewport.viewerElementToImageCoordinates(center);
        
        svgCircle.animate({
            cx: center.x,
            cy: center.y
        }, 5); // 0 miliseconds causes some problems

        // Reporting movement of center
        onMovingCenterHandler(index, center);
    };
    
    var onMouseUp = function() {
        svgCircle.node.closest('svg').removeEventListener('mouseup', onMouseUp);
        svgCircle.node.closest('svg').removeEventListener('mousemove', onMouseMove);
        
        // Center has been changed by mousemove event
        center = new OpenSeadragon.Point(
            Math.round(center.x),
            Math.round(center.y)
        );

        // Reporting change of center
        onCenterChangedHandler(index, center);
    };
    
    var onMouseDown = function(event) {
        //console.log("point clicked -> [ " + index + " ], " + center);
        alert("!!!!!!!!1");
        event.preventDefault();
        event.stopPropagation();
        
        svgCircle.node.closest('svg').addEventListener('mouseup', onMouseUp);
        svgCircle.node.closest('svg').addEventListener('mousemove', onMouseMove);
    };
    
    var remove = function() {
        svgCircle.unmouseout(onMouseOut);
        svgCircle.unmouseover(onMouseOver);
        svgCircle.unmousedown(onMouseDown);
        svgCircle.unmouseup(onMouseUp);
        svgCircle.remove();
    };
    
    svgCircle.attr(offAttributes);
    svgCircle.mouseout(onMouseOut);
    svgCircle.mouseover(onMouseOver);
    svgCircle.mousedown(onMouseDown);
    svgCircle.mouseup(onMouseUp);
    
    return {
        remove: remove
    };
};


edu.wsi.model.Region = function() {
    this.color;
    this.annotation;
    this.createdAtZoom = 0;
    this.regionType = edu.wsi.util.RegionType.DEFAULT;
    
    this.dispose = function() {};
    this.toJSON = function() {return {};};
};


edu.wsi.model.Polygon = function() {
    this.pointsArray = new Array();
    this.regionType = edu.wsi.util.RegionType.POLYGON;
    
    /**
     * Adds a new point for this polygon at the end of the list
     * @param {OpenSeadragon.Point} point
     */
    this.addPoint = function(point) {
        this.pointsArray.push(point);
    };
    
    /**
     * Allows to apply transformations to all points in the polygon. 
     * Specifically, this function is useful to perform coordinate transforms.
     * @param {function} transformFunction is the function that will 
     * be called for each one of the points in this manner:
     * transformFunction(element, index, array)
     */
    this.transform = function(transformFunction) {
        this.pointsArray.forEach(transformFunction);
    };
    
    /**
     * Calculates the mean of all points in the array 
     * and returns it as the center.
     * @returns {OpenSeadragon.Point} the center of the polygon
     */
    this.calculateCenter = function() {
        var centerX = 0;
        var centerY = 0;
        
        this.pointsArray.forEach(function(point) {
            centerX += point.x;
            centerY += point.y;
        });
        
        centerX /= this.pointsArray.length;
        centerY /= this.pointsArray.length;
        
        return new OpenSeadragon.Point(centerX, centerY);
    };
    
    /**
     * Function override for JSON.stringify
     */
    this.toJSON = function() {
        return {
            annotation: this.annotation,
            color: this.color,
            createdAtZoom: this.createdAtZoom,
            regionDefinition: this.pointsArray,
            regionType: this.regionType
        };
    };
};
edu.wsi.model.Polygon.prototype = new edu.wsi.model.Region(); // Polygon inherits from Region


edu.wsi.view.Region = function() {
    var id; // Auto generated identifier
    var selected = false;
    
    var model;
    var svgPaper; // Every region needs to know how to redraw itself
    var openSeadragonViewer; // Copy of the viewer for coordinate transforms
    var svgObject;
    
    var pointHandlers = new Array();
    
    var attachEvents = function() {
        svgObject.node.addEventListener("mouseover", onMouseOver);
        svgObject.node.addEventListener("mouseout", onMouseOut);
        svgObject.node.addEventListener("click", onMouseClick);
    };
    
    var removeEvents = function() {
        svgObject.node.removeEventListener("mouseover", onMouseOver);
        svgObject.node.removeEventListener("mouseout", onMouseOut);
        svgObject.node.removeEventListener("click", onMouseClick);
    };
    
    /**
     * Allows modification of points inside the polygon by means
     * of dragging circles that are shown on top of each one
     */
    var showPolygonEditMode = function() {
        // Used only in edit polygon mode
        var editPreviewLines = new Array();
        
        // Preparing handlers array
        if(pointHandlers === null) {
            pointHandlers = new Array();
        }
        
        // Display a circle for each point
        for(var i = 0; i < model.pointsArray.length; i++) {
            var parameters = {
                index: i,
                paper: svgPaper,
                point: model.pointsArray[i],
                openSeadragonViewer: openSeadragonViewer,
                lineWidth: edu.wsi.view.Canvas.lineWidth / model.createdAtZoom,
                centerChangedHandler: function(index, newCenter) {
                    model.pointsArray[index] = newCenter;
                    
                    // Exiting edit mode first
                    redraw();
                    showPolygonEditMode();
                    
                    // Clearing edit preview lines
                    for(var i = 0; i < editPreviewLines.length; i++) {
                        editPreviewLines[i].remove();
                    }
                    editPreviewLines.length = 0;
                    //console.log("Point [" + index + "] changed center to " + newCenter);
                },
                movingCenterHandler: function(index, newCenter) {
                    var previousIndex = index - 1 >= 0 ? index - 1 : model.pointsArray.length - 1;
                    var nextIndex = index + 1 < model.pointsArray.length ? index + 1 : 0;
                    
                    // Create the lines if they don't exist
                    if(editPreviewLines.length === 0) {
                        var editPreviewLineAttributes = {
                            "stroke": "#000",
                            "strokeWidth": edu.wsi.view.Canvas.lineWidth / model.createdAtZoom / 2,
                            "stroke-opacity": "0.4"
                        };
                        
                        editPreviewLines.push(svgPaper.line(
                            newCenter.x, newCenter.y, 
                            model.pointsArray[previousIndex].x, model.pointsArray[previousIndex].y
                        ));
                
                        editPreviewLines.push(svgPaper.line(
                            newCenter.x, newCenter.y, 
                            model.pointsArray[nextIndex].x, model.pointsArray[nextIndex].y
                        ));
                
                        editPreviewLines[0].attr(editPreviewLineAttributes);
                        editPreviewLines[1].attr(editPreviewLineAttributes);
                        
                        svgObject.before(editPreviewLines[0]);
                        svgObject.before(editPreviewLines[1]);
                    } else {
                        editPreviewLines[0].animate({
                            x1: newCenter.x,
                            y1: newCenter.y
                        }, 5); // 0 miliseconds causes some problems
                        
                        editPreviewLines[1].animate({
                            x1: newCenter.x,
                            y1: newCenter.y
                        }, 5); // 0 miliseconds causes some problems
                    }
                }
            };
            
            pointHandlers.push(new edu.wsi.view.PointHandler(parameters));
        }        
    };
    
    var hidePolygonEditMode = function() {
        if(pointHandlers !== null) {
            // Removing point handlers from view
            for(var i = 0; i < pointHandlers.length; i++) {
                pointHandlers[i].remove();
            }
        }
        
        pointHandlers = null;
    };
    
    var goToEditMode = function() {
        unselect();
        removeEvents();
        showPolygonEditMode();
    };
    
    var exitEditMode = function() {
        hidePolygonEditMode();
        attachEvents();
        select();
    };
    
    var createPolygon = function() {        
        var points = Array();
        for(var i = 0; i < model.pointsArray.length; i++) {
            points.push(model.pointsArray[i].x);
            points.push(model.pointsArray[i].y);
        }
        
        svgObject = svgPaper.polygon(points);
        svgObject.attr({
            "id": id,
            "fill-opacity": 0.05,
            "fill": model.color,
            "stoke-opacity": 1,
            "stroke": model.color,
            "strokeWidth": edu.wsi.view.Canvas.lineWidth / model.createdAtZoom
        });
    };
    
    /*
     * This function removes all svg objects from the view
     * and recreates them WITHOUT any event removal/recreation
     */
    var redraw = function() {
        svgObject.remove();
        hidePolygonEditMode();
        
        createPolygon();
    };
    
    var create = function(_svgPaper, _openSeadragonViewer, _model) {
        edu.wsi.view.Region.counter++; // Incrementing the counter
        id = "region-" + edu.wsi.view.Region.counter; // Generating region id
        
        model = _model;
        svgPaper = _svgPaper;
        openSeadragonViewer = _openSeadragonViewer;
        
        switch(model.regionType) {
            case edu.wsi.util.RegionType.POLYGON:
                createPolygon();
                break;
        }
        
        attachEvents();
    };
    
    var getCenter = function() {
        return model.calculateCenter();
    };
    
    var getZoom = function() {
        return model.createdAtZoom;
    };
    
    var setColor = function(newColor) {
        model.color = newColor;
        svgObject.attr({
            "fill": model.color,
            "stroke": model.color
        });
    };
    
    var getColor = function() {
        return model.color;
    };
    
    var setAnnotation = function(newAnnotation) {
        model.annotation = newAnnotation;
    };
    
    var getAnnotation = function() {
        return model.annotation;
    };
    
    var dispose = function() {
        removeEvents();
        
        model = null;
        svgObject.remove();
    };
    
    /**
     * Allows to apply transformations to all points in the region. 
     * This function is Region type independent, each type handles it correctly. 
     * Specifically, this function is useful to perform coordinate transforms.
     * @param {function} transformFunction is the function that will 
     * be called for each one of the points in this manner:
     * transformFunction(element, index, array)
     */
    var transform = function(transformFunction) {
        model.transform(transformFunction);
    };    
    
    /* Functions to handle region states */
    var select = function() {
        svgObject.attr({
            "fill-opacity": 0.3,
            "strokeWidth": 2 * edu.wsi.view.Canvas.lineWidth / model.createdAtZoom
        });
        
        selected = true;
    };
    
    var highlight = function() {
        svgObject.attr({
            "fill-opacity": 0.02,
            "strokeWidth": 2 * edu.wsi.view.Canvas.lineWidth / model.createdAtZoom
        });
    };
    
    var unselect = function() {
        svgObject.attr({
            "fill-opacity": 0.02,
            "strokeWidth": edu.wsi.view.Canvas.lineWidth / model.createdAtZoom
        });
        
        selected = false;
    };
    
    /* Event handling */
    var onRegionClickHandler;
    var onRegionClick = function(handler) {
        onRegionClickHandler = handler;
    };
    
    var onMouseClick = function(event) {
        if(typeof onRegionClickHandler === "function") {
            onRegionClickHandler(event, id);
        }
    };
    
    var onMouseOver = function() {
        if(!selected) {
            highlight();
        }
    };
    
    var onMouseOut = function() {
        if(!selected) {
            unselect();
        };
    };
    
    /* Overrides */
    var toJSON = function() {
        return model.toJSON();
    };
    
    return {
        getId: function() {return id;},
        isSelected: function() {return selected;},
        
        create: create,
        select: select,
        dispose: dispose,
        unselect: unselect,
        highlight: highlight,
        transform: transform,
        onRegionClick: onRegionClick,
        
        getZoom: getZoom,
        setColor: setColor,
        getColor: getColor,
        getCenter: getCenter,
        setAnnotation: setAnnotation,
        getAnnotation: getAnnotation,
        
        goToEditMode: goToEditMode,
        exitEditMode: exitEditMode,
        
        toJSON: toJSON
    };
};

/*
 * Counter of created regions for identification purposes.
 * This counter will increase only, if a region is deleted
 * it will remain the same. This counter is used to give
 * each region an id that can be used later to access each
 * individual region's properties
 */
edu.wsi.view.Region.counter = 0;