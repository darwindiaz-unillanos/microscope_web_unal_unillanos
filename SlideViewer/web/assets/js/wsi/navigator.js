/* 
 * Scripts to handle WSI navigation using OpenSeaDragon
 */

edu.wsi.view.Navigator = function (_htmlElementId) {
    var viewerHandler;
    var wsiCurrentData;
    var htmlElementId = _htmlElementId;
    var wsiProvider = new edu.wsi.model.WSIProvider();
    var viewerOverlayHandler = new edu.wsi.view.Canvas();
    var interactionPlayerHandler = new edu.wsi.view.InteractionPlayer();
    var imgFile;
    var userId;
    var maxBatchSize = 10;
    var batchSize = 0;
    var data = "";
    var recording = true;
    var _this;
    var _dataWidget;

    var initialize = function (wsiName, readMode, navigator,uid,dataWidget) {
        _this = navigator;
        _dataWidget=dataWidget;
        imgFile = wsiName;
        userId=uid;
        wsiProvider.getInfo(wsiName, readMode, wsiLoaded);
        // Forcing full screen
        $(window).resize(onWindowResize);
        $(window).resize();
    };

    var onWindowResize = function () {
        $("#" + htmlElementId).css({
            height: $(window).height()
        });

        if (typeof viewerOverlayHandler !== 'undefined' &&
                viewerOverlayHandler !== null) {
            viewerOverlayHandler.resize();
        }
    };

    var loadViewport = function () {
        viewerHandler = OpenSeadragon({
            id: htmlElementId,
            navigatorPosition: "BOTTOM_RIGHT",
            showNavigator: true,            
            imageLoaderLimit: 0,
            zoomInButton: "zoom-in",
            zoomOutButton: "zoom-out",
            homeButton: "home",
            fullPageButton: "full-page",
            animationTime: .8,
            tileSources: {
                width: wsiCurrentData.getWidth(),
                height: wsiCurrentData.getHeight(),
                tileSize: 512,
                minLevel: 0,
                maxLevel: wsiCurrentData.getTotalLayers(),
                getTileUrl: function (layer, tileX, tileY) {
                    return wsiProvider.buildTileUrl(wsiCurrentData, tileX, tileY, layer);
                }
            },
            gestureSettingsMouse: {
                clickToZoom: false,
                dblClickToZoom: false
            }
        });

        var x, y, cx, cy, t, z;
        var imagingHelper = viewerHandler.activateImagingHelper({onImageViewChanged: trackNavigation});

        viewerHandler.addHandler("open", function () {
            $.ajax({
                type: "post",
                url: "/SlideViewer/track",
                //url: "/microscopio/track",
                data: "data=" + imgFile +","+wsiCurrentData.getWidth()+","+wsiCurrentData.getHeight()+","+userId
            });
            viewerOverlayHandler.initialize(viewerHandler, wsiCurrentData.getWidth(), wsiCurrentData.getHeight(),imgFile);
            interactionPlayerHandler.initialize(viewerHandler, imagingHelper, _this, imgFile);
            
            //Level restriction
            viewerHandler.viewport.zoomTo(0.1);
        });

        //Level restriction
        viewerHandler.zoomLevels({
            levels: [0.1, 0.25, 1]
        });

        viewerHandler.scalebar({
            type: OpenSeadragon.ScalebarType.MICROSCOPY,
            pixelsPerMeter: 80,
            minWidth: "100px",
            location: OpenSeadragon.ScalebarLocation.BOTTOM_LEFT,
            sizeAndTextRenderer: OpenSeadragon.ScalebarSizeAndTextRenderer.MICROSCOPY_LENGTH,
            xOffset: 5,
            yOffset: 10,
            stayInsideImage: true,
            color: "rgb(150, 150, 150)",
            fontColor: "rgb(100, 100, 100)",
            backgroundColor: "rgba(255, 255, 255, 0.5)",
            fontSize: "medium",
            barThickness: 2
        });

        viewerHandler.addHandler("canvas-double-click", trackDoubleClickedPoint);

        function trackNavigation(event) {
            t = new Date().getTime();
            z = (event.zoomFactor).toFixed(4);
            x = Math.round(imagingHelper.logicalToDataX(event.viewportOrigin.x));
            y = Math.round(imagingHelper.logicalToDataY(event.viewportOrigin.y));
            cx = Math.round(imagingHelper.logicalToDataX(event.viewportCenter.x));
            cy = Math.round(imagingHelper.logicalToDataY(event.viewportCenter.y));
            
            var nx=Math.round(imagingHelper.physicalToDataX(event.viewportCenter.x));
            var ny=Math.round(imagingHelper.physicalToDataY(event.viewportCenter.y));
            
            console.log(nx+","+ny+","+x+ "," + y + "," + cx + "," + cy + "," + z + ";");
            if (recording) {
                data += t + "," + x + "," + y + "," + cx + "," + cy + "," + z + ";";                
                processBatch();
            }
        }

        function trackDoubleClickedPoint(event) {
            var dataX = Math.round(imagingHelper.physicalToDataX(event.position.x));
            var dataY = Math.round(imagingHelper.physicalToDataY(event.position.y));
            data += "0," + dataX + "," + dataY + ",0,0,0;";
            processBatch();
        }
    };

    var processBatch = function () {
        batchSize++;
        if (batchSize === maxBatchSize) {
            sendTrackingData();
        }
    };

    var sendTrackingData = function () {
        $.ajax({
            type: "post",
            url: "/SlideViewer/track",
            //url: "/microscopio/track",
            data: "data=" + data
        });
        batchSize = 0;
        data = "";
    };

    var wsiLoaded = function (wsiData) {
        if (wsiData !== null) {
            wsiCurrentData = wsiData;
            var description=wsiCurrentData.getDescription();
            if(description!==""){
                _dataWidget.html(description);
            }
            loadViewport();
        } else {
            console.log("Couldn't grab data from provider");
        }
    };

    var isRecording = function (rec) {
        recording = rec;
    };

    return {
        initialize: initialize,
        sendTrackingData: sendTrackingData,
        isRecording: isRecording
    };
};