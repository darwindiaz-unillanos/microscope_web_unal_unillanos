edu.wsi.model.Canvas = function () {

    var regions = new Array();
    var selectedRegion = null;

    var loadAnnotations = function (formData, onSuccess, onError) {
        $.ajax({
            type: 'POST',
            url: 'load_annotations',
            success: onSuccess,
            error: onError,
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            accepts: "application/json"
        });
    };
    /*+*/
    var saveAnnotations = function(data, image, onSuccess, onError){
        $.ajax({
            type: 'POST',
            url: 'save_annotations',
            success: onSuccess,
            error: onError,
            data: { "annotations":data,"image": image},
            cache: false,
            accepts: "text/plain"
      });  
    };
    
    var loadAnnotationsdb = function(data, role, onSuccess, onError){
        $.ajax({
            type: 'POST',
            url: 'load_annotationsdb',
            success: onSuccess,
            error: onError,
            data: {"image": data, "role": role},
            cache: false,
            accepts: "application/json"
        });
    };
    
    var privileges = function(data, onSuccess, onError){
        $.ajax({
            type: 'POST',
            url: 'privileges',
            success: onSuccess,
            error: onError,
            data: {"roles": data},
            cache: false,
            accepts: "application/json"
        });
    };
    
    var removeRegionById = function (id) {
        var regionIndex = findRegionIndexById(id);
        if (regionIndex !== null) {
            regions[regionIndex].dispose();
            regions.splice(regionIndex, 1);
        }
    };

    var getRandomColor = function () {
        var color = "" + Math.floor(Math.random() * 16777215).toString(16);
        if (color.length > 5) {
            return "#" + color;
        } else {
            return "#0" + color;
        }
    };

    var findRegionById = function (id) {
        for (var index = 0; index < regions.length; index++) {
            if (regions[index].getId() === id) {
                return regions[index];
            }
        }

        return null;
    };

    var findRegionIndexById = function (id) {
        for (var index = 0; index < regions.length; index++) {
            if (regions[index].getId() === id) {
                return index;
            }
        }

        return null;
    };

    var clearSelection = function () {
        if (selectedRegion !== null) {
            selectedRegion.exitEditMode();
            selectedRegion.unselect();
        }

        selectedRegion = null;
    };

    var goToEditModeOnSelectedRegion = function () {
        if (selectedRegion !== null) {
            selectedRegion.goToEditMode();
        }
    };

    var exitEditModeOnSelectedRegion = function () {
        if (selectedRegion !== null) {
            selectedRegion.exitEditMode();
        }
    };

    var selectRegionById = function (id) {
        var region = findRegionById(id);
        if (region !== null) {
            selectedRegion = region;
            selectedRegion.select();
        }
    };

    return {
        addRegion: function (region) {
            regions.push(region);
        },
        getSelectedRegion: function () {
            return selectedRegion;
        },
        exportToJSON: function () {
            return encodeURIComponent(JSON.stringify(regions));
        },/*+*/
        saveToJSON: function(){
            if(regions.length > 0)
                return JSON.stringify(regions);
            else
                return null
        },
        clearSelection: clearSelection,
        getRandomColor: getRandomColor,
        loadAnnotations: loadAnnotations,
        saveAnnotations: saveAnnotations,
        loadAnnotationsdb: loadAnnotationsdb,
        privileges: privileges,
        removeRegionById: removeRegionById,
        selectRegionById: selectRegionById,
        goToEditModeOnSelectedRegion: goToEditModeOnSelectedRegion,
        exitEditModeOnSelectedRegion: exitEditModeOnSelectedRegion
    };
};

edu.wsi.view.Canvas = function () {
    // Constants
    var LINE_WIDTH_PIXELS = 2;
    var MINIMUM_DISTANCE = 25;

    // This property is initialized once on viewport creation 
    // and does not change afterwards. Thus, declared static
    edu.wsi.view.Canvas.lineWidth;

    var viewer;
    var overlay;
    var svgPaper;
    var svgHtmlNode;
    var infoHtmlNode;
    var wsiName;
    var lastActivePoint;
    var drawingLineWidth;
    var previewLastLine = null;
    var previewStartCircle = null;
    var serverAnnotations = null;
    var previewPoints = new Array();
    var previewPolygonLines = new Array();
    var lastClickOnRegion = false;
    var drawingOperation = edu.wsi.util.DrawingOperation.NONE;
    var model = new edu.wsi.model.Canvas();
    var wsiProvider = new edu.wsi.model.WSIProvider();
    var user_roles = new Array();
    var user_privileges = new Array();

    var initialize = function (openSeadragonViewer, width, height, imgFile) {
        viewer = openSeadragonViewer;
        overlay = viewer.svgOverlay(width, height);
        svgPaper = new Snap(overlay.node());
        wsiName = imgFile;

        // Calculating the equivalent to LINE_WIDTH_PIXELS in image coordinates
        edu.wsi.view.Canvas.lineWidth = viewer.viewport.windowToImageCoordinates(
                new OpenSeadragon.Point(0, 0)
                ).distanceTo(viewer.viewport.windowToImageCoordinates(
                new OpenSeadragon.Point(LINE_WIDTH_PIXELS, 0)
                ));
        
        loadPrivilegesUser();
        
        // Container box
        var r = svgPaper.rect(0, 0, width, height);
        r.attr({
            fill: 'none',
            stroke: "#000",
            //strokeWidth: lineWidthFromCurrentZoom() * 2
            strokeWidth: lineWidthFromCurrentZoom()
        });
    };

    var loadPrivilegesUser = function(){
        var elementRoles = $("div > span.rol");
        for (var i = 0; i < elementRoles.length; i++) {
            user_roles.push($(elementRoles[i]).text());
        }
        model.privileges(user_roles.toString(), loadPrivilegesSuccess, loadPrivilegesError);
    };

    var loadPrivilegesSuccess = function(data){
        //privileges by roles
        var dataJson = JSON.parse(data);
        for (var item in dataJson) {
            user_privileges.push(dataJson[item]["privilege"]);
        }
        //conditions roles
        if(user_privileges.indexOf("read") !== -1){
            $("#actions-toolbar").show();
            $("#actions-toolbar > .dropdown[title='Data Management']").attr("style","display:inline-block");
            $("#load-data").attr("style","display:inline-block");
            $("#import-data").parent().attr("style","display:inline-block");
        }
        if(user_privileges.indexOf("create") !== -1){
            $("#actions-toolbar").show();
            $("#actions-toolbar > .dropdown[title='Add Annotation']").attr("style","display:inline-block");
            $("#actions-toolbar > .dropdown[title='Data Management']").attr("style","display:inline-block");
            $("#save-data").attr("style","display:inline-block");
            $("#export-data").attr("style","display:inline-block");
        }
        attachEvents();
    };
    
    var loadPrivilegesError = function(error){
        alert("An error has occurred while trying to load the privileges");
        console.log(error.responseText);
    };
    
    var attachEvents = function () {
        $(window).keyup(onKeyPress);

        svgHtmlNode = $(overlay.node().closest('svg'));
        
        //This is for Chrome
        svgHtmlNode.on('pointerdown', onViewerPress);
        svgHtmlNode.on('pointerup', onViewerRelease);        
        //This is for Firefox
        svgHtmlNode.on('mousedown', onViewerPress);
        svgHtmlNode.on('mouseup', onViewerRelease);
        
        svgHtmlNode.on('click', onViewerClick);

        infoHtmlNode = $("#infobox"); // It should be better to create the element and don't rely on user to do it
        infoHtmlNode.find("#goto-region").on("click", onGoToRegionClick);
        infoHtmlNode.find("#remove-region").on("click", onRemoveRegionClick);
        infoHtmlNode.find("#edit-annotation").on("click", onEditAnnotationClick);

        $(".add-region").on("click", onAddRegionClick);
        $("#export-data").on("click", onExportDataClick);
        $("#edit-region").on("click", onEditRegionClick);
        $("#accept-edit-region").on("click", onAcceptEditRegionClick);
        $("#annotations-dropdown-list").on("click", ".goto-region", onAnnotationsListClick);
        $("#edit-annotation-modal-accept").on("click", onEditRegionModalAccept);
        $("#load-annotations-modal-accept").on("click", onLoadAnnotationsModalAccept);
        /*+*/
        $("#save-data").on("click", onSaveDataClick);
        $("#load-data").on("click", onLoadDataClick);
        $("#annotation").on("click", function () {
            if (serverAnnotations === null) {
                $("#loading-modal").modal();
                wsiProvider.getServerAnnotation(wsiName, serverAnnotationLoaded);
            }
        });
    };

    function serverAnnotationLoaded(data) {
        $("#loading-modal").modal('toggle');
        if (data !== null && data !== "") {
            serverAnnotations = data;
            onLoadAnnotationsSuccess(data);
        } else {
            toastr["error"]("Error al cargar las anotaciones");
        }
    }

    var clearPreview = function () {
        for (var i = 0; i < previewPolygonLines.length; i++) {
            previewPolygonLines[i].remove();
            previewPolygonLines[i] = null;
        }
        previewPolygonLines.length = 0;

        for (var i = 0; i < previewPoints.length; i++) {
            previewPoints[i] = null;
        }
        previewPoints.length = 0;

        if (previewStartCircle !== null) {
            // Removing event handlers for start point
            previewStartCircle.unclick();
            previewStartCircle.unmouseout();
            previewStartCircle.unmouseover();

            previewStartCircle.remove();
            previewStartCircle = null;
        }

        if (previewLastLine !== null) {
            previewLastLine.remove();
            previewLastLine = null;
        }
    };

    var drawPreviewStartCircle = function (center) {
        var offAttributes = {
            "fill": "#F2C5C2",
            "stroke": "#000",
            "strokeWidth": drawingLineWidth * 0.75
        };

        var onAttributes = {
            "fill": "#D7F2C2",
            "stroke": "#000",
            "strokeWidth": drawingLineWidth * 0.5
        };

        previewStartCircle = svgPaper.circle(center.x, center.y, drawingLineWidth * 2);
        previewStartCircle.attr(offAttributes);

        // Attaching events to the start circle
        previewStartCircle.mouseover(function () {
            previewStartCircle.attr(onAttributes);
        });

        previewStartCircle.mouseout(function () {
            previewStartCircle.attr(offAttributes);
        });

        previewStartCircle.click(function () {
            finishCurrentRegion();
        });
    };

    var drawNewPreviewLine = function (source, target) {
        var line = svgPaper.line(source.x, source.y, target.x, target.y);
        line.attr({
            "stroke": "#000",
            "strokeWidth": drawingLineWidth
        });

        // Starting point always on top
        if (previewStartCircle !== null) {
            previewStartCircle.before(line);
        }

        previewPolygonLines.push(line);
        if (previewPolygonLines.length >= 2) {
            // Removing previous line
            if (previewLastLine !== null) {
                previewLastLine.remove();
                previewLastLine = null;
            }

            previewLastLine = svgPaper.line(
                    target.x, target.y,
                    previewPoints[0].x, previewPoints[0].y
                    );

            previewLastLine.attr({
                "stroke": "#000",
                "strokeWidth": drawingLineWidth / 2,
                "stroke-opacity": "0.4"
            });

            previewStartCircle.before(previewLastLine);
        }
    };

    var addNewPreviewPoint = function (point) {
        var totalPoints = previewPoints.length;
        var viewportPoint = windowToViewportCoordinates(point);

        if (totalPoints === 0) {
            drawingLineWidth = lineWidthFromCurrentZoom();
            drawPreviewStartCircle(viewportPoint);
        } else {
            drawNewPreviewLine(previewPoints[totalPoints - 1], viewportPoint);
        }

        previewPoints.push(viewportPoint);
    };

    var addPolygonToCanvas = function (polygonModel) {
        var region = new edu.wsi.view.Region();
        region.create(svgPaper, viewer, polygonModel);

        // Attach click event to the region
        region.onRegionClick(onRegionClick);

        model.addRegion(region);
        addAnnotationToDropdownList(region);
    };

    var addPolygon = function () {
        var regionModel = new edu.wsi.model.Polygon();
        regionModel.color = model.getRandomColor();
        regionModel.annotation = "Default Annotation";
        regionModel.createdAtZoom = zoomFromCurrentLineWidth();

        for (var i = 0; i < previewPoints.length; i++) {
            regionModel.addPoint(previewPoints[i]);
        }

        addPolygonToCanvas(regionModel);
    };

    var addJSONPolygon = function (regionJSON) {
        var regionModel = new edu.wsi.model.Polygon();
        regionModel.color = regionJSON.color;
        regionModel.annotation = regionJSON.annotation;
        regionModel.createdAtZoom = regionJSON.createdAtZoom;

        for (var i = 0; i < regionJSON.regionDefinition.length; i++) {
            regionModel.addPoint(new OpenSeadragon.Point(
                    regionJSON.regionDefinition[i].x,
                    regionJSON.regionDefinition[i].y
                    ));
        }

        addPolygonToCanvas(regionModel);
    };

    var removeAnnotationFromDropdownList = function (region) {
        var annotationsList = $("#annotations-dropdown-list");
        var anchorElement = annotationsList.find("[data-region-id='" + region.getId() + "']");
        anchorElement.remove();

        if (annotationsList.children().length === 1) {
            annotationsList.find(".empty-placeholder").show();
            annotationsList.data("empty", true);
        }
    };

    var modifyAnnotationInDropdownList = function (region) {
        var annotationsList = $("#annotations-dropdown-list");
        var anchorElement = annotationsList.find("[data-region-id='" + region.getId() + "']");
        anchorElement.html(region.getAnnotation().substring(0, 32));
    };

    var addAnnotationToDropdownList = function (region) {
        var annotationsList = $("#annotations-dropdown-list");
        var listIsEmpty = annotationsList.data("empty");
        var anchorElement = "<a class='goto-region' href='#'"
                + " data-region-id='" + region.getId() + "'>"
                + region.getAnnotation().substring(0, 32)
                + "</a>";

        annotationsList.append(anchorElement);

        if (listIsEmpty) {
            annotationsList.find(".empty-placeholder").hide();
            annotationsList.data("empty", false);
        }
    };

    var finishCurrentRegion = function () {
        if (previewPoints.length >= 3) {
            // For now just polygons are allowed
            addPolygon();
        }

        clearPreview();
        cancelAddRegion();
    };

    var showCurrentRegionInfo = function () {
        var selectedRegion = model.getSelectedRegion();

        if (selectedRegion !== null) {
            infoHtmlNode.find("p").html(selectedRegion.getAnnotation());
            //conditions roles
            if(user_privileges.indexOf("delete") !== -1)
                $("#remove-region").attr("style","display:inline-block");
            if(user_privileges.indexOf("update") !== -1)
                $("#edit-annotation").attr("style","display:inline-block");
            showInfoWindow();
        } else {
            hideInfoWindow();
        }
    };

    var showInfoWindow = function () {
        infoHtmlNode.show();
    };

    var hideInfoWindow = function () {
        infoHtmlNode.hide();
    };

    var goToRegion = function (region) {
        var regionCenter = viewer.viewport.imageToViewportCoordinates(region.getCenter());
        viewer.viewport.panTo(regionCenter);
        viewer.viewport.zoomTo(region.getZoom());
    };

    var goToCurrentRegion = function () {
        var selectedRegion = model.getSelectedRegion();
        if (selectedRegion !== null) {
            goToRegion(selectedRegion);
        }
    };

    var startAddRegion = function (regionType) {
        drawingOperation = regionType;

        $(".add-region[data-region-type='" + regionType + "']").addClass("active");
        $(".add-region[data-region-type='" + regionType + "']").tooltip("hide"); // Force tooltip removal
    };

    var cancelAddRegion = function (regionType) {
        if (typeof regionType === 'undefined') {
            regionType = drawingOperation;
        }

        $(".add-region[data-region-type='" + regionType + "']").removeClass("active");
        $(".add-region[data-region-type='" + regionType + "']").tooltip("hide"); // Force tooltip removal

        drawingOperation = edu.wsi.util.DrawingOperation.NONE;
    };

    var exitEditRegionMode = function () {
        $("#edit-region").hide();
        $("#accept-edit-region").hide();
    };

    var enterEditRegionMode = function () {
        $("#edit-region").hide();
        $("#accept-edit-region").show();
    };

    var showEditRegionButton = function () {
        if(user_privileges.indexOf("create") !== -1 || user_privileges.indexOf("update") !== -1)
            $("#edit-region").show();
    };

    /* Event handling */
    var onKeyPress = function (event) {
        switch (event.keyCode) {
            case 27: // esc
                clearPreview();
                cancelAddRegion();
                break;
        }
    };

    var onExportDataClick = function () {
        exportJSON();
    };

    /*+*/
    var onSaveDataClick = function(){
        var regions = model.saveToJSON();
        if (regions != null)
            model.saveAnnotations(regions, wsiName, onSaveAnnotationsSuccess, onSaveAnnotationsError);
    };
    
    var onSaveAnnotationsSuccess = function(data){
        toastr["success"]("Process success, saved annotations");
    };
    
    var onSaveAnnotationsError = function (data) {
        alert("An error has occurred while trying to save the annotations");
        console.log(data.responseText);
    };
    
    var onLoadDataClick = function(){
        var role;
        if(user_roles.indexOf("student") !== -1)
            role = user_roles[user_roles.indexOf("student")];
        if(user_roles.indexOf("pathologist") !== -1)
            role = user_roles[user_roles.indexOf("pathologist")];

        model.loadAnnotationsdb(wsiName, role, onLoadAnnotationsSuccess, onLoadAnnotationsError);
    };

    /*+*/

    var onEditAnnotationClick = function () {
        var selectedRegion = model.getSelectedRegion();

        if (selectedRegion !== null) {
            var modal = $("#edit-annotation-modal");

            // Filling out modal with region data
            modal.find("[name='color']").val(selectedRegion.getColor());
            modal.find("[name='annotation']").val(selectedRegion.getAnnotation());

            modal.modal("show");
        }
    };

    var onLoadAnnotationsSuccess = function (annotations) {
        if (annotations.length > 0) {
            for (var i = 0; i < annotations.length; i++) {
                addJSONPolygon(annotations[i]);
            }
        } else {
            toastr["warning"]("No hay anotaciones disponibles para esta imagen");
        }
    };

    var onLoadAnnotationsError = function (data) {
        alert("An error has occurred while" +
                " trying to load the annotations file");
        console.log(data.responseText);
    };

    var onLoadAnnotationsModalAccept = function () {
        var formData = new FormData(
                document.getElementById('load-annotations-modal-form')
                );

        model.loadAnnotations(formData, onLoadAnnotationsSuccess, onLoadAnnotationsError);
    };

    var onEditRegionModalAccept = function () {
        var selectedRegion = model.getSelectedRegion();

        if (selectedRegion !== null) {
            var modal = $("#edit-annotation-modal");
            var color = modal.find("[name='color']").val();
            var annotation = modal.find("[name='annotation']").val();

            selectedRegion.setColor(color);
            selectedRegion.setAnnotation(annotation);

            showCurrentRegionInfo();
            modal.modal("hide");

            // Editing dropdown list
            modifyAnnotationInDropdownList(selectedRegion);
        }
    };

    var onRemoveRegionClick = function () {
        var selectedRegion = model.getSelectedRegion();
        removeAnnotationFromDropdownList(selectedRegion);

        model.clearSelection();
        model.removeRegionById(selectedRegion.getId());

        hideInfoWindow();
        exitEditRegionMode();
    };

    var onAddRegionClick = function (event) {
        var regionType = $(event.target).closest("button").data("region-type");

        if (drawingOperation === regionType) {
            cancelAddRegion(regionType);
        } else {
            startAddRegion(regionType);
        }
    };

    var onAnnotationsListClick = function (element) {
        var regionId = $(element.target).data("region-id");

        model.clearSelection();
        model.selectRegionById(regionId);

        goToCurrentRegion();
        showEditRegionButton();
        showCurrentRegionInfo();
    };

    var onGoToRegionClick = function () {
        goToCurrentRegion();
    };

    var onViewerDrag = function (event) {
        var newPoint = new OpenSeadragon.Point(event.pageX, event.pageY);

        switch (drawingOperation) {
            case edu.wsi.util.DrawingOperation.FREEHAND:
            {
                if (lastActivePoint.distanceTo(newPoint) > MINIMUM_DISTANCE) {
                    addNewPreviewPoint(newPoint);
                    lastActivePoint = newPoint;
                }
                break;
            }
        }
    };

    var onViewerPress = function (event) {
        lastActivePoint = new OpenSeadragon.Point(event.pageX, event.pageY);

        switch (drawingOperation) {
            case edu.wsi.util.DrawingOperation.FREEHAND:
            {
                event.preventDefault();
                event.stopPropagation();
                svgHtmlNode.on('mousemove', onViewerDrag);
                svgHtmlNode.on('pointermove', onViewerDrag);

                // Starting polygon
                addNewPreviewPoint(lastActivePoint);
                break;
            }
        }
    };

    var onViewerRelease = function () {
        switch (drawingOperation) {
            case edu.wsi.util.DrawingOperation.FREEHAND:
            {
                svgHtmlNode.off('mousemove', onViewerDrag);
                svgHtmlNode.off('pointermove', onViewerDrag);
                finishCurrentRegion();
                break;
            }
        }
    };

    var onViewerClick = function (event) {
        var noDrag = false;
        var newPoint = new OpenSeadragon.Point(event.pageX, event.pageY);

        // Verifying if it was an actual click rather than drag
        if (lastActivePoint.distanceTo(newPoint) < 2
                && !lastClickOnRegion) {
            noDrag = true;
        }

        switch (drawingOperation) {
            case edu.wsi.util.DrawingOperation.NONE:
            {
                if (noDrag) {
                    hideInfoWindow();
                    exitEditRegionMode();
                    model.clearSelection();
                } else {
                    showCurrentRegionInfo();
                }

                // Resetting value
                lastClickOnRegion = false;
                break;
            }
            case edu.wsi.util.DrawingOperation.POLYGON:
            {
                if (noDrag) {
                    addNewPreviewPoint(newPoint);
                }
            }
        }

        lastActivePoint = newPoint;
    };

    var onEditRegionClick = function () {
        enterEditRegionMode();
        model.goToEditModeOnSelectedRegion();
    };

    var onAcceptEditRegionClick = function () {
        exitEditRegionMode();
        showEditRegionButton();

        model.exitEditModeOnSelectedRegion();
    };

    var onRegionClick = function (event, regionId) {
        var newPoint = new OpenSeadragon.Point(event.pageX, event.pageY);

        // Only allow region selection for navigation mode
        if (drawingOperation === edu.wsi.util.DrawingOperation.NONE) {
            if (lastActivePoint.distanceTo(newPoint) < 2) {
                model.clearSelection();
                model.selectRegionById(regionId);

                showEditRegionButton();
                lastClickOnRegion = true;
            }
        }
    };

    var resize = function () {
        if (typeof overlay !== 'undefined' &&
                overlay !== null) {
            overlay.resize();
        }
    };

    /* Model Handling */
    var exportJSON = function () {
        var a = document.createElement('a');
        a.href = 'data:application/json,' + model.exportToJSON();
        a.target = '_blank';
        a.download = wsiName + '.json';

        document.body.appendChild(a);
        a.click();
        a.remove();
    };

    var windowToViewportCoordinates = function (point) {
        var viewportPoint = viewer.viewport.windowToImageCoordinates(point);
        return new OpenSeadragon.Point(
                Math.round(viewportPoint.x),
                Math.round(viewportPoint.y)
                );
    };

    var lineWidthFromCurrentZoom = function () {
        var currentZoom = viewer.viewport.getZoom();
        return edu.wsi.view.Canvas.lineWidth / currentZoom;
    };

    var zoomFromCurrentLineWidth = function () {
        return edu.wsi.view.Canvas.lineWidth / drawingLineWidth;
    };

    return {
        resize: resize,
        initialize: initialize
    };
};