/* 
 * Scripts to handle WSI navigation using OpenSeaDragon
 */

edu.wsi.view.ImageList = function() {
    var wsiProvider = new edu.wsi.model.WSIProvider();
    
    var initialize = function(cat) {
        wsiProvider.getList(listLoaded,cat);
    };
    
    var buildThumbnail = function(imageName, readMode) {
        var htmlContent = "<div class='col-xs-6 col-md-3'>"
            + "<a href='"
            //+ "<a target='_blank' href='"
            //+"http://172.17.0.111::8084/SlideViewer/viewer?"
            + "http://localhost:8084/SlideViewer/viewer?"
            //+ "http://168.176.61.6:8082/SlideViewer/viewer?"
            //+ "http://cimalab.unal.edu.co/microscopio/viewer?"
            + "wsi=" + imageName + "&reader=" + readMode 
            + "'class='thumbnail' title='" + imageName + "'>"
            + "<img src='" + wsiProvider.buildWSIThumbnailUrl(imageName,150,100) + "'" 
            + "alt='" + imageName + "' /><div class='thumbnail-text'>"+imageName+"</div>"
            + "</a></div>";
    
        return htmlContent;
    };
    
    var printList = function(wsiList) {
        var jpegImages = wsiList.jpeg;
        var openslideImages = wsiList.openslide;
        
        // Creating list for jpeg images
        for(var i = 0; i < jpegImages.length; i++) {
            $("#jpeg-images").append(buildThumbnail(jpegImages[i], "jpeg"));
        }
        
        // Creating list for openslide images
        for(var j = 0; j < openslideImages.length; j++) {
            $("#openslide-images").append(buildThumbnail(openslideImages[j], "openslide"));
        }
    };
    
    var listLoaded = function(wsiList) {
        if(wsiList !== null) {
            printList(wsiList);
        } else {
            console.log("Couldn't grab data from provider");
        }
    };
    
    return {
        initialize: initialize
    };
};