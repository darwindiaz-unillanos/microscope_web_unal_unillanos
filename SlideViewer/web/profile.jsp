<%-- 
    Document   : profile
    Created on : 22/03/2018, 08:47:57 PM
    Author     : darwi
--%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    if(session.getAttribute("user") == null || session.getAttribute("roles") == null){
        request.getRequestDispatcher("login.jsp").forward(request, response);
    }
    ArrayList<String> roles = (ArrayList)session.getAttribute("roles");
%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <!-- Plugins -->
        <link href="assets/css/plugins/bootstrap/bootstrap.min.css" rel="stylesheet" type="text/css">    
        <link href="assets/css/plugins/admin2/sb-admin-2.css" rel="stylesheet" type="text/css">
        <link href="assets/css/plugins/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/plugins/select2.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/plugins/jquery-ui.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/custom/styles.css" rel="stylesheet" type="text/css">    
        <title>CIM@LAB - WSI Viewer</title>
    </head>
    <body>
        <nav>
            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <% if(session != null){
                            if(roles != null){
                                if(roles.contains("student") || roles.contains("pathologist") || roles.contains("admin")){%>
                                    <li>
                                        <a href="index.jsp" class="disabled"><i class="fa fa-home fa-fw"></i>Home</a>
                                    </li>
                                <%}%>
                                <%for (String rol : roles){%>
                                    <li>
                                        <a href="#">
                                            <div name="user_role">
                                                <i class="fa fa-group fa-fw"></i>Rol
                                                <span class="pull-right text-muted small rol"><%=rol%></span>
                                            </div>
                                        </a>
                                    </li>
                                <%}%>
                                <li class="divider"></li>
                        <%}}%>
                        <li>
                            <a href="Logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
        <div class="container-fluid container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                   <h1 class="page-header">Welcome <%=session.getAttribute("user").toString().split("@")[0]%></h1>  
                </div>
            </div>
            <div class="row">
                <% if(roles.contains("admin")){%>
                <div class="col-lg-6 col-md-6">
                    <div class="panel-default">
                        <div class="panel-body">                            
                            <div class="">
                                <input type="text" class="typeahead form-control" placeholder="Search user...">
                            </div>
                        </div>
                    </div>
                </div>
                <%}%>
                <div id="content-profile" class="col-lg-12 col-md-12">
                    <div class="panel-primary">
                        <div class="panel-heading">
                            Basic Information User
                        </div>
                        <div class="panel-body">
                            <form class="form-profile" role="form" method="post" action="UserProfile" onsubmit="return false;">
                                <fieldset>
                                    <div class="form-group col-lg-6 col-md-6">
                                        <label class="control-label">First name</label>
                                        <input class="form-control" placeholder="" name="firstname" type="text" maxlength="100" autofocus>
                                    </div>
                                    <div class="form-group col-lg-6 col-md-6">
                                        <label class="control-label">Last name</label>
                                        <input class="form-control" placeholder="" name="lastname" type="text" maxlength="100">
                                    </div>
                                    <div class="form-group col-lg-6 col-md-6">
                                        <label class="control-label">Identification number</label>
                                        <input class="form-control" placeholder="" name="identification" type="number" maxlength="18" readonly>
                                    </div>
                                    <div class="form-group col-lg-6 col-md-6">
                                        <label class="control-label">Email</label>
                                        <input class="form-control" placeholder="" name="email" type="email" maxlength="100" readonly>
                                    </div>
                                    <div class="form-group col-lg-6 col-md-6">
                                        <label class="control-label">Country</label>
                                        <select class="form-control" name="country">
                                            <option value="AF">Afghanistan</option>
                                            <option value="AL">Albania</option>
                                            <option value="DZ">Algeria</option>
                                            <option value="AS">American Samoa</option>
                                            <option value="AD">Andorra</option>
                                            <option value="AO">Angola</option>
                                            <option value="AI">Anguilla</option>
                                            <option value="AR">Argentina</option>
                                            <option value="AM">Armenia</option>
                                            <option value="AW">Aruba</option>
                                            <option value="AU">Australia</option>
                                            <option value="AT">Austria</option>
                                            <option value="AZ">Azerbaijan</option>
                                            <option value="BS">Bahamas</option>
                                            <option value="BH">Bahrain</option>
                                            <option value="BD">Bangladesh</option>
                                            <option value="BB">Barbados</option>
                                            <option value="BY">Belarus</option>
                                            <option value="BE">Belgium</option>
                                            <option value="BZ">Belize</option>
                                            <option value="BJ">Benin</option>
                                            <option value="BM">Bermuda</option>
                                            <option value="BT">Bhutan</option>
                                            <option value="BO">Bolivia</option>
                                            <option value="BA">Bosnia and Herzegowina</option>
                                            <option value="BW">Botswana</option>
                                            <option value="BV">Bouvet Island</option>
                                            <option value="BR">Brazil</option>
                                            <option value="IO">British Indian Ocean Territory</option>
                                            <option value="BN">Brunei Darussalam</option>
                                            <option value="BG">Bulgaria</option>
                                            <option value="BF">Burkina Faso</option>
                                            <option value="BI">Burundi</option>
                                            <option value="KH">Cambodia</option>
                                            <option value="CM">Cameroon</option>
                                            <option value="CA">Canada</option>
                                            <option value="CV">Cape Verde</option>
                                            <option value="KY">Cayman Islands</option>
                                            <option value="CF">Central African Republic</option>
                                            <option value="TD">Chad</option>
                                            <option value="CL">Chile</option>
                                            <option value="CN">China</option>
                                            <option value="CX">Christmas Island</option>
                                            <option value="CC">Cocos (Keeling) Islands</option>
                                            <option value="CO">Colombia</option>
                                            <option value="KM">Comoros</option>
                                            <option value="CG">Congo</option>
                                            <option value="CD">Congo, the Democratic Republic of the</option>
                                            <option value="CK">Cook Islands</option>
                                            <option value="CR">Costa Rica</option>
                                            <option value="CI">Cote d'Ivoire</option>
                                            <option value="HR">Croatia (Hrvatska)</option>
                                            <option value="CU">Cuba</option>
                                            <option value="CY">Cyprus</option>
                                            <option value="CZ">Czech Republic</option>
                                            <option value="DK">Denmark</option>
                                            <option value="DJ">Djibouti</option>
                                            <option value="DM">Dominica</option>
                                            <option value="DO">Dominican Republic</option>
                                            <option value="EC">Ecuador</option>
                                            <option value="EG">Egypt</option>
                                            <option value="SV">El Salvador</option>
                                            <option value="GQ">Equatorial Guinea</option>
                                            <option value="ER">Eritrea</option>
                                            <option value="EE">Estonia</option>
                                            <option value="ET">Ethiopia</option>
                                            <option value="FK">Falkland Islands (Malvinas)</option>
                                            <option value="FO">Faroe Islands</option>
                                            <option value="FJ">Fiji</option>
                                            <option value="FI">Finland</option>
                                            <option value="FR">France</option>
                                            <option value="GF">French Guiana</option>
                                            <option value="PF">French Polynesia</option>
                                            <option value="TF">French Southern Territories</option>
                                            <option value="GA">Gabon</option>
                                            <option value="GM">Gambia</option>
                                            <option value="GE">Georgia</option>
                                            <option value="DE">Germany</option>
                                            <option value="GH">Ghana</option>
                                            <option value="GI">Gibraltar</option>
                                            <option value="GR">Greece</option>
                                            <option value="GL">Greenland</option>
                                            <option value="GD">Grenada</option>
                                            <option value="GP">Guadeloupe</option>
                                            <option value="GU">Guam</option>
                                            <option value="GT">Guatemala</option>
                                            <option value="GN">Guinea</option>
                                            <option value="GW">Guinea-Bissau</option>
                                            <option value="GY">Guyana</option>
                                            <option value="HT">Haiti</option>
                                            <option value="HM">Heard and Mc Donald Islands</option>
                                            <option value="VA">Holy See (Vatican City State)</option>
                                            <option value="HN">Honduras</option>
                                            <option value="HK">Hong Kong</option>
                                            <option value="HU">Hungary</option>
                                            <option value="IS">Iceland</option>
                                            <option value="IN">India</option>
                                            <option value="ID">Indonesia</option>
                                            <option value="IR">Iran (Islamic Republic of)</option>
                                            <option value="IQ">Iraq</option>
                                            <option value="IE">Ireland</option>
                                            <option value="IL">Israel</option>
                                            <option value="IT">Italy</option>
                                            <option value="JM">Jamaica</option>
                                            <option value="JP">Japan</option>
                                            <option value="JO">Jordan</option>
                                            <option value="KZ">Kazakhstan</option>
                                            <option value="KE">Kenya</option>
                                            <option value="KI">Kiribati</option>
                                            <option value="KP">Korea, Democratic People's Republic of</option>
                                            <option value="KR">Korea, Republic of</option>
                                            <option value="KW">Kuwait</option>
                                            <option value="KG">Kyrgyzstan</option>
                                            <option value="LA">Lao People's Democratic Republic</option>
                                            <option value="LV">Latvia</option>
                                            <option value="LB">Lebanon</option>
                                            <option value="LS">Lesotho</option>
                                            <option value="LR">Liberia</option>
                                            <option value="LY">Libyan Arab Jamahiriya</option>
                                            <option value="LI">Liechtenstein</option>
                                            <option value="LT">Lithuania</option>
                                            <option value="LU">Luxembourg</option>
                                            <option value="MO">Macau</option>
                                            <option value="MK">Macedonia, The Former Yugoslav Republic of</option>
                                            <option value="MG">Madagascar</option>
                                            <option value="MW">Malawi</option>
                                            <option value="MY">Malaysia</option>
                                            <option value="MV">Maldives</option>
                                            <option value="ML">Mali</option>
                                            <option value="MT">Malta</option>
                                            <option value="MH">Marshall Islands</option>
                                            <option value="MQ">Martinique</option>
                                            <option value="MR">Mauritania</option>
                                            <option value="MU">Mauritius</option>
                                            <option value="YT">Mayotte</option>
                                            <option value="MX">Mexico</option>
                                            <option value="FM">Micronesia, Federated States of</option>
                                            <option value="MD">Moldova, Republic of</option>
                                            <option value="MC">Monaco</option>
                                            <option value="MN">Mongolia</option>
                                            <option value="MS">Montserrat</option>
                                            <option value="MA">Morocco</option>
                                            <option value="MZ">Mozambique</option>
                                            <option value="MM">Myanmar</option>
                                            <option value="NA">Namibia</option>
                                            <option value="NR">Nauru</option>
                                            <option value="NP">Nepal</option>
                                            <option value="NL">Netherlands</option>
                                            <option value="AN">Netherlands Antilles</option>
                                            <option value="NC">New Caledonia</option>
                                            <option value="NZ">New Zealand</option>
                                            <option value="NI">Nicaragua</option>
                                            <option value="NE">Niger</option>
                                            <option value="NG">Nigeria</option>
                                            <option value="NU">Niue</option>
                                            <option value="NF">Norfolk Island</option>
                                            <option value="MP">Northern Mariana Islands</option>
                                            <option value="NO">Norway</option>
                                            <option value="OM">Oman</option>
                                            <option value="PK">Pakistan</option>
                                            <option value="PW">Palau</option>
                                            <option value="PA">Panama</option>
                                            <option value="PG">Papua New Guinea</option>
                                            <option value="PY">Paraguay</option>
                                            <option value="PE">Peru</option>
                                            <option value="PH">Philippines</option>
                                            <option value="PN">Pitcairn</option>
                                            <option value="PL">Poland</option>
                                            <option value="PT">Portugal</option>
                                            <option value="PR">Puerto Rico</option>
                                            <option value="QA">Qatar</option>
                                            <option value="RE">Reunion</option>
                                            <option value="RO">Romania</option>
                                            <option value="RU">Russian Federation</option>
                                            <option value="RW">Rwanda</option>
                                            <option value="KN">Saint Kitts and Nevis</option>
                                            <option value="LC">Saint LUCIA</option>
                                            <option value="VC">Saint Vincent and the Grenadines</option>
                                            <option value="WS">Samoa</option>
                                            <option value="SM">San Marino</option>
                                            <option value="ST">Sao Tome and Principe</option>
                                            <option value="SA">Saudi Arabia</option>
                                            <option value="SN">Senegal</option>
                                            <option value="SC">Seychelles</option>
                                            <option value="SL">Sierra Leone</option>
                                            <option value="SG">Singapore</option>
                                            <option value="SK">Slovakia (Slovak Republic)</option>
                                            <option value="SI">Slovenia</option>
                                            <option value="SB">Solomon Islands</option>
                                            <option value="SO">Somalia</option>
                                            <option value="ZA">South Africa</option>
                                            <option value="GS">South Georgia and the South Sandwich Islands</option>
                                            <option value="ES">Spain</option>
                                            <option value="LK">Sri Lanka</option>
                                            <option value="SH">St. Helena</option>
                                            <option value="PM">St. Pierre and Miquelon</option>
                                            <option value="SD">Sudan</option>
                                            <option value="SR">Suriname</option>
                                            <option value="SJ">Svalbard and Jan Mayen Islands</option>
                                            <option value="SZ">Swaziland</option>
                                            <option value="SE">Sweden</option>
                                            <option value="CH">Switzerland</option>
                                            <option value="SY">Syrian Arab Republic</option>
                                            <option value="TW">Taiwan, Province of China</option>
                                            <option value="TJ">Tajikistan</option>
                                            <option value="TZ">Tanzania, United Republic of</option>
                                            <option value="TH">Thailand</option>
                                            <option value="TG">Togo</option>
                                            <option value="TK">Tokelau</option>
                                            <option value="TO">Tonga</option>
                                            <option value="TT">Trinidad and Tobago</option>
                                            <option value="TN">Tunisia</option>
                                            <option value="TR">Turkey</option>
                                            <option value="TM">Turkmenistan</option>
                                            <option value="TC">Turks and Caicos Islands</option>
                                            <option value="TV">Tuvalu</option>
                                            <option value="UG">Uganda</option>
                                            <option value="UA">Ukraine</option>
                                            <option value="AE">United Arab Emirates</option>
                                            <option value="GB">United Kingdom</option>
                                            <option value="US">United States</option>
                                            <option value="UM">United States Minor Outlying Islands</option>
                                            <option value="UY">Uruguay</option>
                                            <option value="UZ">Uzbekistan</option>
                                            <option value="VU">Vanuatu</option>
                                            <option value="VE">Venezuela</option>
                                            <option value="VN">Viet Nam</option>
                                            <option value="VG">Virgin Islands (British)</option>
                                            <option value="VI">Virgin Islands (U.S.)</option>
                                            <option value="WF">Wallis and Futuna Islands</option>
                                            <option value="EH">Western Sahara</option>
                                            <option value="YE">Yemen</option>
                                            <option value="ZM">Zambia</option>
                                            <option value="ZW">Zimbabwe</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-lg-6 col-md-6">
                                        <label class="control-label">City</label>
                                        <input class="form-control" placeholder="" name="city" type="text" maxlength="20">
                                    </div>
                                    <div class="form-group col-lg-6 col-md-6">
                                        <label class="control-label">Zip</label>
                                        <input class="form-control" placeholder="" name="zipcode" type="text" maxlength="10">
                                    </div>
                                    <div class="form-group col-lg-6 col-md-6">
                                        <label class="control-label">University degrees</label>
                                        <select class="form-control" name="degress">
                                            <option value="UGSTU">Undergraduate student</option>
                                            <optgroup label="Medical specialty student">
                                                <option value="MSSR1">1 year resident</option>
                                                <option value="MSSR2">2 year resident</option>
                                                <option value="MSSR3">3 year resident</option>
                                            </optgroup>
                                            <option value="MDDOC">Medical Doctor</option>
                                            <optgroup label="Specialist Doctor">
                                                <option value="SDEm1">Experience < 1 year</option>
                                                <option value="SDEm3">Experience > 1 year and < 3 year</option>
                                                <option value="SDEm5">Experience > 3 year and < 5 year</option>
                                                <option value="SDEM5">Experience > 5 year</option>
                                            </optgroup>
                                        </select>
                                    </div>
                                    <div class="form-group col-lg-6 col-md-6">
                                        <label class="control-label">Institution</label>
                                        <input class="form-control" placeholder="" name="institution" type="text" maxlength="100">
                                    </div>
                                    <div class="form-group col-lg-6 col-md-6">
                                        <label class="control-label">Phone Number</label>
                                        <input class="form-control" placeholder="" name="phone_number" type="number" maxlength="20">
                                    </div>
                                    <div class="form-group col-lg-6 col-md-6">
                                        <label class="control-label">Mobile Number</label>
                                        <input class="form-control" placeholder="" name="mobile_number" type="number" maxlength="20">
                                    </div>
                                    <div class="form-group col-lg-6 col-md-6">
                                        <label class="control-label">Occupation</label>
                                        <input class="form-control" placeholder="" name="occupation" type="text" maxlength="100">
                                    </div>
                                    <div class="form-group col-lg-6 col-md-6">
                                        <label class="control-label">Languajes</label>
                                        <select class="form-control" name="language">
                                            <option value="ES">Español</option>
                                            <option value="EN">Ingles</option>
                                        </select>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group col-lg-6 col-md-6">
                                        <label class="control-label">Specialty</label>
                                        <select class="form-control" id="specialty" name="specialty" multiple="multiple"></select>
                                    </div>
                                    <div class="col-lg-6 col-md-6">
                                        <label class="control-label">Organs</label>
                                        <select class="form-control" id="organs" name="organs" multiple="multiple"></select>
                                    </div>
                                    <% if(roles.contains("admin")){%>
                                        <div class="form-group col-lg-6 col-md-6">
                                            <label class="control-label" for="roles">Roles user</label>
                                                <select class="form-control" id="roles" name="roles" multiple="multiple"></select>
                                        </div>
                                    <%}%>
                                    <div class="clearfix"></div>
                                    <input name="id_user" hidden value="<%=session.getAttribute("id")%>">
                                    <br><br>
                                    <button type="submit" class="btn btn-lg btn-primary col-lg-offset-3 col-md-offset-3 col-lg-6 col-md-6">Save</button>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="assets/js/plugins/jquery-2.1.4.min.js"></script>
        <script src="assets/js/plugins/bootstrap/bootstrap.min.js"></script>
        <script src="assets/js/plugins/jquery-validation/jquery.validate.min.js"></script>
        <script src="assets/js/plugins/bootstrap-typeahead.js"></script>
        <script src="assets/js/plugins/select2.min.js"></script>
        <script src="assets/js/wsi/profile.js"></script>
    </body>
</html>
