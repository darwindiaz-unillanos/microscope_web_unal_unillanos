/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.unal.wsi.core;

import java.io.File;
import java.io.IOException;
import java.awt.image.BufferedImage;

import jasper.Decoder;
import org.openslide.OpenSlide;

import edu.unal.jp2k.core.Processor;
import edu.unal.wsi.util.Config;
import edu.unal.wsiprovider.models.ImageInfo;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.nio.file.Files;
import java.nio.file.Paths;
import javax.imageio.ImageIO;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author Pablo Álvarez
 */
public class WSIReader {

    public static final short READ_JPEG = 0;
    public static final short READ_OPEN_SLIDE = 1;

    final private String JPEG_IMAGE_FOLDER;
    final private String JPEG_INDEX_FOLDER;
    final private String OPENSLIDE_IMAGE_FOLDER;
    final private String IMAGE_THUMBS_FOLDER;
    final private String INTERACTION_FOLDER;
    final private String STRUCT_FILE;
    final private String ANNOTATION_FOLDER;
    final private String DESCRIPTION_FOLDER;

    private static final int TILE_SIZE = 512;

    private short currentMode = 0;

    private ImageInfo jpegSlideInfo;
    private ImageInfo openSlideInfo;

    private Processor jpeg2kReader;
    private OpenSlide openSlideReader;

    public WSIReader() {
        Config config = Config.getInstance();
        JPEG_IMAGE_FOLDER = config.getJPEGImageFolder();
        JPEG_INDEX_FOLDER = config.getJPEGIndexFolder();
        OPENSLIDE_IMAGE_FOLDER = config.getOpenSlideImageFolder();
        IMAGE_THUMBS_FOLDER = config.getThumbFolder();
        INTERACTION_FOLDER = config.getInteractionFolder();
        ANNOTATION_FOLDER = config.getAnnotationFolder();
        STRUCT_FILE = config.getStructFile();
        DESCRIPTION_FOLDER = config.getDescriptionFolder();

        jpegSlideInfo = null;
        openSlideInfo = null;

        jpeg2kReader = null;
        openSlideReader = null;
    }

    public void readImage(String imageName, String readMode) throws IOException, IllegalArgumentException {
        switch (readMode) {
            case "jpeg":
                readImage(imageName, READ_JPEG);
                break;
            case "openslide":
                readImage(imageName, READ_OPEN_SLIDE);
                break;
            default:
                throw new IllegalArgumentException("Not compatible readMode: " + readMode);
        }
    }

    public void readImage(String imageName, short readMode) throws IOException, IllegalArgumentException {
        currentMode = readMode;

        String description=getDescription(imageName);
        switch (readMode) {
            case READ_JPEG:
                // Clearing reader first
                if (jpeg2kReader != null) {
                    if (jpeg2kReader.getName().equals(imageName)) {
                        return;
                    }

                    jpeg2kReader = null;
                }
                jpeg2kReader = new Processor(JPEG_IMAGE_FOLDER, JPEG_INDEX_FOLDER, imageName);
                jpegSlideInfo = new ImageInfo(imageName, jpeg2kReader.getWidth(),
                        jpeg2kReader.getHeight(), jpeg2kReader.getNumLev(),description);
                break;
            case READ_OPEN_SLIDE:
                // Clearing reader first
                if (openSlideReader != null) {
                    if (openSlideReader.getName().equals(imageName)) {
                        return;
                    }

                    openSlideReader.dispose();
                    openSlideReader = null;
                }

                openSlideReader = new OpenSlide(new File(OPENSLIDE_IMAGE_FOLDER + imageName));
                openSlideInfo = new ImageInfo(imageName, openSlideReader.getLevel0Width(),
                        openSlideReader.getLevel0Height(), openSlideReader.getLevelCount() - 1,description);
                break;
            default:
                throw new IllegalArgumentException("Not compatible readMode: " + readMode);
        }
    }

    public BufferedImage fetchThumbnail(String imageName) {

        BufferedImage thumbnail = null;
        try {
            thumbnail = ImageIO.read(new File(IMAGE_THUMBS_FOLDER + imageName));
        } catch (IOException e) {
            System.out.println("Warning: Image thumbnail not found");
        }

        return thumbnail;
    }

    public String getStruct() throws IOException {
        FileReader reader = new FileReader(STRUCT_FILE);
        JSONParser jsonParser = new JSONParser();
        try {
            JSONObject jsonObject = (JSONObject) jsonParser.parse(reader);
            String jsonString = jsonObject.toJSONString();
            jsonString = jsonString.substring(8, jsonString.length() - 1);
            return jsonString;
        } catch (ParseException e) {
            return "[]";
        }
    }

    private static JSONArray images;

    public String listImagesByCategory(String category) throws IOException {
        FileReader reader = new FileReader(STRUCT_FILE);
        JSONParser jsonParser = new JSONParser();
        
        images=new JSONArray();
        try {
            JSONObject jsonObject = (JSONObject) jsonParser.parse(reader);
            JSONArray parents = (JSONArray) jsonObject.get("data");
            for (int i = 0; i < parents.size(); i++) {
                getJsonChilds((JSONObject) parents.get(i), category);
            }
            return "{\"jpeg\": " + images + "}";
        } catch (ParseException e) {
            return "{\"jpeg\": []}";
        }
    }

    private JSONArray getJsonChilds(JSONObject jsonObject, String category) {

        String oid=(String)jsonObject.get("id");
        if (oid!=null && oid.equals(category)) {
            images=(JSONArray) jsonObject.get("images");
            return null;
        } else {
            JSONArray children = (JSONArray) jsonObject.get("nodes");
            if (children != null) {
                for (int i = 0; i < children.size(); i++) {
                    JSONObject obj = (JSONObject) children.get(i);
                    getJsonChilds(obj,category);
                }
            }
            return children;
        }
        
    }

    public String listImages() throws IOException {
        String JSONList;
        try {
            JSONList = "{\"jpeg\":[";
            File jpegDirectory = new File(JPEG_IMAGE_FOLDER);
            String[] files = jpegDirectory.list(new FilenameFilter() {
                @Override
                public boolean accept(File current, String name) {
                    return !new File(current, name).isDirectory();
                }
            });

            for (String filename : files) {
                if (!filename.equals("openslide")) {
                    JSONList += "\"" + filename + "\",";
                }
            }

            if (files.length > 0) {
                JSONList = JSONList.substring(0, JSONList.length() - 1);
            }

            JSONList += "],\"openslide\":[";

            File openslideDirectory = new File(OPENSLIDE_IMAGE_FOLDER);
            files = openslideDirectory.list();

            for (String filename : files) {
                JSONList += "\"" + filename + "\",";
            }
            if (files.length > 0) {
                JSONList = JSONList.substring(0, JSONList.length() - 1);
            }

            JSONList += "]}";
        } catch (NullPointerException e) {
            JSONList = "{\"jpeg\":[]}";
        }

        return JSONList;
    }

    public ImageInfo fetchImage(String imageName, String readMode) throws IOException, IllegalArgumentException {
        switch (readMode) {
            case "jpeg":
                return fetchImage(imageName, READ_JPEG);
            case "openslide":
                return fetchImage(imageName, READ_OPEN_SLIDE);
            default:
                throw new IllegalArgumentException("Not compatible readMode: " + readMode);
        }
    }

    public ImageInfo fetchImage(String imageName, short readMode) throws IOException, IllegalArgumentException {
        ImageInfo info;
        String description=getDescription(imageName);
        switch (readMode) {
            case READ_JPEG:
                Processor _jpeg2kReader = new Processor(JPEG_IMAGE_FOLDER, JPEG_INDEX_FOLDER, imageName);
                info = new ImageInfo(imageName, _jpeg2kReader.getWidth(),
                        _jpeg2kReader.getHeight(), _jpeg2kReader.getNumLev(),description);
                break;
            case READ_OPEN_SLIDE:
                OpenSlide _openSlideReader = new OpenSlide(new File(OPENSLIDE_IMAGE_FOLDER + imageName));
                int numLev = getNumLevels(_openSlideReader) - 1;
                info = new ImageInfo(imageName, _openSlideReader.getLevel0Width(),
                        _openSlideReader.getLevel0Height(), numLev,description);
                break;
            default:
                throw new IllegalArgumentException("Not compatible readMode: " + readMode);
        }

        return info;
    }

    public String getInteraction(String imageName) throws IOException {
        String path = INTERACTION_FOLDER + "/" + imageName + ".txt";
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, "UTF-8");
    }
    
    public String getAnnotation(String imageName)  {
        try {
            String path = ANNOTATION_FOLDER + "/" + imageName + ".json";
            byte[] encoded = Files.readAllBytes(Paths.get(path));
            return new String(encoded, "UTF-8");
        } catch (IOException ex) {
            return "[]";
        }        
    }
    
    private String getDescription(String imageName)  {
        try {
            String path = DESCRIPTION_FOLDER + "/" + imageName + ".txt";
            byte[] encoded = Files.readAllBytes(Paths.get(path));
            return new String(encoded, "UTF-8");
        } catch (IOException ex) {
            return "";
        }        
    }

    protected int[] calculateROI(int tileX, int tileY, int layer) {
        int maxRes;
        double resAmplifier = 1;

        switch (currentMode) {
            case READ_JPEG:
                maxRes = jpegSlideInfo.getTotalLayers();
                resAmplifier = Math.pow(2.0, maxRes - layer);
                break;
            case READ_OPEN_SLIDE:
                //resAmplifier = openSlideReader.getLevelDownsample(layer);
                resAmplifier = Math.pow(2.0, layer);
                break;
        }

        int currentTileSize = TILE_SIZE * (int) resAmplifier;
        int x1 = tileX * currentTileSize;
        int y1 = tileY * currentTileSize;
        int x2 = x1 + currentTileSize;
        int y2 = y1 + currentTileSize;

        //System.out.println("Res = " + layer);
        //System.out.println("tile x = " + tileX);
        //System.out.println("tile y = " + tileY);
        return new int[]{x1, y1, x2, y2, layer};
    }

    public BufferedImage requestRegion(int tileX, int tileY, int layer)
            throws IOException, InterruptedException {

        switch (currentMode) {
            case READ_JPEG:
                return requestJPEGRegion(tileX, tileY, layer);
            case READ_OPEN_SLIDE:
                return requestOpenSlideRegion(tileX, tileY, layer);
        }

        return null;
    }

    protected BufferedImage requestJPEGRegion(int tileX, int tileY, int layer)
            throws IOException, InterruptedException {

        int woiH = TILE_SIZE;
        int woiW = TILE_SIZE;
        int numR = jpegSlideInfo.getTotalLayers() + 1;
        int[] roiCoords = calculateROI(tileX, tileY, layer); //{roiParams.x1, roiParams.y1, roiParams.x2, roiParams.y2};

        try {
            jpeg2kReader.requestRegion(roiCoords, jpegSlideInfo.getTotalLayers() - 1, layer);
            int f = (int) (Math.pow(2, numR - layer)) / 2;
            int w = jpeg2kReader.getDim(numR - 1, roiCoords[2] - roiCoords[0], layer);
            int h = jpeg2kReader.getDim(numR - 1, roiCoords[3] - roiCoords[1], layer);

            if (w != woiW || h != woiH) {
                if (w != woiW) {
                    roiCoords[2] = roiCoords[0] + woiW * f;
                }
                if (h != woiH) {
                    roiCoords[3] = roiCoords[1] + woiH * f;
                }
                jpeg2kReader.requestRegion(roiCoords, jpegSlideInfo.getTotalLayers() - 1, layer);
                w = jpeg2kReader.getDim(numR - 1, roiCoords[2] - roiCoords[0], layer);
                h = jpeg2kReader.getDim(numR - 1, roiCoords[3] - roiCoords[1], layer);
            }

            int[] nCoord = roiCoords.clone();
            if (nCoord[0] != 0) {
                nCoord[0]--;
            }
            if (nCoord[1] != 0) {
                nCoord[1]--;
            }
            nCoord[2]++;
            nCoord[3]++;

            byte[] bytes = jpeg2kReader.requestRegion(nCoord, jpegSlideInfo.getTotalLayers() - 1, layer);
            int nw = jpeg2kReader.getDim(numR - 1, nCoord[2] - nCoord[0], layer);
            int nh = jpeg2kReader.getDim(numR - 1, nCoord[3] - nCoord[1], layer);

            Decoder dec = new Decoder();
            //int[] pix = new int[bytes.length];
            int[] pix = dec.decode(bytes, bytes.length, nw, nh);
            BufferedImage img = getImage(pix, nw, nh);

            int origX = 0, origY = 0;
            int dif;
            if (roiCoords[0] != 0) {
                dif = nw - w;
                origX = roiCoords[2] == nCoord[2] ? dif : dif / 2;
            }
            if (roiCoords[1] != 0) {
                dif = nh - h;
                origY = roiCoords[3] == nCoord[3] ? dif : dif / 2;
            }
            return img.getSubimage(origX, origY, w, h);//getImage(pix, w, h);
        } catch (IOException | ClassNotFoundException e) {
            return null;
        }
    }

    protected BufferedImage requestOpenSlideRegion(int tileX, int tileY, int layer)
            throws IOException {

        int numLev = getNumLevels(openSlideReader) - 1;
        layer = numLev - layer;
        int rlay = getRealLayer(openSlideReader, layer);

        if (rlay < 0) {
            return null;
        }

        // Check if the tile is not outside image bounds
        int roiW = TILE_SIZE;
        int roiH = TILE_SIZE;
        long imageWidth = openSlideReader.getLevel0Width();
        long imageHeight = openSlideReader.getLevel0Height();

        double levelDownSample = Math.pow(2, layer);
        int[] roiCoords = calculateROI(tileX, tileY, layer);

        if (roiCoords[0] + roiW * levelDownSample > imageWidth) {
            roiW = (int) ((imageWidth - roiCoords[0]) / levelDownSample);
        }

        if (roiCoords[1] + roiH * levelDownSample > imageHeight) {
            roiH = (int) ((imageHeight - roiCoords[1]) / levelDownSample);
        }

        int[] pixels = new int[roiW * roiH];

        openSlideReader.paintRegionARGB(pixels, roiCoords[0], roiCoords[1], rlay, roiW, roiH);

        return getImage(pixels, roiW, roiH);
    }

    private int getNumLevels(OpenSlide osr) {
        int realNumLev = osr.getLevelCount() - 1;
        int maxDownSample = (int) osr.getLevelDownsample(realNumLev);
        return (int) (Math.log(maxDownSample) / Math.log(2)) + 1;
    }

    private int getRealLayer(OpenSlide osr, int layer) {
        int downsample = (int) Math.pow(2, layer);
        int numLev = osr.getLevelCount();
        for (int level = 0; level < numLev; level++) {
            if ((int) osr.getLevelDownsample(level) == downsample) {
                return level;
            }
        }
        return -1;
    }

    protected BufferedImage getImage(int[] rgbData, int w, int h) {
        BufferedImage bi = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
        int k = 0, l, n = 0;
        int nh = 3 * h, nw = 3 * w;
        for (int i = 0; i < nh; i = i + 3) {
            l = 0;
            for (int j = 0; j < nw; j = j + 3) {
                bi.setRGB(l, k, rgbData[n]);
                l++;
                n++;
            }
            k++;
        }
        return bi;
    }
}
