/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.unal.wsi.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Clase para cargar los archivos de configuracion directamente desde el equipo
 * donde se encuentre la instalacion del servidor
 *
 * @author Luis Daniel Martinez
 * @version 1.0
 */
public final class Config {

    private static Config _this_;
    private static Properties prop;

    private final String fileName = "config_wsi";
    private final String resource_thumbnail = "res_thumbnail";
    private final String resource_jpeg_images = "res_jpeg_images";
    private final String resource_jpeg_indexes = "res_jpeg_indexes";
    private final String resource_openslide_images = "res_openslide_images";
    private final String resource_interactions = "res_interactions";
    private final String resource_annotations = "res_annotations";
    private final String resource_descriptions = "res_descriptions";
    private final String struct_file = "struct_file";
    
    private Config() {
        try {
            loadConfigFile();
        } catch (FileNotFoundException ex) {
            System.err.println(ex);
        } catch (IOException ex) {
            System.err.println(ex);
        }
    }

    public static Config getInstance() {
        if (_this_ == null) {
            _this_ = new Config();
        }
        return _this_;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        throw new CloneNotSupportedException("A singleton class cannot be cloned");
    }
    
    public void loadConfigFile() throws FileNotFoundException, IOException {
        String confPath = getUserDirPath() + "config" + System.getProperty("file.separator") + fileName + ".properties";
        System.out.println(confPath);
        
        InputStream is = new FileInputStream(confPath);
        prop = new Properties();
        
        prop.load(is);
    }

    private String getUserDirPath() {
        return System.getProperty("user.home") + System.getProperty("file.separator");
    }

    private String getProperty(String property) {
        return prop.getProperty(property);
    }

    public String getThumbFolder() {
        return getProperty(resource_thumbnail);
    }

    public String getJPEGImageFolder() {
        return getProperty(resource_jpeg_images);
    }

    public String getJPEGIndexFolder() {
        return getProperty(resource_jpeg_indexes);
    }

    public String getOpenSlideImageFolder() {
        return getProperty(resource_openslide_images);
    }
    
    public String getInteractionFolder() {
        return getProperty(resource_interactions);
    }
    
    public String getAnnotationFolder() {
        return getProperty(resource_annotations);
    }
    
    public String getStructFile() {
        return getProperty(struct_file);
    }
    
    public String getDescriptionFolder() {
        return getProperty(resource_descriptions);
    }    
}
