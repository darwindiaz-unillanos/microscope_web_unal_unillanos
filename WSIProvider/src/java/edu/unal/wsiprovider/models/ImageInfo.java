/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.unal.wsiprovider.models;

import java.io.Serializable;

/**
 *
 * @author Pablo Álvarez
 */
public class ImageInfo implements Serializable {
    private static final long serialVersionUID = 1L;
    
    final private String name;
    
    final private long width;
    final private long height;
    final private int totalLayers;
    final private String description;
    
    public ImageInfo(String name, long width, long height, int layers,String description){
        this.name = name;
        this.width = width;
        this.height = height;
        this.totalLayers = layers;
        this.description=description;
    }

    public String getName() {
        return name;
    }

    public long getWidth() {
        return width;
    }

    public long getHeight() {
        return height;
    }

    public int getTotalLayers() {
        return totalLayers;
    }
    
    public String getDescription(){
        return description;
    }
    
    public String toJson() {        
        return "{\"name\":\"" + name + "\"," +
                "\"width\":\"" + width + "\"," +
                "\"height\":\"" + height + "\"," +
                "\"layers\":\"" + totalLayers + "\"," +
                "\"description\":\"" + description + "\"" +
                "}";
    }
}
