/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.unal.wsiprovider.servlets;

import edu.unal.wsi.core.WSIReader;
import edu.unal.wsiprovider.models.ImageInfo;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletConfig;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Pablo Álvarez
 */
public class WSIProvider extends HttpServlet {

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        WSIReader wsiReader = initializeReader(request);

        String imageName = request.getParameter("wsi");
        String readMode = request.getParameter("reader");

        switch (request.getRequestURI()) {
            case "/WSIProvider/navigate":
                int r = Integer.parseInt(request.getParameter("r"));
                int x = Integer.parseInt(request.getParameter("x"));
                int y = Integer.parseInt(request.getParameter("y"));

                try {
                    wsiReader.readImage(imageName, readMode);
                    BufferedImage image = wsiReader.requestRegion(x, y, r);
                    writeImage(response, image);
                } catch (InterruptedException e) {
                    response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                    response.setContentType("text/plain");
                    response.getWriter().println(e.getMessage());
                } catch (IllegalArgumentException e) {
                    System.out.println("Null image sent");
                }
                break;
            case "/WSIProvider/info":
                try {
                    ImageInfo info = wsiReader.fetchImage(imageName, readMode);

                    // Allowing cross domain policy
                    response.addHeader("Access-Control-Allow-Origin", "*");
                    response.setContentType("application/json;charset=UTF-8");

                    response.getWriter().println(info.toJson());
                } catch (IOException | IllegalArgumentException e) {
                    response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                    response.setContentType("text/plain");
                    response.getWriter().println(e.getMessage());
                }

                break;
            case "/WSIProvider/list":
                response.addHeader("Access-Control-Allow-Origin", "*");
                response.setContentType("application/json;charset=UTF-8");
                response.getWriter().println(wsiReader.listImages());
                break;
            case "/WSIProvider/listByCategory":
                response.addHeader("Access-Control-Allow-Origin", "*");
                response.setContentType("application/json;charset=UTF-8");
                String category = request.getParameter("cat");
                response.getWriter().println(wsiReader.listImagesByCategory(category));
                break;
            case "/WSIProvider/struct":
                // Allowing cross domain policy
                response.addHeader("Access-Control-Allow-Origin", "*");
                response.setContentType("application/json;charset=UTF-8");
                response.getWriter().println(wsiReader.getStruct());
                break;
            case "/WSIProvider/thumb":
                String wsi = request.getParameter("wsi");
                String w = request.getParameter("w");
                String h = request.getParameter("h");

                BufferedImage thumb = wsiReader.fetchThumbnail(wsi + ".jpg");

                if (thumb != null) {
                    if (w != null && h != null) {
                        int thumbW = Integer.parseInt(w);
                        int thumbH = Integer.parseInt(h);
                        if (thumbH > thumb.getHeight()) {
                            thumbH = thumb.getHeight();
                        }
                        if (thumbW > thumb.getWidth()) {
                            thumbW = thumb.getWidth();
                        }
                        thumb = thumb.getSubimage(0, 0, thumbW, thumbH);
                    }
                } else {
                    thumb = ImageIO.read(new File(getServletContext().getRealPath("/") + "/img/default-img.jpg"));
                }
                writeImage(response, thumb);
                break;
            case "/WSIProvider/interaction":
                wsi = request.getParameter("wsi");
                response.setContentType("text/plain");
                response.getWriter().println(wsiReader.getInteraction(wsi));
                break;
            case "/WSIProvider/annotation":
                wsi = request.getParameter("wsi");
                response.setContentType("text/plain");
                response.getWriter().println(wsiReader.getAnnotation(wsi));
                break;
            /*case "/WSIProvider/description":
                wsi = request.getParameter("wsi");
                response.setContentType("text/plain");
                response.getWriter().println(wsiReader.getDescription(wsi));
                break;*/
        }
    }

    private WSIReader initializeReader(HttpServletRequest request) {
        HttpSession session = request.getSession();
        WSIReader reader = (WSIReader) session.getAttribute("wsiReader");

        if (reader == null) {
            reader = new WSIReader();
            session.setAttribute("wsiReader", reader);
        }

        return reader;
    }

    private void writeImage(HttpServletResponse response, BufferedImage image) throws IOException {
        String imgName = "img.jpg";
        OutputStream os = response.getOutputStream();
        StringBuilder sbContentDispValue = new StringBuilder();

        sbContentDispValue.append("inline");
        sbContentDispValue.append("; filename=");
        sbContentDispValue.append(imgName);

        response.setContentType("image/jpeg");
        response.setHeader("Cache-Control", "max-age=30");
        response.setHeader("Content-disposition", sbContentDispValue.toString());

        ImageIO.write(image, "jpg", os);

        os.close();
    }
}
