/*
 *  OpenSlide, a library for reading whole slide image files
 *
 *  Copyright (c) 2007-2009 Carnegie Mellon University
 *  All rights reserved.
 *
 *  OpenSlide is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation, version 2.1.
 *
 *  OpenSlide is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with OpenSlide. If not, see
 *  <http://www.gnu.org/licenses/>.
 *
 */
package org.openslide;

import java.io.File;
import java.io.IOException;

public class TestCLI {
    
    static int TILE_SIZE = 512;

    static void print_downsamples(OpenSlide osr) {
        for (int level = 0; level < osr.getLevelCount(); level++) {
            System.out.printf("level %d: downsample: %g\n", level, osr
                    .getLevelDownsample(level));
        }
    }

    static int get_real_levels(OpenSlide osr) {
        int numLev = osr.getLevelCount() - 1;
        int maxDownSample = (int) osr.getLevelDownsample(numLev);
        return (int) (Math.log(maxDownSample) / Math.log(2)) + 1;
    }

    static int transform_layer(OpenSlide osr, int layer) {

        int downsample = (int) Math.pow(2, layer);
        int numLev = osr.getLevelCount();
        for (int level = 0; level < numLev; level++) {
            if ((int) osr.getLevelDownsample(level) == downsample) {
                return level;
            }
        }
        return -10;

    }

    static void test_next_biggest(OpenSlide osr, double downsample) {
        int level = osr.getBestLevelForDownsample(downsample);
        System.out.printf("level for downsample %g: %d (%g)\n", downsample,
                level, osr.getLevelDownsample(level));
    }

    static  int getNumLevels(OpenSlide osr) {
        int realNumLev = osr.getLevelCount() - 1;
        int maxDownSample = (int) osr.getLevelDownsample(realNumLev);
        return (int) (Math.log(maxDownSample) / Math.log(2)) + 1;
    }
    
    static int getRealLayer(OpenSlide osr, int layer) {

        int downsample = (int) Math.pow(2, layer);
        int numLev = osr.getLevelCount();
        for (int level = 0; level < numLev; level++) {
            if ((int) osr.getLevelDownsample(level) == downsample) {
                return level;
            }
        }
        return -10;
    }

    static  void requestOpenSlideRegion(OpenSlide openSlideReader, int tileX, int tileY, int layer) {
        
        // Check if the tile is not outside image bounds
        int roiW = TILE_SIZE;
        int roiH = TILE_SIZE;
        long imageWidth = openSlideReader.getLevel0Width();
        long imageHeight = openSlideReader.getLevel0Height();

        int numLev = getNumLevels(openSlideReader) - 1;
        layer = numLev - layer;

        double levelDownSample = Math.pow(2, layer);//openSlideReader.getLevelDownsample(layer);
        
        int[] roiCoords = calculateROI(tileX, tileY, layer);
        
        if (roiCoords[0] + roiW * levelDownSample > imageWidth) {
            roiW = (int) ((imageWidth - roiCoords[0]) / levelDownSample);
        }

        if (roiCoords[1] + roiH * levelDownSample > imageHeight) {
            roiH = (int) ((imageHeight - roiCoords[1]) / levelDownSample);
        }

        System.out.println(tileX+" "+tileY+" "+layer+" > "+roiCoords[0]+" "+roiCoords[1]+" "+roiW+" "+roiH);
           
    }

    static  int[] calculateROI(int tileX, int tileY, int layer) {
        double resAmplifier = Math.pow(2.0, layer);//openSlideReader.getLevelDownsample(layer);

        int currentTileSize = TILE_SIZE * (int) resAmplifier;
        int x1 = tileX * currentTileSize;
        int y1 = tileY * currentTileSize;
        int x2 = x1 + currentTileSize;
        int y2 = y1 + currentTileSize;

        return new int[]{x1, y1, x2, y2, layer};
    }

    public static void main(String args[]) throws IOException {
        /*if (args.length != 1) {
         System.out.printf("give file!\n");
         return;
         }*/
        //System.setProperty("java.library.path", "C:\\Users\\German\\Documents\\Programas\\openslide\\32\\root\\bin");

        System.out.printf("version: %s\n", OpenSlide.getLibraryVersion());

        String filePath = "C:\\resources\\images\\openslide\\CMU-2.svs";
        //String filePath = "C:\\resources\\images\\openslide\\CMU-1.ndpi";
        File f = new File(filePath);

        System.out.printf("openslide_detect_vendor returns %s\n",
                OpenSlide.detectVendor(f));
        OpenSlide osr = new OpenSlide(f);

        long w, h;

        osr.dispose();

        osr = new OpenSlide(f);

        w = osr.getLevel0Width();
        h = osr.getLevel0Height();
        System.out.printf("dimensions: %d x %d\n", w, h);

        int levels = osr.getLevelCount();
        System.out.printf("num levels: %d\n", levels);
        
        requestOpenSlideRegion(osr,0,1,0);

        /*print_downsamples(osr);

        test_next_biggest(osr, 0.8);
        test_next_biggest(osr, 1.0);
        test_next_biggest(osr, 1.5);
        test_next_biggest(osr, 2.0);
        test_next_biggest(osr, 3.0);
        test_next_biggest(osr, 3.1);
        test_next_biggest(osr, 10);
        test_next_biggest(osr, 20);
        test_next_biggest(osr, 25);
        test_next_biggest(osr, 100);
        test_next_biggest(osr, 1000);
        test_next_biggest(osr, 10000);
*/
        osr.dispose();
    }
}
