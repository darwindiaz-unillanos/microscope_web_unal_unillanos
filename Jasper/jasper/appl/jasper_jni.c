#include <stdio.h>
#include <stdlib.h>
#include <jasper/jasper.h>
#include <jni.h>

int get_rgb(int r, int g, int b){
    return ((0xFF<<24) |((0xFF & r) << 16) | ((0xFF & g) << 8) | (0xFF & b));
}

void get_pixels(int x,int y,int w,int h,int **bufrgb,jas_image_t *ActImage){
	
	int i=0,j=0,k=0;
	jas_matrix_t *ActBufs[3];
	
	ActBufs[0] = jas_matrix_create(h,w);
	ActBufs[1] = jas_matrix_create(h,w);
	ActBufs[2] = jas_matrix_create(h,w);
	
	jas_image_readcmpt(ActImage, 0, x,y,w,h,ActBufs[0]);
	jas_image_readcmpt(ActImage, 1, x,y,w,h,ActBufs[1]);
	jas_image_readcmpt(ActImage, 2, x,y,w,h,ActBufs[2]);

	for(j=0;j<h;j++){
		for(i=0;i<w;i++){
			(*bufrgb)[k]=get_rgb((int)ActBufs[0]->data_[k],(int)ActBufs[1]->data_[k],(int)ActBufs[2]->data_[k]);
			k++;
		}
	}
	
	jas_matrix_destroy(ActBufs[0]);
	jas_matrix_destroy(ActBufs[1]);
	jas_matrix_destroy(ActBufs[2]);
}


int run(jbyte *bufjpc, int size_jpc, int *bufrgb,int w,int h){
	jas_image_t *image;
	jas_stream_t *in;

	int fmt;

	if (jas_init()) {
		abort();
	}

    if (!(in = jas_stream_memopen(bufjpc, size_jpc))) {
       fprintf(stderr,"error: cannot open memory stream\n");
       return -1;
    }

	if ((fmt = jas_image_getfmt(in)) < 0) {
		fprintf(stderr, "error: input image has unknown format\n");
		exit(EXIT_FAILURE);
	}

	if (!(image = jas_image_decode(in, fmt, 0))) {
		fprintf(stderr, "error: cannot load image data\n");
		exit(EXIT_FAILURE);
	}

	(void) jas_stream_close(in);

	get_pixels(0,0,w,h,&bufrgb,image);	

	jas_image_destroy(image);
	jas_image_clearfmts();

	return EXIT_SUCCESS;
}

JNIEXPORT jintArray JNICALL Java_jasper_Decoder_decode(JNIEnv *env, jclass cls,
		jbyteArray jbufjpc,jint jlen_jpc,jint w,jint h){
    
	int *bufrgb;
    jbyte *bufjpc;
	int size;
	jintArray result;

	size = w*h;
    bufrgb = malloc(size*sizeof(int));
	bufjpc = malloc(jlen_jpc*sizeof(char));

	(*env)->GetByteArrayRegion(env, jbufjpc, 0, jlen_jpc, bufjpc);

	//decoding
	run(bufjpc,jlen_jpc,bufrgb,w,h);

	result=(*env)->NewIntArray(env,size);
	(*env)->SetIntArrayRegion(env,result,0,size,bufrgb);

	//free memory
	free( bufjpc );
	free( bufrgb );
	
	return result;
}