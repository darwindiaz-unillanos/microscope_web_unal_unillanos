PROJECT(libjasper)

INCLUDE_DIRECTORIES(
	${libjasper_SOURCE_DIR}/../libjasper/include
)

IF(WIN32)
	ADD_DEFINITIONS(/D "WIN32")
	ADD_DEFINITIONS(/D "JAS_WIN_MSVC_BUILD")
	ADD_DEFINITIONS(/D "NDEBUG")
	ADD_DEFINITIONS(/D "_MBCS")
	ADD_DEFINITIONS(/D "_LIB")
ENDIF(WIN32)


ADD_LIBRARY(libjasper
	../libjasper/base/jas_cm.c
	../libjasper/base/jas_debug.c
	../libjasper/base/jas_getopt.c
	../libjasper/base/jas_icc.c
	../libjasper/base/jas_iccdata.c
	../libjasper/base/jas_image.c
	../libjasper/base/jas_init.c
	../libjasper/base/jas_malloc.c
	../libjasper/base/jas_seq.c
	../libjasper/base/jas_stream.c
	../libjasper/base/jas_string.c
	../libjasper/base/jas_tmr.c
	../libjasper/base/jas_tvp.c
	../libjasper/base/jas_version.c
	../libjasper/bmp/bmp_cod.c
	../libjasper/bmp/bmp_dec.c
	../libjasper/bmp/bmp_enc.c
	../libjasper/jp2/jp2_cod.c
	../libjasper/jp2/jp2_dec.c
	../libjasper/jp2/jp2_enc.c
	../libjasper/jpc/jpc_bs.c
	../libjasper/jpc/jpc_cs.c
	../libjasper/jpc/jpc_dec.c
	../libjasper/jpc/jpc_enc.c
	../libjasper/jpc/jpc_math.c
	../libjasper/jpc/jpc_mct.c
	../libjasper/jpc/jpc_mqcod.c
	../libjasper/jpc/jpc_mqdec.c
	../libjasper/jpc/jpc_mqenc.c
	../libjasper/jpc/jpc_qmfb.c
	../libjasper/jpc/jpc_t1cod.c
	../libjasper/jpc/jpc_t1dec.c
	../libjasper/jpc/jpc_t1enc.c
	../libjasper/jpc/jpc_t2cod.c
	../libjasper/jpc/jpc_t2dec.c
	../libjasper/jpc/jpc_t2enc.c
	../libjasper/jpc/jpc_tagtree.c
	../libjasper/jpc/jpc_tsfb.c
	../libjasper/jpc/jpc_util.c
	../libjasper/jpg/jpg_dec.c
	../libjasper/jpg/jpg_dummy.c
	../libjasper/jpg/jpg_enc.c
	../libjasper/jpg/jpg_val.c
	../libjasper/mif/mif_cod.c
	../libjasper/pgx/pgx_cod.c
	../libjasper/pgx/pgx_dec.c
	../libjasper/pgx/pgx_enc.c
	../libjasper/pnm/pnm_cod.c
	../libjasper/pnm/pnm_dec.c
	../libjasper/pnm/pnm_enc.c
	../libjasper/ras/ras_cod.c
	../libjasper/ras/ras_dec.c
	../libjasper/ras/ras_enc.c
	../libjasper/bmp/bmp_cod.h
	../libjasper/bmp/bmp_enc.h
	../libjasper/include/jasper/jas_cm.h
	../libjasper/include/jasper/jas_config.h
	../libjasper/include/jasper/jas_dll.h
	../libjasper/include/jasper/jas_debug.h
	../libjasper/include/jasper/jas_fix.h
	../libjasper/include/jasper/jas_getopt.h
	../libjasper/include/jasper/jas_icc.h
	../libjasper/include/jasper/jas_image.h
	../libjasper/include/jasper/jas_init.h
	../libjasper/include/jasper/jas_malloc.h
	../libjasper/include/jasper/jas_math.h
	../libjasper/include/jasper/jas_seq.h
	../libjasper/include/jasper/jas_stream.h
	../libjasper/include/jasper/jas_string.h
	../libjasper/include/jasper/jas_tmr.h	
	../libjasper/include/jasper/jas_tvp.h
	../libjasper/include/jasper/jas_types.h
	../libjasper/include/jasper/jas_version.h
	../libjasper/include/jasper/jasper.h
	../libjasper/jp2/jp2_cod.h
	../libjasper/jp2/jp2_dec.h
	../libjasper/jpc/jpc_bs.h
	../libjasper/jpc/jpc_cod.h
	../libjasper/jpc/jpc_cs.h
	../libjasper/jpc/jpc_dec.h
	../libjasper/jpc/jpc_enc.h
	../libjasper/jpc/jpc_fix.h
	../libjasper/jpc/jpc_flt.h
	../libjasper/jpc/jpc_math.h
	../libjasper/jpc/jpc_mct.h
	../libjasper/jpc/jpc_mqcod.h
	../libjasper/jpc/jpc_mqdec.h
	../libjasper/jpc/jpc_mqenc.h
	../libjasper/jpc/jpc_qmfb.h
	../libjasper/jpc/jpc_t1cod.h
	../libjasper/jpc/jpc_t1dec.h
	../libjasper/jpc/jpc_t1enc.h
	../libjasper/jpc/jpc_t2cod.h
	../libjasper/jpc/jpc_t2dec.h
	../libjasper/jpc/jpc_t2enc.h
	../libjasper/jpc/jpc_tagtree.h
	../libjasper/jpc/jpc_tsfb.h
	../libjasper/jpc/jpc_util.h
	../libjasper/jpg/jpg_cod.h
	../libjasper/jpg/jpg_enc.h
	../libjasper/jpg/jpg_jpeglib.h
	../libjasper/mif/mif_cod.h
	../libjasper/pgx/pgx_cod.h
	../libjasper/pgx/pgx_enc.h
	../libjasper/pnm/pnm_cod.h
	../libjasper/pnm/pnm_enc.h
	../libjasper/ras/ras_cod.h
	../libjasper/ras/ras_enc.h
)

SUBDIRS(
	jasper
)
