{"data":
[{
	"text": "Skin",
	"selectable":false,
	"nodes": [
	{
		"text": "Carcinomas",
		"id": "carcinoma",
		"selectable":false,
		"href": "list?cat=carcinoma",
		"images": ["1164-16","13104-15","1339-16","1460-09-2","1467-09-2","1527-09","1611-16","1687-09","1724-09-1","1864-09","1906-09","2096-16","102879","2224-09","2237-16","2310-09","181908","182868","2546-09-NC","2621-16","2648-09","Imghistopat_1sl","TQB4","TQB8"]
	},
	{
		"text": "Melanomas",
		"id": "melanoma",
		"selectable":false,
		"href": "list?cat=melanoma",
		"images": ["088-06","286504"],
	},
	{
		"text": "Trichoepithelioma",
		"id": "tricho",
		"selectable":false,
		"href": "list?cat=tricho",
		"images": ["2577-16","TAF1_1","TAF1_2","TAF1_3"],
	},
	{
		"text": "Nevus",
		"id": "nevus",
		"selectable":false,
		"href": "list?cat=nevus",
		"images": ["2547-09","Imghistopat_36sl"],
	},
	{
		"text": "Warts",
		"id": "wart",
		"selectable":false,
		"href": "list?cat=wart",
		"images": ["1801-09","1886-09","253-09"],
	},
	{
		"text": "Other cases",
		"id": "other_skin",
		"selectable":false,
		"href": "list?cat=other_skin",
		"images": ["1870-10-4",	"2419-16",	"2438-14",	"2456-09",	"5529-14",	"811-05",	"8152-13",	"Imghistopat_47sl",	"Imghistopat_6sl",	"TAF7",	"TASu2",	"cuero_cabelludo",	"piel_fina",	"piel_gruesa"],
	}
	]	
},
{
	"text": "Breast",
	"id": "breast",
	"selectable":false,
	"href": "list?cat=breast",
	"images": ["263-1-16","264-2-16","437-16"],
},
{
	"text": "Stomach",
	"id": "stomach",
	"selectable":false,
	"href": "list?cat=stomach",
	"images": ["1306-06-2","ana-005-14","ana-041-14","gastrica.004-14","gastrica.005-14"],
},
{
	"text": "Intestine",
	"id": "intestine",
	"selectable":false,
	"href": "list?cat=intestine",
	"images": ["yeyuno_pas"],
},
{
	"text": "Lung",
	"id": "lung",
	"selectable":false,
	"href": "list?cat=lung",
	"images": ["bronquiolos10X"],
},
{
	"text": "Other organs",
	"id": "other_organs",
	"selectable":false,
	"href": "list?cat=other_organs",
	"images": [],
}
]}